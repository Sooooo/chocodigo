﻿#ifndef __COMMON_LIB_H__
#define __COMMON_LIB_H__

#include "cocostudio/CocoStudio.h"

USING_NS_CC;

namespace commonLib	{

	std::string to_string(int t);
	void appplicationExit();
}

#endif
