﻿#ifndef __SCENE_PROLOGUE_CREATOR_H__
#define __SCENE_PROLOGUE_CREATOR_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

#include "CommonTagSet.h"
#include "CommonLib.h"

USING_NS_CC;
using namespace ui;

class PrologueCreator : public Layer
{
public:
	Button* Background;

	PrologueCreator();
	~PrologueCreator();

	static Scene* createScene(int i);
	virtual bool init();
	CREATE_FUNC(PrologueCreator);

	void onTouch(Ref* sender, Widget::TouchEventType type);		//터치 이벤트
};
#endif
