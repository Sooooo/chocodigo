﻿#ifndef __SCENE_GAME_3_3_1_H__
#define __SCENE_GAME_3_3_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"

/*************** 태그 정보 ***************/
#define TAG_BUTTON_NEXT		703
#define TAG_BUTTON_FRONT	704
#define TAG_TEXT_PAGE1		705
#define TAG_TEXT_PAGE2		706
#define TAG_TEXT_PAGE3		707
#define TAG_TEXT_PAGE4		708
/****************************************/


class SceneGame_3_3_1 : public Layer
{
public:

	Button* ButtonBack;
	Button* ButtonNext;
	Button* ButtonFront;

	TextField* Recipe1;
	TextField* Recipe2;
	TextField* Recipe3;
	TextField* Recipe4;

	SceneGame_3_3_1();
	~SceneGame_3_3_1();

	static cocos2d::Scene* createScene();
	virtual bool init();
	CREATE_FUNC(SceneGame_3_3_1);

	void onTouch(Ref* sender, Widget::TouchEventType type);

};


#endif 