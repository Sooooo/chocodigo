﻿//기본 헤더
#include "SceneTitle.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"

#include "SceneCreator.h"
#include "checkProgress.h"

//화면 전환
#include "SceneWorld.h"


USING_NS_CC;
using namespace ui;
using namespace cocos2d::experimental;

SceneTitle::SceneTitle(){
	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM.mp3";
}

SceneTitle::~SceneTitle(){
	delete logManager;
	
	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneTitle::createScene()
{
    auto scene = Scene::create();
  
	auto layer = SceneTitle::create();
    scene->addChild(layer);

    return scene;
}

// on "init" you need to initialize your instance
bool SceneTitle::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	
	//타이틀 씬 불러오기
	auto CSB_SceneTitle = CSLoader::createNode("Scene_Title.csb");
	addChild(CSB_SceneTitle);

	//태그로 버튼 이미지 가져오기
	ButtonStart = (Button*)CSB_SceneTitle->getChildByTag(TAG_BUTTON_PLAY);
	ButtonQuit = (Button*)CSB_SceneTitle->getChildByTag(TAG_BUTTON_EXIT);
	ButtonStartContinue = (Button*)CSB_SceneTitle->getChildByTag(TAG_BUTTON_PLAY_CONTINUE);
	
	//버튼에 터치 이벤트 등록
	ButtonStart->addTouchEventListener(CC_CALLBACK_2(SceneTitle::onTouch, this));
	ButtonQuit->addTouchEventListener(CC_CALLBACK_2(SceneTitle::onTouch, this));
	ButtonStartContinue->addTouchEventListener(CC_CALLBACK_2(SceneTitle::onTouch, this));
	
	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

    return true;
}

void SceneTitle::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_PLAY:
		{
			logManager->tmpFunction(0);

			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			auto scene = TransitionFade::create(0.5, SceneCreator::createScene(0, 1));
			Director::getInstance()->replaceScene(scene);

			break;
		}
		case TAG_BUTTON_PLAY_CONTINUE:
		{
			if (logManager->getPrologue()) {
				logManager->setPrologue();
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				auto scene = TransitionFade::create(0.5, SceneCreator::createScene(0, 1));
				Director::getInstance()->replaceScene(scene);
			}
			else {
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				auto scene = TransitionFade::create(0.5, SceneWorld::createScene());
				Director::getInstance()->replaceScene(scene);
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();	
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();
			appplicationExit();
			break;
		}
		default:
			break;
		
		}

	}
}

void SceneTitle::appplicationExit(){
		Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
		EXIT(0);
#endif
	}