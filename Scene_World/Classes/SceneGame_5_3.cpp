﻿//기본 헤더
#include "SceneGame_5_3.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
//#include <stdlib.h>

//화면 전환
#include "SceneStage_5.h"

USING_NS_CC;
using namespace ui;

bool isReplay53;

SceneGame_5_3::SceneGame_5_3(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;
	hintVisible = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_5.mp3";
}

SceneGame_5_3::~SceneGame_5_3(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_5_3::createScene(bool _booltmp)
{
	isReplay53 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_5_3::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_5_3::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_5_3 = CSLoader::createNode("Scene_Game_5_3.csb");
	addChild(CSB_SceneGame_5_3);
	
	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_5_3->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_5_3->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_5_3->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_5_3::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	/* System Button */

	Button_O = (Button*)CSB_SceneGame_5_3->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_5_3->getChildByTag(TAG_X);
	ButtonConfirm = (Button*)CSB_SceneGame_5_3->getChildByTag(TAG_BUTTON_START);
	ButtonReset = (Button*)CSB_SceneGame_5_3->getChildByTag(TAG_BUTTON_RESET);

	//Add Touch Event

	ButtonConfirm->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));
	ButtonReset->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));

	/* System Button End */

	ButtonHint = (Button*)CSB_SceneGame_5_3->getChildByTag(TAG_BUTTON_HINT);
	SpriteHint = (Sprite*)CSB_SceneGame_5_3->getChildByTag(TAG_SPRITE_HINT);
	TextHint = (TextField*)CSB_SceneGame_5_3->getChildByTag(TAG_HINT_NUM);

	ButtonHint->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));

	TextQuestion = (TextField*)CSB_SceneGame_5_3->getChildByTag(TAG_CIPHER_QUESTION);
	TextAnswer = (TextField*)CSB_SceneGame_5_3->getChildByTag(TAG_CIPHER_ANSWER);

	for (int i = 0; i < 27; i++) {
		ButtonInput[i] = (Button*)CSB_SceneGame_5_3->getChildByTag(TAG_BUTTON_INPUT + i);
		ButtonInput[i]->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_3::onTouch, this));
	}

	//Set Randomize
	srand((unsigned int)time(NULL));
	setRandomQuestion();


	return true;
}

void SceneGame_5_3::setHELLOWORLD(int key) {

	std::string bufText;
	std::string textStr[26] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
	
	//7 4 11 11 14 22 14 17 11 3

	bufText = textStr[7 + key];
	bufText.append(textStr[4 + key]);
	bufText.append(textStr[11 + key]);
	bufText.append(textStr[11 + key]);
	bufText.append(textStr[14 + key]);
	bufText.append(textStr[22 + key]);
	bufText.append(textStr[14 + key]);
	bufText.append(textStr[17 + key]);
	bufText.append(textStr[11 + key]);
	bufText.append(textStr[3 + key]);
	
	TextQuestion->setString(bufText);
}
void SceneGame_5_3::setRandomQuestion(){

	key = rand()%3 + 1;
	if (rand() % 2) {
		key = 0 - key;
	}
	cnt = 0;
	answer = "";
	TextAnswer->setString(answer);
	TextHint->setString(commonLib::to_string(key));
	setHELLOWORLD(key);
	hintVisible = false;

}

bool SceneGame_5_3::isCorrect() {

	if (answer == "HELLOWORLD")
		
		return true;
	else
		return false;
	
	//정답 TextField가 HELLOWORLD인지 확인
}


void SceneGame_5_3::onTouch(Ref* sender, Widget::TouchEventType type){

	if (type == Widget::TouchEventType::ENDED){

		auto SelectButton = (Button*)sender;

		int selectedbuf = SelectButton->getTag();

		switch (selectedbuf)
		{
			//System Button
		case TAG_BUTTON_OPTION:
		{
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);
						
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);
			ButtonConfirm->setTouchEnabled(false);
			ButtonReset->setTouchEnabled(false);
			ButtonHint->setTouchEnabled(false);

			for (int i = 0; i < 26; i++)
				ButtonInput[i]->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners(); 
			
			Scene* scene;

			if (isReplay53) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_5::createScene());
			}
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);
						
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);
			ButtonConfirm->setTouchEnabled(true);
			ButtonReset->setTouchEnabled(true);
			ButtonHint->setTouchEnabled(true);

			for (int i = 0; i < 26; i++)
				ButtonInput[i]->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_BUTTON_START:

			if (isCorrect()) {
				Button_O->setVisible(true);
			}
			else {
				Button_X->setVisible(true);
				flag = false;
			}

			break;
		case TAG_BUTTON_RESET :
			answer = "";
			TextAnswer->setString(answer);
			break;
		case TAG_O:
		{
			Scene* scene;

			Button_O->setVisible(false);
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			if (isReplay53) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				logManager->setChapterProgress(70);
				scene = TransitionFade::create(0.5, SceneCreator::createScene(5, 7));
			}
			Director::getInstance()->replaceScene(scene);
		}
		break;

		case TAG_X:
		{
			Button_X->setVisible(false);
			answer = "";
			TextAnswer->setString(answer);
			setRandomQuestion();
			flag = true;
		}
		break;

		//Game Button
		case TAG_BUTTON_HINT: {
			if (hintVisible == false) {
				//hint 보임
				SpriteHint->setVisible(true);
				TextHint->setVisible(true);
				hintVisible = true;

				Button_O->setTouchEnabled(false);
				Button_X->setTouchEnabled(false);
				ButtonConfirm->setTouchEnabled(false);
				ButtonReset->setTouchEnabled(false);

				for (int i = 0; i < 26; i++)
					ButtonInput[i]->setTouchEnabled(false);
			}
			else if (hintVisible == true) {
				SpriteHint->setVisible(false);
				TextHint->setVisible(false);
				hintVisible = false;

				Button_O->setTouchEnabled(true);
				Button_X->setTouchEnabled(true);
				ButtonConfirm->setTouchEnabled(true);
				ButtonReset->setTouchEnabled(true);

				for (int i = 0; i < 26; i++)
					ButtonInput[i]->setTouchEnabled(true);
			}			
			break;
		}

		case TAG_BUTTON_INPUT:
		case TAG_BUTTON_INPUT + 1:
		case TAG_BUTTON_INPUT + 2:
		case TAG_BUTTON_INPUT + 3:
		case TAG_BUTTON_INPUT + 4:
		case TAG_BUTTON_INPUT + 5:
		case TAG_BUTTON_INPUT + 6:
		case TAG_BUTTON_INPUT + 7:
		case TAG_BUTTON_INPUT + 8:
		case TAG_BUTTON_INPUT + 9:
		case TAG_BUTTON_INPUT + 10:
		case TAG_BUTTON_INPUT + 11:
		case TAG_BUTTON_INPUT + 12:
		case TAG_BUTTON_INPUT + 13:
		case TAG_BUTTON_INPUT + 14:
		case TAG_BUTTON_INPUT + 15:
		case TAG_BUTTON_INPUT + 16:
		case TAG_BUTTON_INPUT + 17:
		case TAG_BUTTON_INPUT + 18:
		case TAG_BUTTON_INPUT + 19:
		case TAG_BUTTON_INPUT + 20:
		case TAG_BUTTON_INPUT + 21:
		case TAG_BUTTON_INPUT + 22:
		case TAG_BUTTON_INPUT + 23:
		case TAG_BUTTON_INPUT + 24:
		case TAG_BUTTON_INPUT + 25:
		{
			if (flag) {
				int buf = selectedbuf - TAG_BUTTON_INPUT;
				std::string textStr[26] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
				if (answer.empty()) {
					answer = textStr[buf];
				}
				else{
					answer = answer + textStr[buf];
				}

				TextAnswer->setString(answer);
				break;
			}
			else {
				Button_X->setVisible(false);
				answer = "";
				TextAnswer->setString(answer);
				setRandomQuestion();
				flag = true;
			}
			
		}
		case TAG_BUTTON_INPUT + 26: {
			if (flag) {
				if (answer.empty()) {}
				else {
					answer.pop_back();
					TextAnswer->setString(answer);
				}
			}
			else {
				Button_X->setVisible(false);
				answer = "";
				TextAnswer->setString(answer);
				setRandomQuestion();
				flag = true;
			}
			
			break;
		}
		default:{ break; }

		}
	}
}

void SceneGame_5_3::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_5_3::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}