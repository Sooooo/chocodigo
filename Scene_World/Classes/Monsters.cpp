﻿#include "Monsters.h"

using namespace std;

Monsters::Monsters()
{
	createMonster(0, "Hidden Boss", 100, "Battle_System/BOSS.png");	
	createMonster(1, "변해버린 산타", 20, "Game_Resource/script_system/character/2_santa.png");
	createMonster(2, "새까만 까마귀", 30, "Game_Resource/script_system/character/3_crow.png");
	createMonster(3, "사나운 상어", 40, "Game_Resource/script_system/character/4_write.png");
	createMonster(4, "나태한 작가", 50, "Game_Resource/script_system/character/5_shark.png");
	createMonster(5, "사이비 교주", 60, "Game_Resource/script_system/character/6_cleric.png");
	createMonster(6, "마지막 사탄", 70, "Game_Resource/script_system/character/7_satan.png");
}

monster* Monsters::getMonster(int index)
{
	return &(Monster[index]);
}

void Monsters::createMonster(int index, string name, int hp, string imgSrc)
{
	Monster[index].setName(name);
	Monster[index].setHP(hp);
	Monster[index].setChangeHP(hp);
	Monster[index].setImgSrc(imgSrc);
}