﻿//기본 헤더
#include "SceneGame_3_3_2.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"

//#include <ctime>
//#include <cstdlib>
//#include <math.h>

//화면 전환
#include "SceneGame_3_3.h"

USING_NS_CC;
using namespace ui;

bool isReplay332;

SceneGame_3_3_2::SceneGame_3_3_2(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_3.mp3";

	ProblemNum = 1;
}

SceneGame_3_3_2::~SceneGame_3_3_2()
{
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_3_3_2::createScene(bool _bool)
{
	isReplay332 = _bool;

	auto scene = Scene::create();

	auto layer = SceneGame_3_3_2::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_3_3_2::init()
{

	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_3_3_2 = CSLoader::createNode("Scene_Game_3_3_2.csb");
	addChild(CSB_SceneGame_3_3_2);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_3_3_2->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_3_3_2->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_3_3_2->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneGame_3_3_2::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//버튼 가져오기
	ButtonStart = (Button*)CSB_SceneGame_3_3_2->getChildByTag(TAG_BUTTON_PLAY);
	Button_O = (Button*)CSB_SceneGame_3_3_2->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_3_3_2->getChildByTag(TAG_X);

	ButtonNum1 = (Button*)CSB_SceneGame_3_3_2->getChildByTag(TAG_BUTTON_NUM_1);
	ButtonNum10 = (Button*)CSB_SceneGame_3_3_2->getChildByTag(TAG_BUTTON_NUM_10);

	//스프라이트 가져오기
	Flour = (Sprite*)CSB_SceneGame_3_3_2->getChildByTag(TAG_FLOUR);
	Egg = (Sprite*)CSB_SceneGame_3_3_2->getChildByTag(TAG_EGG);
	Milk = (Sprite*)CSB_SceneGame_3_3_2->getChildByTag(TAG_MILK);
	Q1 = (Sprite*)CSB_SceneGame_3_3_2->getChildByTag(TAG_Q1_SQUARE);
	Q2_1= (Sprite*)CSB_SceneGame_3_3_2->getChildByTag(TAG_Q2_SQUARE);
	Q2_2 = (Sprite*)CSB_SceneGame_3_3_2->getChildByTag(TAG_Q2_CIRCLE_2);
	Q3_1 = (Sprite*)CSB_SceneGame_3_3_2->getChildByTag(TAG_Q3_CIRCLE_1);
	Q3_2 = (Sprite*)CSB_SceneGame_3_3_2->getChildByTag(TAG_Q3_DIAMOND);

	//텍스트필드 가져오기
	QuestCount = (TextField*)CSB_SceneGame_3_3_2->getChildByTag(TAG_PROBLEM_STATE);
	InputNum1 = (TextField*)CSB_SceneGame_3_3_2->getChildByTag(TAG_INPUT_NUM_1);
	InputNum2 = (TextField*)CSB_SceneGame_3_3_2->getChildByTag(TAG_INPUT_NUM_2);
	InputNum3 = (TextField*)CSB_SceneGame_3_3_2->getChildByTag(TAG_INPUT_NUM_3);
	Text_1 = (TextField*)CSB_SceneGame_3_3_2->getChildByTag(TAG_TEXT_NUM_1);
	Text_10 = (TextField*)CSB_SceneGame_3_3_2->getChildByTag(TAG_TEXT_NUM_10);

	ProbNum1 = (TextField*)CSB_SceneGame_3_3_2->getChildByTag(TAG_PROBNUM_1);
	ProbNum2 = (TextField*)CSB_SceneGame_3_3_2->getChildByTag(TAG_PROBNUM_2);
	ProbNum3 = (TextField*)CSB_SceneGame_3_3_2->getChildByTag(TAG_PROBNUM_3);

	//터치이벤트 추가
	ButtonStart->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	ButtonNum1->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	ButtonNum10->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_2::onTouch, this));
	
	problemInit();

	return true;
}

void SceneGame_3_3_2::problemInit(){

	Input1 = 0;
	Input2 = 0;
	Input3 = 0;
	UserAnswer = 0;

	srand((unsigned int)time(NULL));

	Input1 = rand() % 9 + 1;
	Input2 = rand() % 9 + 1;

	ProbNum3->setVisible(false);
	InputNum3->setVisible(false);

	//2,3 문제일때 입력 수 3개
	if (ProblemNum > 1){
		Input3 = rand() % 10 + 2;
		InputNum3->setString(commonLib::to_string(Input3));
		ProbNum3->setString(commonLib::to_string(Input3));
		InputNum3->setVisible(true);
		ProbNum3->setVisible(true);
	}

	if (ProblemNum == 3){
		Input1 = rand() % 4 + 2;
		Input2 = rand() % 3 + 2;
	}

	InputNum1->setString(commonLib::to_string(Input1));
	InputNum2->setString(commonLib::to_string(Input2));
	ProbNum1->setString(commonLib::to_string(Input1));
	ProbNum2->setString(commonLib::to_string(Input2));
	
	Button_O->setVisible(false);
	Button_X->setVisible(false);

	QuestCount->setString(commonLib::to_string(ProblemNum));

	//문제출제
	if (ProblemNum == 1){
		Flour->setVisible(true);
		Q1->setVisible(true);
		Q2_1->setVisible(false);
		Q2_2->setVisible(false);
		Q3_1->setVisible(false);
		Q3_2->setVisible(false);
	}
	else if (ProblemNum ==2){
		Flour->setVisible(false);
		Milk->setVisible(true);
		Q1->setVisible(false);
		Q2_1->setVisible(true);
		Q2_2->setVisible(true);
		Q3_1->setVisible(false);
		Q3_2->setVisible(false);
	}
	else if (ProblemNum == 3){
		Milk->setVisible(false);
		Egg->setVisible(true);
		Q1->setVisible(false);
		Q2_1->setVisible(false);
		Q2_2->setVisible(false);
		Q3_1->setVisible(true);
		Q3_2->setVisible(true);
	}



	//답칸 0 초기화
	UserAnswerInit();
	//정답계산
	ProblemAnswerCheck();
	
}

void SceneGame_3_3_2::ProblemAnswerCheck(){
	
	//문제마다 계산
	if (ProblemNum == 1){
		CorrectAnswer = Input1 * Input2;
	}
	else if (ProblemNum == 2){
		CorrectAnswer = Input1 * Input2 + Input3;
	}
	else if (ProblemNum == 3){
		CorrectAnswer = pow(Input1, Input2) / Input3;
	}
	
}

void SceneGame_3_3_2::UserAnswerInit(){
	NumTen = 0;
	NumOne = 0;

	Text_1->setString(commonLib::to_string(NumOne));
	Text_10->setString(commonLib::to_string(NumTen));
}

void SceneGame_3_3_2::play()
{
	if (CorrectAnswer == UserAnswer)	//정답인 경우
	{
		Button_O->setVisible(true);
		ProblemNum++;
	}
	else	//오답인 경우
	{
		Button_X->setVisible(true);

	}

}

void SceneGame_3_3_2::UserAnswerCheck(){
	UserAnswer = (NumTen * 10) + NumOne;
}


void SceneGame_3_3_2::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){
		

		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			ButtonStart->setTouchEnabled(false);
			ButtonNum1->setTouchEnabled(false);
			ButtonNum10->setTouchEnabled(false);
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:{
			
			auto scene = TransitionFade::create(0.5, SceneGame_3_3::createScene(isReplay332));
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonStart->setTouchEnabled(true);
			ButtonNum1->setTouchEnabled(true);
			ButtonNum10->setTouchEnabled(true);
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_BUTTON_PLAY:{
			UserAnswerCheck();
			play();

			break;
		}
		case TAG_O:{
			
			Button_O->setVisible(false);
			Scene* scene;

			if (isReplay332) {
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
				Director::getInstance()->replaceScene(scene);
			}
			else {
				if (ProblemNum > 3){
					logManager->setChapterProgress(60);
					logManager->setQuest(8);
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					auto scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 9));
					Director::getInstance()->replaceScene(scene);
				}
				else
				{
					problemInit();
				}
			}
			break;
		}
		case TAG_X:{
			
			Button_X->setVisible(false);
			break;
		}
		case TAG_BUTTON_NUM_1:{
			if (NumOne == 9){
				NumOne = 0;
			}
			else{ NumOne++; }
			Text_1->setString(commonLib::to_string(NumOne));
			
			break;
		}
		case TAG_BUTTON_NUM_10:{
			if (NumTen == 9){
				NumTen = 0;
			}
			else{ NumTen++; }
			Text_10->setString(commonLib::to_string(NumTen));

			break;
		}

		default:
			break;
		}
	}
}

void SceneGame_3_3_2::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_3_3_2::ScheduleAudio(float dt)
{
	if (FlagAudio == true)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}