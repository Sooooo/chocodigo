﻿//기본 헤더
#include "SceneGame_3_1_2.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"
//#include <ctime>

//화면 전환
#include "SceneStage_3.h"

USING_NS_CC;
using namespace ui;

SceneGame_3_1_2::SceneGame_3_1_2(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_3.mp3";
	
	ProblemNum = 1;
}

SceneGame_3_1_2::~SceneGame_3_1_2()
{
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_3_1_2::createScene()
{
	auto scene = Scene::create();

	auto layer = SceneGame_3_1_2::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_3_1_2::init()
{

	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_3_1_2 = CSLoader::createNode("Scene_Game_3_1_2.csb");
	addChild(CSB_SceneGame_3_1_2);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_3_1_2->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_3_1_2->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_3_1_2->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneGame_3_1_2::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//스프라이트 및 버튼 가져오기
	Rabbit = (Sprite*)CSB_SceneGame_3_1_2->getChildByTag(TAG_RABBIT);
	Cat = (Sprite*)CSB_SceneGame_3_1_2->getChildByTag(TAG_CAT);
	
	ButtonConfirm = (Button*)CSB_SceneGame_3_1_2->getChildByTag(TAG_BUTTON_CONFIRM);
	ButtonZero = (Button*)CSB_SceneGame_3_1_2->getChildByTag(TAG_BUTTON_ZERO);
	ButtonOne = (Button*)CSB_SceneGame_3_1_2->getChildByTag(TAG_BUTTON_ONE);
	Button_O = (Button*)CSB_SceneGame_3_1_2->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_3_1_2->getChildByTag(TAG_X);	

	//텍스트 필드 가져오기
	ProblemState = (TextField*)CSB_SceneGame_3_1_2->getChildByTag(TAG_PROBLEM_STATE);
	ProblemWindow = (TextField*)CSB_SceneGame_3_1_2->getChildByTag(TAG_PROBLEM_WINDOW);
	TextBinary = (TextField*)CSB_SceneGame_3_1_2->getChildByTag(TAG_BINARY);

	//터치이벤트 추가
	ButtonConfirm->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));
	ButtonZero->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));
	ButtonOne->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_1_2::onTouch, this));

	problemInit();

	return true;
}

void SceneGame_3_1_2::problemInit()
{
	//퀘스트 번호 초기화
	ProblemState->setString(commonLib::to_string(ProblemNum));

	//O, X 안보이게
	Button_O->setVisible(false);
	Button_X->setVisible(false);

	ButtonConfirm->setVisible(true);

	srand((unsigned int)time(NULL));

	//문제 초기화
	std::string ConvertString;

	for (int Index = 0; Index < 6; Index++)
	{
		bool NumFlag = rand() % 2;
		Problem[Index] = NumFlag;
		ConvertString += commonLib::to_string(Problem[Index]);
	}

	ProblemWindow->setString(ConvertString);

	//동물 초기화
	bool AnimalFlag = rand() % 2;

	if (AnimalFlag == 0)
	{
		Cat->setVisible(true);
		Rabbit->setVisible(false);
	}
	else if (AnimalFlag == 1)
	{
		Cat->setVisible(false);
		Rabbit->setVisible(true);
	}
	
	//답칸 초기화
	UserAnswerInit();

	//정답 계산
	ProblemAnswerCheck();

}

void SceneGame_3_1_2::ProblemAnswerCheck()
{
	int count = 0;

	for (int i = 0; i < 6; i++){
		if (Problem[i] == 1){
			count++;
		}
	}

	if (Cat->isVisible()){
		//Cat은 1이 짝수개

		if (count % 2 == 0)
		{
			ProblemAnswer = 0;
		}
		else{
			ProblemAnswer = 1;
		}

	}
	else if (Rabbit->isVisible())
	{
		//Rabbit은 1이 홀수개

		if (count % 2 == 0)
		{
			ProblemAnswer = 1;
		}
		else{
			ProblemAnswer = 0;
		}
	}


}


void SceneGame_3_1_2::UserAnswerInit()
{
	TextBinary->setString("");
}

void SceneGame_3_1_2::UserAnswerCheck()
{
	if (TextBinary->getString() == "0")
	{
		UserAnswer = 0;
	}
	else if (TextBinary->getString() == "1")
	{
		UserAnswer = 1;
	}
	else {
		UserAnswer = -1;
	}
}

void SceneGame_3_1_2::play()
{
	ButtonConfirm->setVisible(false);
	if (UserAnswer == ProblemAnswer)	//정답인 경우
	{
		Button_O->setVisible(true);

		ProblemNum++;
	}
	else if (UserAnswer != ProblemAnswer)	//오답인 경우
	{
		Button_X->setVisible(true);
	}

}

void SceneGame_3_1_2::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED)
	{
		

		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			ButtonConfirm->setTouchEnabled(false);
			ButtonZero->setTouchEnabled(false);
			ButtonOne->setTouchEnabled(false);
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			
			auto scene = TransitionFade::create(0.5, SceneStage_3::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonConfirm->setTouchEnabled(true);
			ButtonZero->setTouchEnabled(true);
			ButtonOne->setTouchEnabled(true);
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_BUTTON_CONFIRM:
		{
			UserAnswerCheck();
			play();
			break;
		}
		case TAG_BUTTON_ZERO:
		{
			TextBinary->setString("0");

			break;
		}
		case TAG_BUTTON_ONE:
		{
			TextBinary->setString("1");
			break;
		}
		case TAG_O:
		{
			

			if (ProblemNum > 3)	//문제를 다 푼경우
			{
				logManager->setChapterProgress(40);
				logManager->setQuest(7);
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				auto scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 5));
				Director::getInstance()->replaceScene(scene);
			}
			else
			{
				problemInit();
				UserAnswerInit();
			}

			break;
		}
		case TAG_X:
		{
			

			Button_X->setVisible(false);
			UserAnswerInit();
			ButtonConfirm->setVisible(true);

			break;
		}
		default:
			break;
		}
	}
}

void SceneGame_3_1_2::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_3_1_2::ScheduleAudio(float dt)
{
	if (FlagAudio == true)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}





