﻿//기본 헤더
#include "SceneGame_4_2.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"
#include <ctime>

//화면 전환
#include "SceneStage_4.h"
#include "SceneCreator.h"

USING_NS_CC;
using namespace ui;

bool isReplay42;

SceneGame_4_2::SceneGame_4_2(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_4.mp3";
}

SceneGame_4_2::~SceneGame_4_2(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_4_2::createScene(bool _booltmp)
{
	isReplay42 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_4_2::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_4_2::init()
{

	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_4_2 = CSLoader::createNode("Scene_Game_4_2.csb");
	addChild(CSB_SceneGame_4_2);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_4_2->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_4_2->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_4_2->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneGame_4_2::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//버튼 가져오기
	ButtonConfirm = (Button*)CSB_SceneGame_4_2->getChildByTag(TAG_BUTTON_CONFIRM);
	Button_O = (Button*)CSB_SceneGame_4_2->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_4_2->getChildByTag(TAG_X);

	for (int i = 0; i < 4; i++){
		Check[i] = (CheckBox*)CSB_SceneGame_4_2->getChildByTag(TAG_CHECK_1 + i);
		People[i] = (CheckBox*)CSB_SceneGame_4_2->getChildByTag(TAG_MAN_1 + i);
	}

	//텍스트 필드 가져오기
	TxtChat = (TextField*)CSB_SceneGame_4_2->getChildByTag(TAG_TXT_CHAT);

	//터치이벤트 추가
	ButtonConfirm->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));

	for (int i = 0; i < 4; i++){
		Check[i]->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));
		People[i]->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_2::onTouch, this));
	}

	Button_O->setVisible(false);
	Button_X->setVisible(false);

	ButtonConfirm->setVisible(true);

	return true;
}

void SceneGame_4_2::Chat(int tag)
{
	
	std::string chat[4] = {
		"이 사건과 연관이 있는 사람은 ‘나영’이야.\n",
		"이 사건과 연관이 있는 사람은 ‘라영’이야\n",
		"나는 이 사건과 연관이 없어.\n",
		" ‘나영’이는 거짓말을 하고 있어.\n"
	};

	std::string s = chat[tag - TAG_MAN_1];
	
	TxtChat->setString(s);
}

void SceneGame_4_2::UserAnswerInit()
{
	for (int i = 0; i < 4; i++){
		Check[i]->setEnabled(true);
	}	
}

void SceneGame_4_2::UserAnswerCheck()
{
	for (int i = 0; i < 4; i++){
		if (Check[i]->isSelected() == true){
			UserAnswer = i;
			break;
		}
	}
}

void SceneGame_4_2::play()
{
	ProblemAnswer = 2;

	ButtonConfirm->setVisible(false);
	if (UserAnswer == ProblemAnswer)	//정답인 경우
	{
		Button_O->setVisible(true);
	}
	else if (UserAnswer != ProblemAnswer)	//오답인 경우
	{
		Button_X->setVisible(true);
	}

}

void SceneGame_4_2::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED)
	{
		

		auto SelectButton = (Button*)sender;
		int Tag = SelectButton->getTag();

		switch (Tag)
		{
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			for (int i = 0; i < 4; i++)
			{
				Check[i]->setTouchEnabled(false);
				People[i]->setTouchEnabled(false);
			}

			ButtonConfirm->setTouchEnabled(false);
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			
			Scene* scene;

			if (isReplay42) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_4::createScene());
			}
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			for (int i = 0; i < 4; i++)
			{
				Check[i]->setTouchEnabled(true);
				People[i]->setTouchEnabled(true);
			}

			ButtonConfirm->setTouchEnabled(true);
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_BUTTON_CONFIRM:
		{
			UserAnswerCheck();
			play();
			break;
		}		
		case TAG_O:
		{
			
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			Scene* scene;

			if (isReplay42) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				logManager->setChapterProgress(30);
				
				scene = TransitionFade::create(0.5, SceneCreator::createScene(4, 4));
			}
			Director::getInstance()->replaceScene(scene);

			break;
		}
		case TAG_X:
		{
			

			Button_X->setVisible(false);
			UserAnswerInit();
			ButtonConfirm->setVisible(true);

			break;
		}
		case TAG_CHECK_1:
		case TAG_CHECK_2:
		case TAG_CHECK_3:
		case TAG_CHECK_4:
		{
			for (int i = 0; i < 4; i++){
				if (Tag == TAG_CHECK_1 + i){
					//Check[i]->setSelected(true);
				}
				else{
					Check[i]->setSelected(false);
				}
			}

			break;
		}
		case TAG_MAN_1:
		case TAG_MAN_1 + 1:
		case TAG_MAN_1 + 2:
		case TAG_MAN_1 + 3:
		{
			Chat(Tag);
			break;
		}
		default:
			break;
		}
	}
}

void SceneGame_4_2::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_4_2::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}