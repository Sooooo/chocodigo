﻿//기본 헤더
#include "SceneGame_2_2_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"

//#include <ctime>

//화면 전환
#include "SceneStage_2.h"

USING_NS_CC;
using namespace ui;

bool isReplay221;

SceneGame_2_2_1::SceneGame_2_2_1(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_2.mp3";

	ProblemNum = 1;
}

SceneGame_2_2_1::~SceneGame_2_2_1(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_2_2_1::createScene(bool _booltmp)
{
	isReplay221 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_2_2_1::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_2_2_1::init()
{
	if (!Layer::init())
	{
		return false;
	}
	

	//불러오기
	auto CSB_SceneGame_2_2_1 = CSLoader::createNode("Scene_Game_2_2_1.csb");
	addChild(CSB_SceneGame_2_2_1);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_2_2_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_2_2_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_2_2_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_2_2_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//별 가져오기
	for (int Index = 0; Index < 9; Index++){
		int Tag = Index + TAG_STAR_1;
		Stars[Index] = (Star*)CSB_SceneGame_2_2_1->getChildByTag(Tag);
	}

	//버튼 및 텍스트 필드 가져오기 
	ButtonConfirm = (Button*)CSB_SceneGame_2_2_1->getChildByTag(TAG_BUTTON_CONFIRM);
	Button_O = (Button*)CSB_SceneGame_2_2_1->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_2_2_1->getChildByTag(TAG_X);
	ProblemState = (TextField*)CSB_SceneGame_2_2_1->getChildByTag(TAG_PROBLEM_STATE);
	Operation = (TextField*)CSB_SceneGame_2_2_1->getChildByTag(TAG_OPERATION);
	
	//터치 이벤트 등록
	ButtonConfirm->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_1::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_1::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_1::onTouch, this));

	problemInit();

	return true;
}

void SceneGame_2_2_1::setProblemNum(int Index, bool Value)
{
	ProblemAnswer[Index] = Value;
}

void SceneGame_2_2_1::problemInit()
{
	//퀘스트 번호 초기화
	ProblemState->setString(commonLib::to_string(ProblemNum));

	//O, X 안보이게
	Button_O->setVisible(false);
	Button_X->setVisible(false);

	//문제 별 초기화 및 연산자 초기화
	srand((unsigned int)time(NULL));
		
	for (int Index = 0; Index < 6; Index++)
	{
		bool StarFlag = rand() % 2;

		Stars[Index]->setSelected(StarFlag);
	}

	bool OpFlag = rand() % 2;
	std::string Op;

	if (OpFlag == 0)
	{
		Operation->setString("And");
		Op = "And";
	}
	else if (OpFlag == 1)
	{
		Operation->setString("Or");
		Op = "Or";
	}
	
	//답칸의 별 초기화
	UserAnswerInit();

	//정답 계산
	for (int Index = 0; Index < 3; Index++)
	{
		int num1 = Stars[Index]->isSelected();
		int num2 = Stars[Index + 3]->isSelected();

		if (Op == "And")
		{
			setProblemNum(Index, num1&&num2);
		}
		else if (Op == "Or")
		{
			setProblemNum(Index, num1 || num2);
		}	
	}	
}

void SceneGame_2_2_1::UserAnswerInit()
{
	Button_X->setVisible(false);

	for (int Index = 0; Index < 3; Index++)
	{
		Stars[Index + 6]->setSelected(false);
	}
}

bool SceneGame_2_2_1::UserAnswerCheck()
{
	for (int Index = 0; Index < 3; Index++)
	{
		UserAnswer[Index] = Stars[Index + 6]->isSelected();

		if (UserAnswer[Index] != ProblemAnswer[Index])
		{
			return false;
		}
	}

	return true;
}


void SceneGame_2_2_1::play(bool Ck)
{
	ButtonConfirm->setVisible(false);
	if (Ck == true)	//정답인 경우
	{
		Button_O->setVisible(true);

				
		ProblemNum++;		 	
	}
	else if (Ck == false)	//오답인 경우
	{
		Button_X->setVisible(true);

	}

}

void SceneGame_2_2_1::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		
		auto SelectButton = (Button*)sender;

		ButtonConfirm->setVisible(true);

		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();

			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_BACK:{
			
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);

			Scene* scene;

			if (isReplay221) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
				Director::getInstance()->replaceScene(scene);
			}
			else {				
				scene = TransitionFade::create(0.5, SceneStage_2::createScene());
				Director::getInstance()->replaceScene(scene);
			}
			
			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			for (int i = 0; i < 9; i++)
				Stars[i]->setTouchEnabled(false);

			ButtonConfirm->setTouchEnabled(false);
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);

			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			for (int i = 0; i < 9; i++)
				Stars[i]->setTouchEnabled(true);

			ButtonConfirm->setTouchEnabled(true);
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
			case TAG_BUTTON_CONFIRM:
			{
				play(UserAnswerCheck());
				break;
			}
			case TAG_O:
			{
				

				if (isReplay221) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					auto scene = TransitionFade::create(0.5, QuestReplay::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				else {
					if (ProblemNum > 3)	//문제를 다 푼경우
					{
						Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
						logManager->setChapterProgress(50);
						logManager->setQuest(5);
						auto scene = TransitionFade::create(0.5, SceneCreator::createScene(2, 5));
						Director::getInstance()->replaceScene(scene);
					}
					else
					{
						problemInit();
						UserAnswerInit();
					}
				}
				break;
			}
			case TAG_X:
			{
				

				UserAnswerInit();
				ButtonConfirm->setVisible(true);
				break;
			}
			default:
			{
				break;
			}
		}

	}
}

void SceneGame_2_2_1::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_2_2_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);
	}
}


