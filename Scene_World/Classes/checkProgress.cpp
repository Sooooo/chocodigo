﻿#include "checkProgress.h"


checkProgress::checkProgress(){
	// 진행도에 따른 flag 정해줘야함
}
checkProgress::~checkProgress() {
}

void checkProgress::checkChapter1(int tmp) {
	switch(tmp) {
		case 0 :
		case 10:
		case 50:
		case 80:
		case 90:
			this->noticeFlag[0] = true;
			this->noticeFlag[1] = false;
			this->noticeFlag[2] = false;
			this->noticeFlag[3] = false;
			break;
		case 20:
		case 35:
		case 60:
		case 70:
			this->noticeFlag[0] = false;
			this->noticeFlag[1] = true;
			this->noticeFlag[2] = false;
			this->noticeFlag[3] = false;
			break;
		case 25:
			this->noticeFlag[0] = false;
			this->noticeFlag[1] = false;
			this->noticeFlag[2] = true;
			this->noticeFlag[3] = false;
			break;
		case 30:
			this->noticeFlag[0] = false;
			this->noticeFlag[1] = false;
			this->noticeFlag[2] = false;
			this->noticeFlag[3] = true;
			break;
		case 40:
			this->noticeFlag[0] = false;
			this->noticeFlag[1] = true;
			this->noticeFlag[2] = true;
			this->noticeFlag[3] = true;
			break;
		default :
			break;
	};
}

void checkProgress::checkChapter2(int tmp) {
	switch (tmp) {
	case 0:
	case 10:
	case 20:
	case 21:
	case 22:
	case 23:
	case 24:
	case 25:
		noticeFlag[0] = false;
		noticeFlag[1] = true;
		noticeFlag[2] = false;
		noticeFlag[3] = false;
		break;
	case 30:
	case 40:
		noticeFlag[0] = false;
		noticeFlag[1] = false;
		noticeFlag[2] = true;
		noticeFlag[3] = false;
		break;
	case 50:
	case 60 :
		noticeFlag[0] = false;
		noticeFlag[1] = false;
		noticeFlag[2] = false;
		noticeFlag[3] = true;
		break;
	case 70:
	case 80:
		noticeFlag[0] = true;
		noticeFlag[1] = false;
		noticeFlag[2] = false;
		noticeFlag[3] = false;
		break;
	default:
		break;

	}
}

void checkProgress::checkChapter3(int tmp) {
	switch (tmp) {
	case 0:
	case 10:
		noticeFlag[0] = false;
		noticeFlag[1] = false;
		noticeFlag[2] = true;
		break;
	case 20:
	case 30:
	case 35:
		noticeFlag[0] = true;
		noticeFlag[1] = false;
		noticeFlag[2] = false;
		break;
	case 40:
	case 45:
	case 50:
	case 55:
	case 60:
		noticeFlag[0] = false;
		noticeFlag[1] = false;
		noticeFlag[2] = true;
		break;	
	case 70:
	case 80:
	case 90:
		noticeFlag[0] = false;
		noticeFlag[1] = true;
		noticeFlag[2] = false;
		break;
		break;
	default:
		break;

	}
}

void checkProgress::checkChapter4(int tmp) {
	switch (tmp) {
	case 0:
	case 5:
	case 35:
	case 40:
	case 60:
	case 70:
		noticeFlag[0] = true;
		noticeFlag[1] = false;
		break;
	case 10:
	case 20:
	case 50:
		noticeFlag[0] = false;
		noticeFlag[1] = true;
		break;	
	default:
		break;

	}
}

void checkProgress::checkChapter5(int tmp) {
	switch (tmp) {
	case 0:
	case 5:
	case 10:
	case 20:
		noticeFlag[0] = true;
		noticeFlag[1] = false;
		noticeFlag[2] = false;
		break;
	case 25:
	case 70:
	case 80:
		noticeFlag[0] = false;
		noticeFlag[1] = true;
		noticeFlag[2] = false;
		break;
	case 30:
	case 40:
	case 50:
	case 60:
		noticeFlag[0] = false;
		noticeFlag[1] = false;
		noticeFlag[2] = true;
		break;
	default:
		break;

	}
}

void checkProgress::checkChapter6(int tmp) {
	switch (tmp) {
	case 0:
	case 5:
	case 10:
		noticeFlag[0] = true;
		noticeFlag[1] = false;
		noticeFlag[2] = false;
		break;
	case 40:
	case 50:
	case 60:
	case 70:
	case 80:
		noticeFlag[0] = false;
		noticeFlag[1] = true;
		noticeFlag[2] = false;
		break;
	case 20:
	case 25:
	case 30:
		noticeFlag[0] = false;
		noticeFlag[1] = false;
		noticeFlag[2] = true;
		break;
	default:
		break;

	}
}