﻿#ifndef __CHECK_PROGRESS_H__
#define __CHECK_PROGRESS_H__

#include "cocos2d.h"
#include "LogManagement.h"

//#include "SceneCreator.h"
//#include "SceneTitle.h"

/*
namespace Quest_State {
	enum {
		CAN_NOT_START = 0,
		CAN_START_BUT_YET = 1,
		NOW_DOING = 5,
		ALREADY_FINISHED = 10,
	};
}*/

using namespace cocos2d;

class checkProgress {

	private:

		//item은 bool형 배열로 선언 예정
		//bool chapterProgress[7][10][4];
		bool noticeFlag[4];

	public:

		checkProgress();
		~checkProgress();

		//bool getChapterProgress(int chapter, int progress, int notice) { return this->chapterProgress[chapter][progress][notice]; }

		void checkChapter1(int tmp);
		void checkChapter2(int tmp);
		void checkChapter3(int tmp);
		void checkChapter4(int tmp);
		void checkChapter5(int tmp);
		void checkChapter6(int tmp);
		bool getNoticeFlag(int i){ return this->noticeFlag[i];};
};

#endif