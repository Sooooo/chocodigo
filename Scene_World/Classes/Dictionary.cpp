﻿#pragma execution_character_set("UTF-8")

#include "Dictionary.h"

Dic::Dic()
{
	dic[0] = "갈퀴나물"; dic[1] = "개미취"; dic[2] = "금난초";
	dic[3] = "난쟁이붓꽃"; dic[4] = "노루귀"; dic[5] = "능소화";
	dic[6] = "더덕"; dic[7] = "도루박이"; dic[8] = "둥굴레";
	dic[9] = "라일락"; dic[10] = "렌즈콩"; dic[11] = "로즈마리";
	dic[12] = "말나리"; dic[13] = "머위"; dic[14] = "미나리냉이";
	dic[15] = "바람꽃"; dic[16] = "부들"; dic[17] = "비수리";
	dic[18] = "산고들빼기"; dic[19] = "생강나무"; dic[20] = "솔나리";
	dic[21] = "애기똥풀"; dic[22] = "연령초"; dic[23] = "우산나물";
	dic[24] = "장대나물"; dic[25] = "제비꽃"; dic[26] = "지리고들빼기";
	dic[27] = "참당귀"; dic[28] = "창포"; dic[29] = "초롱꽃";
	dic[30] = "칼잎용담"; dic[31] = "콩제비꽃"; dic[32] = "큰금계국";
	dic[33] = "탑꽃"; dic[34] = "토끼풀"; dic[35] = "투구꽃";
	dic[36] = "패랭이꽃"; dic[37] = "풍란"; dic[38] = "피막이";
	dic[39] = "하늘타리"; dic[40] = "해란초"; dic[41] = "흰진교";

}