﻿#pragma execution_character_set("UTF-8")

#ifndef __JSON_H__
#define __JSON_H__

#include "cocos2d.h"

#include "json/rapidjson.h"
#include "json/document.h"
//#include <string.h>
#include "CommonLib.h"

USING_NS_CC;
using namespace rapidjson;

class Json {
private:

	int ChapterNum;
	int SceneNum;
	Document doc;
	std::string ObjectName;

public :

	Json();
	~Json();

	bool openfile(int c, int s, std::string str);

	std::string getScriptData(std::string content);
	std::string getScriptData(int i, std::string content);
	bool checkNext(int i);
	int getIntParameter(int i);
	int getArraySize();
	int getProgressPara(int i);

	std::string getQuestData(int n, std::string str);
};

#endif