﻿//기본 헤더
#include "SceneGame_3_2.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
//#include <stdlib.h>

//화면 전환
#include "SceneStage_3.h"

USING_NS_CC;
using namespace ui;

bool isReplay32;
int gameCNT = 0;

SceneGame_3_2::SceneGame_3_2(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_3.mp3";
}

SceneGame_3_2::~SceneGame_3_2()
{
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}


Scene* SceneGame_3_2::createScene(bool _bool)
{
	isReplay32 = _bool;

	auto scene = Scene::create();

	auto layer = SceneGame_3_2::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_3_2::init()
{
	if (!Layer::init())
	{
		return false;
	}
	
	auto CSB_SceneGame_3_2 = CSLoader::createNode("Scene_Game_3_2.csb");
	addChild(CSB_SceneGame_3_2);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_3_2->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_3_2->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_3_2->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneGame_3_2::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	Button_O = (Button*)CSB_SceneGame_3_2->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_3_2->getChildByTag(TAG_X);
	ButtonConfirm = (Button*)CSB_SceneGame_3_2->getChildByTag(TAG_BUTTON_START);

	//Add Touch Event
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));
	ButtonConfirm->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));

	for (int i = 0; i < 3; i++) {
		SpriteQuestion[i] = (Sprite*)CSB_SceneGame_3_2->getChildByTag(TAG_SPRITE + i);
		SpriteQuestion[i]->setVisible(false);
	}
	for (int i = 0; i < 12; i++) {
		ButtonAnswer[i] = (Button*)CSB_SceneGame_3_2->getChildByTag(TAG_BUTTON + i);
		ButtonAnswer[i]->setVisible(false);
	}

	selAnswer = 0;


	SpriteQuestion[gameCNT]->setVisible(true);
	for (int i = 0; i < 4; i++) {
		if (gameCNT > 0) {
			ButtonAnswer[(gameCNT - 1) * 4 + i]->setVisible(false);
			ButtonAnswer[(gameCNT - 1) * 4 + i]->setTouchEnabled(false);
		}
		ButtonAnswer[gameCNT * 4 + i]->setVisible(true);
		ButtonAnswer[gameCNT * 4 + i]->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_2::onTouch, this));
	}

	return true;
}


bool SceneGame_3_2::isCorrect() {
	
	switch (gameCNT) {
	case 0:
		if (1 == selAnswer)
			return true;
		break;
	case 1:
		if (0 == selAnswer)
			return true;
		break;
	case 2:
		if (3 == selAnswer)
			return true;
		break;
	}
	
	return false;
}

void SceneGame_3_2::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){

		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
		case 711:
		case 715:
		case 719:
			ButtonAnswer[gameCNT * 4 + selAnswer]->setTouchEnabled(true);
			ButtonAnswer[gameCNT * 4 + selAnswer]->setBright(true);
			ButtonAnswer[gameCNT * 4 + 0]->setTouchEnabled(false);
			ButtonAnswer[gameCNT * 4 + 0]->setBright(false);
			selAnswer = 0;
				break;
		case 712:
		case 716:
		case 720:
			ButtonAnswer[gameCNT * 4 + selAnswer]->setTouchEnabled(true);
			ButtonAnswer[gameCNT * 4 + selAnswer]->setBright(true);
			ButtonAnswer[gameCNT * 4 + 1]->setTouchEnabled(false);
			ButtonAnswer[gameCNT * 4 + 1]->setBright(false);
			selAnswer = 1; 
			break;
		case 713 :
		case 717 :
		case 721:
			ButtonAnswer[gameCNT * 4 + selAnswer]->setTouchEnabled(true);
			ButtonAnswer[gameCNT * 4 + selAnswer]->setBright(true);
			ButtonAnswer[gameCNT * 4 + 2]->setTouchEnabled(false);
			ButtonAnswer[gameCNT * 4 + 2]->setBright(false);
			selAnswer = 2; 
			break;
		case 714 :
		case 718 :
		case 722:
			ButtonAnswer[gameCNT * 4 + selAnswer]->setTouchEnabled(true);
			ButtonAnswer[gameCNT * 4 + selAnswer]->setBright(true);
			ButtonAnswer[gameCNT * 4 + 3]->setTouchEnabled(false);
			ButtonAnswer[gameCNT * 4 + 3]->setBright(false);
			selAnswer = 3; 
			break;
		
			//System Button
		case TAG_BUTTON_START:

			if (isCorrect()) {
				Button_O->setVisible(true);
			}
			else {
				Button_X->setVisible(true);
			}

			break;

		case TAG_O:  
		{
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			Scene* scene;
			if (gameCNT < 2) {
				gameCNT++;
				ButtonAnswer[gameCNT * 4 + selAnswer]->setTouchEnabled(false);
				ButtonAnswer[gameCNT * 4 + selAnswer]->setBright(false);
				init();
			}
			else {
				if (isReplay32) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
				}
				else {
					Button_O->setVisible(false);
					logManager->setChapterProgress(50);
					logManager->setQuest(8);

					scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 7));
				}
				Director::getInstance()->replaceScene(scene);
			}
			
		}
			break;

		case TAG_X: 
		{
			
			ButtonAnswer[gameCNT * 4 + selAnswer]->setTouchEnabled(false);
			ButtonAnswer[gameCNT * 4 + selAnswer]->setBright(false);
			Button_X->setVisible(false);
			init(); 
		}
			break; 
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			ButtonConfirm->setTouchEnabled(false);
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);

			for (int i = 0; i < 12; i++)
				ButtonAnswer[i]->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			Scene* scene;

			if (isReplay32) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_3::createScene());
			}
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonConfirm->setTouchEnabled(true);
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);

			for (int i = 0; i < 12; i++)
				ButtonAnswer[i]->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		default:{ break; }
			
		}
	}
}

void SceneGame_3_2::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_3_2::ScheduleAudio(float dt)
{
	if (FlagAudio == true)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}