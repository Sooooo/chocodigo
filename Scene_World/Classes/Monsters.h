﻿#ifndef __MONSTERS_H__
#define __MONSTERS_H__

//#include <iostream>
#include "Character.h"

#define monster Character
#define MAX_Monster	7

//SingleTone Design Pattern 사용하는 것으로 바꾸기
class Monsters
{
private:
	monster Monster[MAX_Monster];

public:
	Monsters();
	monster* getMonster(int index);
	void setMonster();
	void createMonster(int index, string name, int hp, string imgSrc);
};

#endif