﻿//기본 헤더
#include "SceneGame_6_3.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
//#include <ctime>

//화면 전환
#include "SceneStage_6.h"

USING_NS_CC;
using namespace ui;

bool isReplay63;

SceneGame_6_3::SceneGame_6_3(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_6.mp3";

}

SceneGame_6_3::~SceneGame_6_3(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_6_3::createScene(bool _booltmp)
{
	isReplay63 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_6_3::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_6_3::init()
{
	if (!Layer::init())
	{
		return false;
	}
	
	//불러오기
	auto CSB_SceneGame_6_3 = CSLoader::createNode("Scene_Game_6_3.csb");
	addChild(CSB_SceneGame_6_3);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_6_3->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_6_3->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_6_3->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_6_3::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//사람 가져오기
	for (int Index = 0; Index < 10; Index++)
	{
		int Tag = Index + TAG_PEOPLE_1;
		People[Index] = (CheckBox*)CSB_SceneGame_6_3->getChildByTag(Tag);
	}

	//버튼 및 텍스트 필드 가져오기 
	ButtonChange = (Button*)CSB_SceneGame_6_3->getChildByTag(TAG_BUTTON_CHANGE);
	ButtonPass = (Button*)CSB_SceneGame_6_3->getChildByTag(TAG_BUTTON_NOT_CHANGE);
	Button_O = (Button*)CSB_SceneGame_6_3->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_6_3->getChildByTag(TAG_X);

	//터치 이벤트 등록
	ButtonChange->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));
	ButtonPass->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));

	for (int Index = 0; Index < 10; Index++)
		People[Index]->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_3::onTouch, this));
	
	problemInit();
	
	return true;
}


void SceneGame_6_3::mixPeople()
{
	//Init
	for (int Index = 0; Index < 10; Index++)
	{
		MixedPeople[Index] = Index;
	}

	//Logical Mix
	srand((unsigned int)time(NULL));

	for (int Index = 9; Index >= 0; Index--)
	{
		int Flag = rand() % 9;
		int Temp = MixedPeople[Index];

		MixedPeople[Index] = MixedPeople[Flag];
		MixedPeople[Flag] = Temp;
	}
	
	//Physical Mix
	Vec2 position[10];
	
	for (int Index = 0; Index < 10; Index++)
		position[Index] = People[Index]->getPosition();

	for (int Index = 0; Index < 10; Index++)
	{
		//일단. 믹스피플의 첫번째에 무슨 값이 들어있는가를 확인해서
		//해당 값의 사람을 그 위치로 옮긴다.
		int target = MixedPeople[Index];
		People[target]->setPosition(position[Index]);
	}
}

void SceneGame_6_3::problemInit()
{
	//O, X 안보이게
	Button_O->setVisible(false);
	Button_X->setVisible(false);
	
	//바꾸는 버튼 활성화
	ButtonChange->setVisible(true);
	ButtonPass->setVisible(true);
	
	//다 터치 안되게 하기
	for (int i = 0; i < 10; i++)
		People[i]->setTouchEnabled(false);
	
	//Selected cancel
	for (int i = 0; i < 10; i++)
		People[i]->setSelected(false);

	//사람 위치 섞기
	mixPeople();

	RepetitionCount = 9;	

	SelectedPeople.set(0, 1);
	People[MixedPeople[0]]->setSelected(true);
	People[MixedPeople[1]]->setSelected(true);
}

void SceneGame_6_3::selectPeople()
{
	if (RepetitionCount == 0)
	{
		ButtonChange->setVisible(false);
		ButtonPass->setVisible(false);
		Button_O->setVisible(true);
	}
	else
	{
		ButtonChange->setVisible(true);
		ButtonPass->setVisible(true);

		//자동으로 한칸씩 밀리며 선택
		if (SelectedPeople.y == RepetitionCount)
		{
			SelectedPeople.set(0, 1);

			for (int i = 0; i < 10; i++)
				People[i]->setSelected(false);

			People[MixedPeople[0]]->setSelected(true);
			People[MixedPeople[1]]->setSelected(true);
			RepetitionCount--;
		}
		else
		{
			int target = SelectedPeople.x;
			People[MixedPeople[target]]->setSelected(false);
			People[MixedPeople[target + 1]]->setSelected(false);

			SelectedPeople.x = SelectedPeople.x + 1;
			SelectedPeople.y = SelectedPeople.y + 1;

			target = SelectedPeople.x;

			People[MixedPeople[target]]->setSelected(true);
			People[MixedPeople[target + 1]]->setSelected(true);
		}
	}	
}


void SceneGame_6_3::swapPeople()
{
	Vec2 address_1, address_2;
	int index_1 = SelectedPeople.x;
	int index_2 = SelectedPeople.y;

	address_1 = People[MixedPeople[index_1]]->getPosition();
	address_2 = People[MixedPeople[index_2]]->getPosition();

	//작은거|큰거 순이라면 바꾼다.
	//큰 값이 우선순위가 높아서 값이 작다.
	int target = SelectedPeople.x;
	if (MixedPeople[target] > MixedPeople[target + 1])
	{
		//swap
		People[MixedPeople[index_1]]->setPosition(address_2);
		People[MixedPeople[index_2]]->setPosition(address_1);		

		int temp = MixedPeople[target];
		MixedPeople[target] = MixedPeople[target + 1];
		MixedPeople[target + 1] = temp;
	}
	else if(MixedPeople[target] < MixedPeople[target + 1])
	{
		//아니라면 X
		ButtonChange->setVisible(false);
		ButtonPass->setVisible(false);
		Button_X->setVisible(true);
	}

	People[MixedPeople[target]]->setSelected(false);
	People[MixedPeople[target + 1]]->setSelected(false);
	
}


void SceneGame_6_3::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		
		auto SelectButton = (Button*)sender;

		ButtonChange->setVisible(true);
		ButtonPass->setVisible(true);

		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			ButtonChange->setTouchEnabled(false);
			ButtonPass->setTouchEnabled(false);
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);
			
			for (int i = 0; i < 10; i++)
				People[i]->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			Scene* scene;

			if (isReplay63) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_6::createScene());
			}
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonChange->setTouchEnabled(true);
			ButtonPass->setTouchEnabled(true);
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);

			for (int i = 0; i < 10; i++)
				People[i]->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_PEOPLE_1:
		case TAG_PEOPLE_2:
		case TAG_PEOPLE_3:
		case TAG_PEOPLE_4:
		case TAG_PEOPLE_5:
		case TAG_PEOPLE_6:
		case TAG_PEOPLE_7:
		case TAG_PEOPLE_8:
		case TAG_PEOPLE_9:
		case TAG_PEOPLE_10:
		{
			int tag = SelectButton->getTag();
			//두명만 선택되게 해야함
			//두명을 선택하게 되면 체인지 버튼 활성화
			//이때 가장 작은애를 선택하지 않은 경우에 X표시
			//아닐 경우 계속 진행하게
			break;
		}
		case TAG_BUTTON_CHANGE:
		{
			swapPeople();
			selectPeople();
			break;
		}
		case TAG_BUTTON_NOT_CHANGE:
		{
			int target = SelectedPeople.x;
			if (MixedPeople[target] < MixedPeople[target + 1])
			{
				selectPeople();
			}
			else if(MixedPeople[target] > MixedPeople[target + 1])
			{
				ButtonChange->setVisible(false);
				ButtonPass->setVisible(false);
				Button_X->setVisible(true);
			}
			break;
		}
		case TAG_O:
		{			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			Scene* scene;

			if (isReplay63) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				logManager->setChapterProgress(80);

				scene = TransitionFade::create(0.5, SceneStage_6::createScene());
			}
				Director::getInstance()->replaceScene(scene);
			break;

		}
		case TAG_X:
		{
			
				
			init();

			break;
		}
		default:
		{
			break;
		}
		}

	}
}


void SceneGame_6_3::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_6_3::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}