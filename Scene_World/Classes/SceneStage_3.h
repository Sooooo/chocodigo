﻿#ifndef __SCENE_STAGE_3_H__
#define __SCENE_STAGE_3_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "checkProgress.h"
#include "SceneCreator.h"

class SceneStage_3 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;
	checkProgress* cP;

	//Map
	Button* Map[3];
	Sprite* Notice[3];
	Button* ButtonPlayerInfo;
public:
	SceneStage_3();
	~SceneStage_3();

	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(SceneStage_3);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};

#endif
