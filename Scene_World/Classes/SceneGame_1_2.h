﻿#ifndef __SCENE_GAME_1_2_H__
#define __SCENE_GAME_1_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "QuestReplay.h"


/******************** 태그 정보 ********************/
#define TAG_BUTTON_EXCHANGE					701
#define TAG_SPRITE_STUFF					710
/**************************************************/

class SceneGame_1_2 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	Button* button[3];
	Sprite* sprite[3][3];
	//710~712
	//713~715
	//716~718

	int flag[3];

	Button* ButtonPlay;
	Button* ButtonReset;

	Button* Button_O;
	Button* Button_X;

public:
	SceneGame_1_2();
	~SceneGame_1_2();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_1_2);
	
	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	bool isCorrect();

};

#endif 