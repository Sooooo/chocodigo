﻿//기본 헤더
#include "SceneGame_3_3.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneStage_3.h"
#include "SceneGame_3_3_1.h"
#include "SceneGame_3_3_2.h"

USING_NS_CC;
using namespace ui;

bool isReplay33;

SceneGame_3_3::SceneGame_3_3(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_3.mp3";
}

SceneGame_3_3::~SceneGame_3_3(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_3_3::createScene(bool _bool)
{
	isReplay33 = _bool;

	auto scene = Scene::create();

	auto layer = SceneGame_3_3::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_3_3::init()
{

	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_3_3 = CSLoader::createNode("Scene_Game_3_3.csb");
	addChild(CSB_SceneGame_3_3);
	
	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_3_3->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_3_3->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_3_3->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneGame_3_3::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//버튼 가져오기
	ButtonCook = (Button*)CSB_SceneGame_3_3->getChildByTag(TAG_BUTTON_COOK);
	ButtonRecipe = (Button*)CSB_SceneGame_3_3->getChildByTag(TAG_BUTTON_RECIPE);

	//터치이벤트 추가
	ButtonCook->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3::onTouch, this));
	ButtonRecipe->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3::onTouch, this));

	return true;
}
void SceneGame_3_3::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){
		

		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			ButtonCook->setTouchEnabled(false);
			ButtonRecipe->setTouchEnabled(false);			

			break;
		}
		case TAG_BUTTON_BACK:{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			Scene* scene;

			if (isReplay33) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_3::createScene());
			}

			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonCook->setTouchEnabled(true);
			ButtonRecipe->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_BUTTON_RECIPE:{
			auto scene = TransitionFade::create(0.5, SceneGame_3_3_1::createScene());
			Director::getInstance()->pushScene(scene);
			break;
		}
		case TAG_BUTTON_COOK:{
			auto scene = TransitionFade::create(0.5, SceneGame_3_3_2::createScene(isReplay33));
			Director::getInstance()->pushScene(scene);
			break;
		}

		default:
			break;
		}
	}
}

void SceneGame_3_3::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_3_3::ScheduleAudio(float dt)
{
	if (FlagAudio == true)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}




