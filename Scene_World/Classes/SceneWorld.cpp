﻿//기본 헤더
#include "SceneWorld.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneTitle.h"
#include "PrologueCreator.h"
#include "SceneCreator.h"
#include "SceneCharacterInfo.h"

USING_NS_CC;
using namespace ui;

SceneWorld::SceneWorld(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM.mp3";
}

SceneWorld::~SceneWorld(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneWorld::createScene()
{
	auto scene = Scene::create();

	auto layer = SceneWorld::create();
	scene->addChild(layer);

	return scene;
}

// on "init" you need to initialize your instance
bool SceneWorld::init()
{
	if (!Layer::init())
	{
		return false;
	}
	
	//Import csb file
	auto CSB_SceneWorld = CSLoader::createNode("Scene_World.csb");
	addChild(CSB_SceneWorld);
	
	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneWorld->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneWorld->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneWorld->getChildByTag(TAG_BUTTON_OPTION);
	
	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	alertWorld[0] = (Sprite*)CSB_SceneWorld->getChildByTag(TAG_STAGE_ALERT);
	alertWorld[1] = (Sprite*)CSB_SceneWorld->getChildByTag(TAG_STAGE_ALERT + 1);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneWorld::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneWorld::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneWorld::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneWorld::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneWorld::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneWorld::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);
	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneWorld::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}	

	//Scroll
	Scroll = (ScrollView*)CSB_SceneWorld->getChildByTag(TAG_SCROLL);

	for (int i = 0; i < 7; i++) {
		Stage[i] = (Button*)Scroll->getChildByTag(TAG_STAGE + i);
		Stage[i]->addTouchEventListener(CC_CALLBACK_2(SceneWorld::onTouch, this));
	}

	int tmp = logManager->getChapter();

	for (int i = 0; i < 7; i++) {
		Stage[i] = (Button*)Scroll->getChildByTag(TAG_STAGE + i);
		Stage[i]->addTouchEventListener(CC_CALLBACK_2(SceneWorld::onTouch, this));
		if (i > (tmp-1) && i > 0) {
			Stage[i]->setTouchEnabled(false);
			Stage[i]->setBright(false);
		}
	}

	//Player Info
	ButtonPlayerInfo = (Button*)CSB_SceneWorld->getChildByTag(TAG_QUEST_INFO);
	ButtonPlayerInfo->addTouchEventListener(CC_CALLBACK_2(SceneWorld::onTouch, this));

	if (logManager->getChapter() == 7) {
		if (logManager->getChapterProgress() < 10) {
			alertWorld[0]->setVisible(true);
			alertWorld[1]->setVisible(false);
		}
		else {
			alertWorld[0]->setVisible(false);
			alertWorld[1]->setVisible(true);
		}
	}
	else {
		alertWorld[0]->setVisible(false);
		alertWorld[1]->setVisible(false);
	}
	return true;
}

void SceneWorld::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		
		auto SelectButton = (Button*)sender;

		int tag = SelectButton->getTag();
		switch (tag)
		{			
			case TAG_BUTTON_OPTION:
			{
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{					
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
					
				}
				else if (Audio->isSelected() == true)
				{					
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}			
			case TAG_BUTTON_BACK:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				auto scene = TransitionFade::create(0.5, SceneTitle::createScene());
				Director::getInstance()->replaceScene(scene);
				
				break;
			}
			case TAG_BUTTON_EXIT:
			{	
				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);

				Scroll->setTouchEnabled(false);
				ButtonPlayerInfo->setTouchEnabled(false);
				for (int i = 0; i < 7; i++)
					Stage[i]->setTouchEnabled(false);					

				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);

				Scroll->setTouchEnabled(true);
				ButtonPlayerInfo->setTouchEnabled(true);

				int chapter = logManager->getChapter();
								
				for (int i = 0; i < 7; i++)
				{
					if (i >(chapter - 1) && i > 0) {
						Stage[i]->setTouchEnabled(false);
						Stage[i]->setBright(false);
					}
					Stage[i]->setTouchEnabled(true);
				}

				break;
			}
			case TAG_EXIT:
			{
				
				//FlagClickExit = true;
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();
				break;
			}
			case TAG_STAGE:
			case TAG_STAGE + 1:
			case TAG_STAGE + 2:
			case TAG_STAGE + 3:
			case TAG_STAGE + 4:
			case TAG_STAGE + 5:
			{
				Scene* scene;
				int stageNum = tag - TAG_STAGE + 1;
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				if (logManager->getChapter() == 7) {
					scene = TransitionFade::create(0.5, SceneCreator::createScene(7, 1));
				}
				else {
					scene = TransitionFade::create(0.5, PrologueCreator::createScene(stageNum));
				}				
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_STAGE + 6:
			{
				Scene* scene;
				int stageNum = tag - TAG_STAGE + 1;
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				/////////////////////// 이부분 stage처럼 처리해야함~
				if (logManager->getChapter() == 7) {
					scene = TransitionFade::create(0.5, SceneCreator::createScene(7, 1));
				}
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_QUEST_INFO:
			{
				auto scene = TransitionFade::create(0.5, SceneCharacterInfo::createScene());
				Director::getInstance()->pushScene(scene);
			}
			default:
				break;
		}

	}
}

void SceneWorld::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneWorld::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{		
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);
		
		FlagAudio = false;
	}
}