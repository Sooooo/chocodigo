﻿#ifndef __SCENE_GAME_1_1_H__
#define __SCENE_GAME_1_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"

/******************** 태그 정보 ********************/
#define TAG_BUTTON_SELECT_VALUE_1_1         701
#define TAG_BUTTON_SELECT_VALUE_1_2         702

#define TAG_BUTTON_SELECT_VALUE_2_1         703
#define TAG_BUTTON_SELECT_VALUE_2_2         704
#define TAG_BUTTON_SELECT_VALUE_2_3         705

#define TAG_BUTTON_SELECT_VALUE_3_1         706
#define TAG_BUTTON_SELECT_VALUE_3_2         707
#define TAG_BUTTON_SELECT_VALUE_3_3         708

#define TAG_SPRITE_CLOTHES					711
#define TAG_SPRITE_PATTERN_SPRITE			712
#define TAG_SPRITE_PATTERN_STAR				713
#define TAG_SPRITE_PATTERN_SNOW				714

class SceneGame_1_1 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	//Game
	Button* ButtonSelect_1_1;
	Button* ButtonSelect_1_2;

	Button* ButtonSelect_2_1;
	Button* ButtonSelect_2_2;
	Button* ButtonSelect_2_3;

	Button* ButtonSelect_3_1;
	Button* ButtonSelect_3_2;
	Button* ButtonSelect_3_3;

	Button* ButtonRun;
	Button* ButtonReset;

	Button* Button_O;
	Button* Button_X;

	Sprite* SpritePatternSnow;
	Sprite* SpritePatternSprite;
	Sprite* SpritePatternStar;
	Sprite* SpriteClothes;

public:	
	SceneGame_1_1();
	~SceneGame_1_1();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_1_1);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	int setRandomPattern();
	int setRandomColor();

	int category, pattern, color;
	int selCategory, selPattern, selColor;

	bool isCorrect();
	int buttonTouch(int, int, int);
};

#endif 