﻿#ifndef __SCENE_GAME_3_1_2_H__
#define __SCENE_GAME_3_1_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "SceneCreator.h"


/*************** 태그 정보 ***************/
#define TAG_RABBIT				701
#define TAG_CAT					702
#define TAG_BUTTON_ZERO			703
#define TAG_BUTTON_ONE			704

#define TAG_PROBLEM_WINDOW		710
#define TAG_BINARY				711
/****************************************/


class SceneGame_3_1_2 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	int ProblemNum;	//문제 번호

	int Problem[6];	//문제
	int ProblemAnswer;	//문제 답
	int UserAnswer;	//사용자 답
	TextField* ProblemState;
	TextField* ProblemWindow;
	TextField* TextBinary;

	Sprite* Rabbit;
	Sprite* Cat;

	Button* ButtonConfirm;
	Button* ButtonZero;
	Button* ButtonOne;

	Button* Button_O;
	Button* Button_X;

public:
	SceneGame_3_1_2();
	~SceneGame_3_1_2();

	static cocos2d::Scene* createScene();
	virtual bool init();
	CREATE_FUNC(SceneGame_3_1_2);

	void problemInit();	//문제 초기화
	void ProblemAnswerCheck();	//정답 계산
	void UserAnswerInit();	//정답 초기화
	void UserAnswerCheck();	//계산 결과 확인
	void play();	//실행하기

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};


#endif 