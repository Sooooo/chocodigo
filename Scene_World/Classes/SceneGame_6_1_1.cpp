﻿// 기본 헤더
#include "SceneGame_6_1_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"

//화면 전환
#include "SceneStage_6.h"
#include "SceneGame_6_1_2.h"

USING_NS_CC;
using namespace ui;

bool isReplay611;

SceneGame_6_1_1::SceneGame_6_1_1(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_6.mp3";
}

SceneGame_6_1_1::~SceneGame_6_1_1(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_6_1_1::createScene(bool _booltmp)
{
	isReplay611 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_6_1_1::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_6_1_1::init()
{

	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_6_1_1 = CSLoader::createNode("Scene_Game_6_1_1.csb");
	addChild(CSB_SceneGame_6_1_1);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_6_1_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_6_1_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_6_1_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_6_1_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//버튼 가져오기
	ButtonPlay = (Button*)CSB_SceneGame_6_1_1->getChildByTag(TAG_BUTTON_START);
	Button_O = (Button*)CSB_SceneGame_6_1_1->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_6_1_1->getChildByTag(TAG_X);

	//체크박스 가져오기
	C_A1 = (CheckBox*)CSB_SceneGame_6_1_1->getChildByTag(TAG_CHECKBOX_Q1_A1);
	C_A2 = (CheckBox*)CSB_SceneGame_6_1_1->getChildByTag(TAG_CHECKBOX_Q1_A2);
	C_A3 = (CheckBox*)CSB_SceneGame_6_1_1->getChildByTag(TAG_CHECKBOX_Q1_A3);
	C_A4 = (CheckBox*)CSB_SceneGame_6_1_1->getChildByTag(TAG_CHECKBOX_Q1_A4);

	//터치이벤트
	ButtonPlay->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	C_A1->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	C_A2->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	C_A3->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	C_A4->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_1_1::onTouch, this));

	problemInit();

	return true;
}
void SceneGame_6_1_1::problemInit(){
	Answer = 0;
	Button_O->setVisible(false);
	Button_X->setVisible(false);

	C_A1->setSelected(false);
	C_A2->setSelected(false);
	C_A3->setSelected(false);
	C_A4->setSelected(false);
}
void  SceneGame_6_1_1::AnswerCheck(){
	if (Answer == 3){
		Button_O->setVisible(true);
	}
	else{
		Button_X->setVisible(true);
	}
}
void SceneGame_6_1_1::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){
		

		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{	
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_EXIT:
			{
				

				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);

				ButtonPlay->setTouchEnabled(false);
				Button_O->setTouchEnabled(false);
				Button_X->setTouchEnabled(false);
				C_A1->setTouchEnabled(false);
				C_A2->setTouchEnabled(false);
				C_A3->setTouchEnabled(false);
				C_A4->setTouchEnabled(false);

				break;
			}
			case TAG_BUTTON_BACK:{
				
				Scene* scene;
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				if (isReplay611) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
				}
				else {
					scene = TransitionFade::create(0.5, SceneStage_6::createScene());
				}
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);

				ButtonPlay->setTouchEnabled(true);
				Button_O->setTouchEnabled(true);
				Button_X->setTouchEnabled(true);
				C_A1->setTouchEnabled(true);
				C_A2->setTouchEnabled(true);
				C_A3->setTouchEnabled(true);
				C_A4->setTouchEnabled(true);

				break;
			}
			case TAG_EXIT:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();

				break;
			}
			case TAG_BUTTON_START:{
				
				AnswerCheck();
				break;
			}
			case TAG_CHECKBOX_Q1_A1:{
				Answer = 1;
				C_A2->setSelected(false);
				C_A3->setSelected(false);
				C_A4->setSelected(false);
				break;
			}
			case TAG_CHECKBOX_Q1_A2:{
				Answer = 2;
				C_A1->setSelected(false);
				C_A3->setSelected(false);
				C_A4->setSelected(false);
				break;
			}
			case TAG_CHECKBOX_Q1_A3:{
				Answer = 3;
				C_A1->setSelected(false);
				C_A2->setSelected(false);
				C_A4->setSelected(false);
				break;
			}
			case TAG_CHECKBOX_Q1_A4:{
				Answer = 4;
				C_A1->setSelected(false);
				C_A2->setSelected(false);
				C_A3->setSelected(false);
				break;
			}
			case TAG_O:{
				
				Button_O->setVisible(false);
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				auto scene = TransitionFade::create(0.5, SceneGame_6_1_2::createScene(isReplay611));
				Director::getInstance()->replaceScene(scene);
				break;
				break; 
			}
			case TAG_X:{
				
				Button_X->setVisible(false);
				problemInit();
				break; 
			}
			default:
				break;
		}
	}
}

void SceneGame_6_1_1::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_6_1_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}