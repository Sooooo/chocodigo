﻿//기본 헤더
#include "SceneGame_2_1_2.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"

#include <ctime>
#include <cstdlib>
#include <math.h>

//화면전환
#include "SceneGame_2_1.h"

USING_NS_CC;
using namespace ui;
using namespace std;

int _tmp;

bool isReplay212;

SceneGame_2_1_2::SceneGame_2_1_2(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_2.mp3";
}

SceneGame_2_1_2::~SceneGame_2_1_2(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}


Scene* SceneGame_2_1_2::createScene(int i, bool _booltmp)
{
	isReplay212 = _booltmp;

	_tmp = i;
	auto scene = Scene::create();

	auto layer = SceneGame_2_1_2::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_2_1_2::init()
{
	
	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_2_1_2 = CSLoader::createNode("Scene_Game_2_1_2.csb");
	addChild(CSB_SceneGame_2_1_2);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_2_1_2->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_2_1_2->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_2_1_2->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_2_1_2::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//태그로 버튼 가져오기
	Button_O = (Button*)CSB_SceneGame_2_1_2->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_2_1_2->getChildByTag(TAG_X);
	ButtonStart = (Button*)CSB_SceneGame_2_1_2->getChildByTag(TAG_BUTTON_START);
	ButtonReset = (Button*)CSB_SceneGame_2_1_2->getChildByTag(TAG_BUTTON_RESET);

	//업,다운 버튼
	ButtonUp_1 = (Button*)CSB_SceneGame_2_1_2->getChildByTag(TAG_BUTTON_UP_1);
	ButtonUp_10 = (Button*)CSB_SceneGame_2_1_2->getChildByTag(TAG_BUTTON_UP_10);
	ButtonDown_1 = (Button*)CSB_SceneGame_2_1_2->getChildByTag(TAG_BUTTON_DOWN_1);
	ButtonDown_10 = (Button*)CSB_SceneGame_2_1_2->getChildByTag(TAG_BUTTON_DOWN_10);

	//텍스트필드 가져오기
	Problem = (TextField*)CSB_SceneGame_2_1_2->getChildByTag(TAG_PROB);
	Decimal_1 = (TextField*)CSB_SceneGame_2_1_2->getChildByTag(TAG_DECIMAL_1);
	Decimal_10 = (TextField*)CSB_SceneGame_2_1_2->getChildByTag(TAG_DECIMAL_10);

	//버튼 터치이벤트
	ButtonStart->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	ButtonReset->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	ButtonUp_1->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	ButtonDown_1->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	ButtonUp_10->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));
	ButtonDown_10->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_2::onTouch, this));

	problemInit();

	return true;
}


//2진수 변환 함수

string SceneGame_2_1_2::decimalToBinary(int Decimal, string s )
{
	int binary = Decimal;

	while (binary >= 0){
		s.insert(0, commonLib::to_string(binary % 2));
		binary = binary / 2;

		if (binary == 1 || binary == 0){
			s.insert(0, commonLib::to_string(binary));
			break;
		}
	}
	return s;
}


void SceneGame_2_1_2::problemInit()
{
	ProblemToBinary = "";

	srand((unsigned int)time(NULL));

	ProblemDecimal = rand() % 15 + 1;

	//2진수 변환함수
	//decimalToBinary(ProblemDecimal, ProblemToBinary);
	Problem->setString(decimalToBinary(ProblemDecimal, ProblemToBinary));

	//Problem->setString(commonLib::to_string(ProblemDecimal));

	//O, X 안보이게
	Button_O->setVisible(false);
	Button_X->setVisible(false);

	//답칸 0 초기화
	UserAnswerInit();

}

void SceneGame_2_1_2::UserAnswerInit()
{
	NumTen = 0;
	NumOne = 0;

	Button_X->setVisible(false);

	Decimal_1->setString(commonLib::to_string(NumTen));
	Decimal_10->setString(commonLib::to_string(NumOne));
}

bool SceneGame_2_1_2::UserAnswerCheck()
{
	int result = 0;

	//판단
	result = NumTen * 10 + NumOne;

	if (ProblemDecimal == result){
		return true;
	}
	else {
		return false;
	}

}

void SceneGame_2_1_2::play(bool Ck)
{
	if (Ck == true)	//정답인 경우
	{
		Button_O->setVisible(true);
	}
	else if (Ck == false)	//오답인 경우
	{
		Button_X->setVisible(true);

	}

}

void SceneGame_2_1_2::NumUp_1(){
	if (NumOne<9)
		NumOne++;
	else if (NumOne == 9) NumOne = 0;

	Decimal_1->setString(commonLib::to_string(NumOne));
}

void SceneGame_2_1_2::NumDown_1(){
	if (NumOne>0)
		NumOne--;
	else if (NumOne == 0) NumOne = 9;
	Decimal_1->setString(commonLib::to_string(NumOne));
}

void SceneGame_2_1_2::NumUp_10(){
	if (NumTen<9)
		NumTen++;
	else if (NumTen == 9) NumTen = 0;

	Decimal_10->setString(commonLib::to_string(NumTen));
}

void SceneGame_2_1_2::NumDown_10(){
	if (NumTen>0)
		NumTen--;
	else if (NumTen == 0) NumTen = 9;
	Decimal_10->setString(commonLib::to_string(NumTen));
}

void SceneGame_2_1_2::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){
		
		auto SelectButton = (Button*)sender;
		auto action = ToggleVisibility::create();

		switch (SelectButton->getTag())
		{
			case TAG_BUTTON_START:{
				play(UserAnswerCheck());
				break;
			}
			case TAG_BUTTON_RESET:{
				UserAnswerInit();
				break;
			}
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();

				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_BACK:{
				

				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				if (isReplay212) {
					auto scene = TransitionFade::create(0.5, QuestReplay::createScene());
					Director::getInstance()->replaceScene(scene);
					Button_O->setVisible(false);
				}
				else {
					auto scene = TransitionFade::create(0.5, SceneGame_2_1::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				break;
			}
			case TAG_BUTTON_EXIT:
			{
				

				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);

				ButtonStart->setTouchEnabled(false);
				ButtonReset->setTouchEnabled(false);
				Button_O->setTouchEnabled(false);
				Button_X->setTouchEnabled(false);
				ButtonUp_1->setTouchEnabled(false);
				ButtonUp_10->setTouchEnabled(false);
				ButtonDown_1->setTouchEnabled(false);
				ButtonDown_10->setTouchEnabled(false);

				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);

				ButtonStart->setTouchEnabled(true);
				ButtonReset->setTouchEnabled(true);
				Button_O->setTouchEnabled(true);
				Button_X->setTouchEnabled(true);
				ButtonUp_1->setTouchEnabled(true);
				ButtonUp_10->setTouchEnabled(true);
				ButtonDown_1->setTouchEnabled(true);
				ButtonDown_10->setTouchEnabled(true);

				break;
			}
			case TAG_EXIT:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();

				break;
			}
			case TAG_O:{

				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				if (isReplay212) {
					auto scene = TransitionFade::create(0.5, QuestReplay::createScene());
					Button_O->setVisible(false);
					Director::getInstance()->replaceScene(scene);
				}
				else {
					logManager->setQ2_2_Flag(_tmp);
					logManager->setChapterProgress(logManager->getChapterProgress() + 1);
					logManager->printAll();
					if (logManager->getChapterProgress() == 26) {

						auto scene = TransitionFade::create(0.5, SceneCreator::createScene(2, 3));
						Director::getInstance()->replaceScene(scene);
					}
					else {
						auto scene = TransitionFade::create(0.5, SceneGame_2_1::createScene());
						Director::getInstance()->replaceScene(scene);
					}
				}
				break; 
			}
			case TAG_X:{
				UserAnswerInit();
				break;
			}
			case TAG_BUTTON_UP_1:{
				NumUp_1();
				break;
			}
			case TAG_BUTTON_UP_10:{
				NumUp_10();
				break;
			}
			case TAG_BUTTON_DOWN_1:{
				NumDown_1();
				break;
			}
			case TAG_BUTTON_DOWN_10:{
				NumDown_10();
				break;
			}
			default:
				break;
		}
	}
}

void SceneGame_2_1_2::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_2_1_2::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}