﻿#include "Character.h"

using namespace std;

Character::Character(){
}

Character::Character(Character* newCharacter){
	this->Name = newCharacter->getName();
	this->ImgSrc = newCharacter->getImgSrc();
	this->HP = newCharacter->getHP();
	this->ChangeHP = newCharacter->getHP();
}

std::string Character::getName()
{
	return this->Name;
}

void Character::setName(std::string name)
{
	this->Name = name;
}

std::string Character::getImgSrc()
{
	return this->ImgSrc;
}

void Character::setImgSrc(std::string newImgSrc)
{
	this->ImgSrc = newImgSrc;
}

int Character::getHP()
{
	return this->HP;
}

void Character::setHP(int newHP)
{
	this->HP = newHP;
}

int Character::getChangeHP()
{
	return this->ChangeHP;
}

void Character::setChangeHP(int newChangeHP)
{
	this->ChangeHP = newChangeHP;
}

float Character::getHPPercentage()
{
	return (this->ChangeHP * 100 / this->HP);
}
