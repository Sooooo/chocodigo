﻿#pragma execution_character_set("UTF-8")

//기본 헤더
#include "SceneGame_4_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
//#include <stdlib.h>

//화면 전환
#include "SceneStage_4.h"

USING_NS_CC;
using namespace ui;

bool isReplay41;

SceneGame_4_1::SceneGame_4_1(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_4.mp3";
}

SceneGame_4_1::~SceneGame_4_1(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_4_1::createScene(bool _booltmp)
{
	isReplay41 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_4_1::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_4_1::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_4_1 = CSLoader::createNode("Scene_Game_4_1.csb");
	addChild(CSB_SceneGame_4_1);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_4_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_4_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_4_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneGame_4_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	/* System Button */

	Button_O = (Button*)CSB_SceneGame_4_1->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_4_1->getChildByTag(TAG_X);
	ButtonConfirm = (Button*)CSB_SceneGame_4_1->getChildByTag(TAG_BUTTON_START);

	//Add Touch Event

	ButtonConfirm->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));
	
	/* System Button End */

	for (int i = 0; i < 4; i++) {
		ButtonAnswer[i] = (Button*)CSB_SceneGame_4_1->getChildByTag(TAG_BUTTON_ANSWER + i);
		ButtonAnswer[i]->addTouchEventListener(CC_CALLBACK_2(SceneGame_4_1::onTouch, this));
	}

	turn = 0;

	selAnswer[0] = 1;
	selAnswer[1] = 1;
	selAnswer[2] = 1;
	selAnswer[3] = 1;

	for (int i = 0; i < 4; i++) {
		std::string s = commonLib::to_string(selAnswer[i]);
		TextAnswer[i] = (TextField*)CSB_SceneGame_4_1->getChildByTag(TAG_TEXTFIELD_ANSWER + i);
		TextAnswer[i]->setString(s);
		TextQuestion[i] = (TextField*)CSB_SceneGame_4_1->getChildByTag(TAG_TEXTFIELD_QUESTION + i);
	}
	
	//Set Randomize
	srand((unsigned int)time(NULL));

	setRandomQuestion();

	return true;
}

void SceneGame_4_1::setRandomAnswer(int questionNumber) {
	
	int buf = rand() % 4;
//	int buf = 0;
	bool stringList[4] = { true, true, true, true };
	int cnt = 0;

	std:: string questionString[3][4] = {
		{	"무엇이든 실제보다 더욱 흉측하게 비추는 거울을 가진 트롤이 거울을 떨어뜨렸습니다.", 
			"떨어진 거울은 수억 개의 조각들로 부서져 인간 세상 사람들의 심장과 눈에 박혀버렸습니다.", 
			"작은 마을에 살던 소년 카이의 심장과 눈에도 이 거울 조각이 박혀버렸습니다.",
			"거울 조각이 박힌 카이는 차갑게 변하고 무엇이든 나쁘게 보게 되었습니다."},
		{	"카이는 단짝 친구였던 소녀 겔다와 멀어지게 되었습니다.", 
			"그러던 어느 겨울 날 카이는 눈의 여왕을 만나게 되었습니다.",
			"눈의 여왕은 카이에게 추위를 느끼지 않게 하고 겔다와 가족을 잊도록 하는 입맞춤을 했습니다.",
			"입맞춤을 한 눈의 여왕은 카이를 자신의 궁전으로 데려가 얼음 조각으로 된 퍼즐을 풀어야지만 벗어날 수 있게 해준다고 했습니다."},
		{	"사라져버린 카이를 찾아 길을 떠나는 게르다는 온갖 모험을 이겨내며 마침내 눈의 여왕의 궁전에 도착했습니다.", 
			"마침내 카이를 찾은 겔다는 뜨거운 눈물을 흘리고, 그 눈물은 카이의 심장에 박힌 거울 조각을 녹였습니다.", 
			"카이도 함께 눈물을 흘리자 그의 눈에 있던 거울 조각도 빠져 나오게 되었습니다.",
			"원래의 모습으로 돌아온 카이는 겔다와 함께 얼음 조각 퍼즐을 맞추고, 둘은 무사히 그곳에서 벗어날 수 있게 되었습니다."},
	};
	
	while (cnt < 4) {
		if (stringList[buf]) {
			stringList[buf] = false;
			TextAnswer[cnt]->setString(commonLib::to_string(selAnswer[cnt]));
			TextQuestion[buf]->setString(questionString[questionNumber][cnt]);
			gameAnswer[buf] = cnt;

			cnt++;
		}
		buf = rand() % 4;	
	}

}

void SceneGame_4_1::setRandomQuestion(){

	setRandomAnswer(turn);
	
}

bool SceneGame_4_1::isCorrect() {
	for (int i = 0; i < 4; i++)  {
		if (gameAnswer[i] != (selAnswer[i]-1)) {
			return false;
		}
	}
	return true;
}

void SceneGame_4_1::answerButtonClicked(int buttonNum) {

	if (selAnswer[buttonNum] == 4)
		selAnswer[buttonNum] = 1;
	else
		selAnswer[buttonNum] += 1;

	TextAnswer[buttonNum]->setString(commonLib::to_string(selAnswer[buttonNum]));

}

void SceneGame_4_1::onTouch(Ref* sender, Widget::TouchEventType type){

	if (type == Widget::TouchEventType::ENDED){

		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
			//System Button
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			for (int i = 0; i < 4; i++)
				ButtonAnswer[i]->setTouchEnabled(false);

			ButtonConfirm->setTouchEnabled(false);
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			
			Scene* scene;

			if (isReplay41) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_4::createScene());
			}
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			for (int i = 0; i < 4; i++)
				ButtonAnswer[i]->setTouchEnabled(false);

			ButtonConfirm->setTouchEnabled(true);
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_BUTTON_START:

			if (isCorrect()) {
				Button_O->setVisible(true);
			}
			else {
				Button_X->setVisible(true);
			}

			break;
		case TAG_O:
		{
			Scene* scene;


			if (isReplay41) {
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
				Director::getInstance()->replaceScene(scene);
			}
			else {

				Button_O->setVisible(false);
				if (turn == 2) {
					logManager->setChapterProgress(60);

					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(4, 7));
					Director::getInstance()->replaceScene(scene);
			
				}
				else {
					turn++;

					selAnswer[0] = 1;
					selAnswer[1] = 1;
					selAnswer[2] = 1;
					selAnswer[3] = 1;

					setRandomQuestion();
				}
			}
			
		}
		break;

		case TAG_X:
		{
			
			Button_X->setVisible(false);

			selAnswer[0] = 1;
			selAnswer[1] = 1;
			selAnswer[2] = 1;
			selAnswer[3] = 1;

			setRandomQuestion();
		}
		break;

		//Game Button
		case TAG_BUTTON_ANSWER:
			answerButtonClicked(0);
			break;
		case TAG_BUTTON_ANSWER + 1:
			answerButtonClicked(1);
			break;
		case TAG_BUTTON_ANSWER + 2:
			answerButtonClicked(2);
			break;
		case TAG_BUTTON_ANSWER + 3:
			answerButtonClicked(3);
			break;
		
		default:{ break; }

		}
	}
}

void SceneGame_4_1::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_4_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}