﻿//기본 헤더
#include "SceneGame_2_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneGame_2_1_1.h"
#include "SceneGame_2_1_2.h"
#include "SceneStage_2.h"

USING_NS_CC;
using namespace ui;

bool isReplay21;

SceneGame_2_1::SceneGame_2_1(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;
		
	cP = new checkProgress;
	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_2.mp3";

	ProblemAchievement = 0;

	for (int i = 0; i < 8; i++){
		Problems[i] = 0;
	}
}

SceneGame_2_1::~SceneGame_2_1(){
	delete logManager;
	delete cP;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_2_1::createScene(bool _booltmp)
{
	isReplay21 = _booltmp;
    auto scene = Scene::create();

	auto layer = SceneGame_2_1::create();
    scene->addChild(layer);

    return scene;
}

// on "init" you need to initialize your instance
bool SceneGame_2_1::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
	auto CSB_SceneGame_2_1 = CSLoader::createNode("Scene_Game_2_1.csb");
	addChild(CSB_SceneGame_2_1);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_2_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_2_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_2_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_2_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	logManager->reloadQ2_2();
	if (isReplay21) {
		for (int Index = 0; Index < 6; Index++){
			Problems[Index] = (Button*)CSB_SceneGame_2_1->getChildByTag(Index + TAG_PROBLEM_1);
			Problems[Index]->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1::onTouch, this));
		}
	}
	else {
		for (int Index = 0; Index < 6; Index++){
			Problems[Index] = (Button*)CSB_SceneGame_2_1->getChildByTag(Index + TAG_PROBLEM_1);
			Problems[Index]->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1::onTouch, this));
			if (logManager->getQ2_2_Flag(Index)) {
				Problems[Index]->setTouchEnabled(false);
				Problems[Index]->setBright(false);
			}
			else {
				Problems[Index]->setTouchEnabled(true);
				Problems[Index]->setBright(true);
			}
		}
	}

    return true;
}
void SceneGame_2_1::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){
		
		
		Scene* scene;

		auto SelectCheckBox = (CheckBox*)sender;
		auto SelectButton = (Button*)sender;

		int tmp = SelectCheckBox->getTag();
		int isTouchPop = false;
		
		switch (SelectButton->getTag())
		{
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();

				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_BACK:{

				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);

				Scene* scene;

				if (isReplay21) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				else {
					scene = TransitionFade::create(0.5, SceneStage_2::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				
				break;
			}
			case TAG_BUTTON_EXIT:
			{
				isTouchPop = true;
				

				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);
				
				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);
				
				break;
			}
			case TAG_EXIT:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();

				break;
			}
			default:
				break;
		}

		if (isTouchPop == false)
		{
			switch (tmp)
			{
			case TAG_PROBLEM_1:
			case TAG_PROBLEM_2:
			case TAG_PROBLEM_3:
			{
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				scene = TransitionFade::create(0.5, SceneGame_2_1_1::createScene(tmp - 701, isReplay21));
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_PROBLEM_4:
			case TAG_PROBLEM_5:
			case TAG_PROBLEM_6:
			{
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				scene = TransitionFade::create(0.5, SceneGame_2_1_2::createScene(tmp - 701, isReplay21));
				Director::getInstance()->replaceScene(scene);
				break;
			}
			default:
				break;
			}
		}
		
	}
}

void SceneGame_2_1::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_2_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}