﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_GAME_5_2_2_H__
#define __SCENE_GAME_5_2_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include "CommonLib.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"

/*************** 태그 정보 ***************/
#define TAG_PLAYER				701
#define TAG_HEART				702	//702 ~ 705
#define TAG_NEXT				706
/****************************************/
#define MAZE_SIZE 8
#define MAZE_SIZE_WIDTH 140
#define MAZE_SIZE_HEIGHT 105
#define DESTINATION_X	7
#define DESTINATION_Y	7
#define SPEED 1.0f
#define ACTION_MAX	40
#define	ROAD_COUNT	28

enum
{
	UP = 1,
	DOWN = 2,
	LEFT = 3,
	RIGHT = 4,
	PICK = 5
}; 

class SceneGame_5_2_2 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	int Maze[MAZE_SIZE][MAZE_SIZE];

	Sprite* Player;
	Sprite* Heart[4];

	Button* ButtonNext;
	Button* ButtonPlay;

	Button* Button_O;
	Button* Button_X;

public:	
	
	SceneGame_5_2_2();
	~SceneGame_5_2_2();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	static cocos2d::Scene* createScene(std::string useranswer, bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_5_2_2);
	
	bool Simulation();

	//카운트값이 정해졌을때 몇칸까지 움직일 수 있는지 알려주는 함수.
	int IsMoveUp(int count, int x, int y);
	int IsMoveDown(int count, int x, int y);
	int IsMoveLeft(int count, int x, int y);
	int IsMoveRight(int count, int x, int y);

	//충돌 처리 후 PICK
	void scheduleCollision(float dt);

	void play();	//실행하기

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};

#endif 