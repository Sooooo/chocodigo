﻿#ifndef __QUEST_REPLAY__
#define __QUEST_REPLAY__
//#define COCOS2D_DEBUG 1

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include "CommonTagSet.h"
#include "SceneExplainCreator.h"

USING_NS_CC;
using namespace ui;

#define TAG_QUEST			701
#define TAG_TO_WORLD		720

class QuestReplay : public Layer
{
private:


public:
	QuestReplay();
	~QuestReplay();

	Button* Quest[17];
	Button* Button_toWorld;

	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(QuestReplay);

	void onTouch(Ref* sender, Widget::TouchEventType type);	//터치 이벤트
};

#endif
