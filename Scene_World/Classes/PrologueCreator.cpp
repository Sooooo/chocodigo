﻿//기본 헤더
#include "PrologueCreator.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "ui\CocosGUI.h"

//화면 전환
#include "SceneStage_1.h"
#include "SceneStage_2.h"
#include "SceneStage_3.h"
#include "SceneStage_4.h"
#include "SceneStage_5.h"
#include "SceneStage_6.h"
#include "SceneStage_7.h"

USING_NS_CC;
using namespace ui;

int _ChapterNum;

PrologueCreator::PrologueCreator(){

}

PrologueCreator::~PrologueCreator(){

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}


Scene* PrologueCreator::createScene(int i)
{
	auto scene = Scene::create();

	_ChapterNum = i;

	auto layer = PrologueCreator::create();
	scene->addChild(layer);


	return scene;
}

bool PrologueCreator::init()
{
	if (!Layer::init())
	{
		return false;
	}


	//Stage 4의 프롤로그 불러오기
	auto PrologueCreator = CSLoader::createNode("PrologueCreator.csb");
	addChild(PrologueCreator);

	//버튼 가져와 터치 이벤트 등록
	Background = (Button*)PrologueCreator->getChildByTag(TAG_BACKGROUND);
	Background->addTouchEventListener(CC_CALLBACK_2(PrologueCreator::onTouch, this));
	std::string str = "Game_Resource/script_system/Prologue/Chapter";
	str.append(commonLib::to_string(_ChapterNum));
	str.append(".png");
	Background->loadTextures(str, str);

	return true;
}

void PrologueCreator::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		

		Scene* scene;

		switch (_ChapterNum) {
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

		case 1:
			scene = TransitionFade::create(0.5, SceneStage_1::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		case 2:
			scene = TransitionFade::create(0.5, SceneStage_2::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		case 3:
			scene = TransitionFade::create(0.5, SceneStage_3::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		case 4:
			scene = TransitionFade::create(0.5, SceneStage_4::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		case 5:
			scene = TransitionFade::create(0.5, SceneStage_5::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		case 6:
			scene = TransitionFade::create(0.5, SceneStage_6::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		case 7:
			scene = TransitionFade::create(0.5, SceneCreator::createScene(7,2));
			Director::getInstance()->replaceScene(scene);
		default :
			break;
		}
	}

}