﻿#include "json.h"

using std::string;
using rapidjson::Document;

Json::Json(){
}

Json::~Json() {
	FileUtils::destroyInstance;
}

bool Json::openfile(int c, int s, std::string str) {
	
	this->ChapterNum = c;
	this->SceneNum = s;

	std::string fileName = "Game_Resource/scriptresources/";
	fileName.append(str);
	if (str.compare("script") == 0) {
		
		fileName.append(commonLib::to_string(this->ChapterNum));
		fileName.append("_");
		fileName.append(commonLib::to_string(this->SceneNum));
		ObjectName = "Script";
	}	
	fileName.append(".json");
	
	long bufferSize = 0;

	bool error = true;

	while (error) {

		string tmpData;
		tmpData = FileUtils::getInstance()->getStringFromFile(fileName.c_str());

		size_t pos = tmpData.rfind("}");
		tmpData = tmpData.substr(0, pos+1);
	
		while (tmpData.empty()) {
			tmpData =	FileUtils::getInstance()->getStringFromFile(fileName.c_str());
		}

		(this->doc).Parse<0>(tmpData.c_str());
		error = this->doc.HasParseError();
		
	}

	return true; 
}

int Json::getProgressPara(int i) {
	int tmp;
	rapidjson::Value& gem = this->doc[ObjectName.c_str()][(SizeType)i];
	assert(gem.IsObject());
	assert(gem["progressPara"].IsInt());
	tmp = gem["progressPara"].GetInt();
	FileUtils::destroyInstance;

	return tmp;
}

int Json::getArraySize() {

	rapidjson::Value& tmp = this->doc[ObjectName.c_str()];
	assert(tmp.IsArray());

	int a = tmp.Size();

	FileUtils::destroyInstance;

	return a;

}

std::string Json::getScriptData(std::string content){

	rapidjson::Value& gem = this->doc[ObjectName.c_str()][(SizeType)0];


	assert(gem.IsObject());
	std::string tmp = "Game_Resource/script_system/";

	//초기 세팅
	if (content.compare("BGIMG") == 0) {
		tmp.append("background/");
		tmp.append(commonLib::to_string(this->ChapterNum));
		tmp.append("_");
		assert(gem["BGIMG"].IsString());
		std::string ttmp = gem["BGIMG"].GetString();
		if (ttmp.compare("") == 0) {
			tmp = "Game_Resource/script_system/background/blank.png";
		}
		else {
			tmp.append(ttmp);
		}
	}
	else if (content.compare("LPot") == 0) {
		tmp.append("character/");
		tmp.append(commonLib::to_string(this->ChapterNum));
		tmp.append("_");
		assert(gem["LPot"].IsString());
		std::string ttmp = gem["LPot"].GetString();
		if (ttmp.compare("") == 0) {
			tmp = "Game_Resource/script_system/character/blank.png";
		}
		else {
			tmp.append(ttmp);
		}
	}
	else if (content.compare("RPot") == 0) {
		tmp.append("character/");
		tmp.append(commonLib::to_string(this->ChapterNum));
		tmp.append("_");
		assert(gem["RPot"].IsString());
		std::string ttmp = gem["RPot"].GetString();
		if (ttmp.compare("") == 0) {
			tmp = "Game_Resource/script_system/character/blank.png";
		}
		else {
			tmp.append(ttmp);
		}
	}
	
	FileUtils::destroyInstance;
	return tmp;
}; 

std::string Json::getScriptData(int i, std::string content){

	rapidjson::Value& gem = this->doc[ObjectName.c_str()][(SizeType)i];

	assert(gem.IsObject());
	std::string tmp;

	//text, name 가져옴
	if (content.compare("name") == 0) {
		assert(gem["name"].IsString());
		tmp = gem["name"].GetString();
	}
	else if (content.compare("dialogue") == 0) {
		assert(gem["dialogue"].IsString());
		tmp = gem["dialogue"].GetString();
	}
	else if (content.compare("event") == 0) {
		assert(gem["event"].IsString());
		tmp = gem["event"].GetString();
	}
	else if (content.compare("parameter") == 0) {
		assert(gem["parameter"].IsString());
		tmp = gem["parameter"].GetString();
	}
	FileUtils::destroyInstance;
	return tmp;
};

std::string Json::getQuestData(int i, std::string str){

	rapidjson::Value& gem = this->doc["quest"][(SizeType)i];

	assert(gem.IsObject());
	std::string tmp;

	if (str.compare("inform") == 0) {
		assert(gem["inform"].IsString());
		tmp = gem["inform"].GetString();
	}
	else if (str.compare("goal") == 0) {
		assert(gem["goal"].IsString());
		tmp = gem["goal"].GetString();		
	}
	else if (str.compare("QTitle") == 0) {
		assert(gem["QTitle"].IsString());
		tmp = gem["QTitle"].GetString();
	}
	FileUtils::destroyInstance;
	return tmp;
}

bool Json::checkNext(int i) {
	
	bool tmp;
	rapidjson::Value& gem = this->doc[ObjectName.c_str()][(SizeType)i];
	assert(gem.IsObject());
	assert(gem["next"].IsBool());
	tmp = gem["next"].GetBool();
	FileUtils::destroyInstance;

	return tmp;
}

int Json::getIntParameter(int i) {
	
	int tmp;
	rapidjson::Value& gem = this->doc[ObjectName.c_str()][(SizeType)i];
	assert(gem.IsObject());
	assert(gem["parameter"].IsInt());
	tmp = gem["parameter"].GetInt();
	FileUtils::destroyInstance;

	return tmp;
}