﻿#pragma execution_character_set("UTF-8")
//한글출력시 필요.

#ifndef __SCENE_GAME_4_2_H__
#define __SCENE_GAME_4_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"

/*************** 태그 정보 ***************/
#define TAG_CHECK_1				701
#define TAG_CHECK_2				702
#define TAG_CHECK_3				703
#define TAG_CHECK_4				704
#define TAG_MAN_1				705
#define TAG_MAN_2				706
#define TAG_MAN_3				707
#define TAG_MAN_4				708
#define TAG_TXT_CHAT			709
/****************************************/


class SceneGame_4_2 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	int ProblemAnswer;
	int UserAnswer;

	TextField* TxtChat;

	CheckBox* Check[4];
	CheckBox* People[4];

	Button* ButtonConfirm;

	Button* Button_O;
	Button* Button_X;

public:
	SceneGame_4_2();
	~SceneGame_4_2();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_4_2);

	void Chat(int tag);
	void UserAnswerInit();	//정답 초기화
	void UserAnswerCheck();	//계산 결과 확인
	void play();	//실행하기

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};


#endif 