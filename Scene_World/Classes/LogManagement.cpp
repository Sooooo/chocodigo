﻿#include "LogManagement.h"


LogManagement* LogManagement::_instance = nullptr;

void LogManagement::tmpFunction(int i){
	UserDefault *pUserDefault = UserDefault::getInstance();

	switch (i) {
	case 0 : 
		pUserDefault->setBoolForKey("PROLOGUE", true);
	case 2:
		for (int j = 0; j < 6; j++) {
			std::string tmp = "Q2_FLAG_";
			tmp.append(commonLib::to_string(j));
			this->quest2_2[i] = false;
			pUserDefault->setBoolForKey(tmp.c_str(), false);
		}
		break;
	default :
		break;
	}

	pUserDefault->setIntegerForKey("CHAPTER", i);
	pUserDefault->setIntegerForKey("ChapterPROGRESS", 0);

	this->chapter = pUserDefault->getIntegerForKey("CHAPTER", 0);
	this->chapterProgress = pUserDefault->getIntegerForKey("ChapterPROGRESS", 0);

	pUserDefault->flush();
};
LogManagement::LogManagement(){

	UserDefault *pUserDefault = UserDefault::getInstance();
	this->chapter = pUserDefault->getIntegerForKey("CHAPTER", 0);
	this->currentQuest = pUserDefault->getIntegerForKey("QUEST", 0);
	this->chapterProgress = pUserDefault->getIntegerForKey("ChapterPROGRESS", 0);

	for (int i = 0; i < 6; i++) {
		std::string tmp = "Q2_FLAG_";
		tmp.append(commonLib::to_string(i));
		this->quest2_2[i] = pUserDefault->getBoolForKey(tmp.c_str(), false);
		//pUserDefault->setBoolForKey(tmp.c_str(), false);
	}
	
	//progress = pUserDefault->getIntegerForKey("PROGRESS", 0);
	
	this->systemsound = pUserDefault->getBoolForKey("BoolSound", true);
		
	this->prologue = pUserDefault->getBoolForKey("PROLOGUE", true);
}

void LogManagement::reloadQ2_2() {
	UserDefault *pUserDefault = UserDefault::getInstance();
	for (int i = 0; i < 6; i++) {
		std::string tmp = "Q2_FLAG_";
		tmp.append(commonLib::to_string(i));
		this->quest2_2[i] = pUserDefault->getBoolForKey(tmp.c_str(), false);
	}
}
void LogManagement::setQ2_2_Flag(int i) {
	UserDefault *pUserDefault = UserDefault::getInstance();
	this->quest2_2[i] = true;
	std::string tmp = "Q2_FLAG_";
	tmp.append(commonLib::to_string(i));
	pUserDefault->setBoolForKey(tmp.c_str(), true);
	pUserDefault->flush();
}

void LogManagement::setSoundOption(bool _tmp) {

	UserDefault *pUserDefault = UserDefault::getInstance();
	systemsound = _tmp;
	if (_tmp){
		pUserDefault->setBoolForKey("BoolSound", true);
	}
	else {
		pUserDefault->setBoolForKey("BoolSound", false);
	}

	pUserDefault->flush();
}

void LogManagement::setChapter(int _chapter) {
	UserDefault *pUserDefault = UserDefault::getInstance();
	this->chapter = _chapter;
	pUserDefault->setIntegerForKey("CHAPTER", _chapter);
	pUserDefault->flush();
}
void LogManagement::setLog(int _progress, int _quest, int _chapter) {

	UserDefault *pUserDefault = UserDefault::getInstance();
	pUserDefault->setIntegerForKey("ChapterPROGRESS", _progress);
	pUserDefault->setIntegerForKey("QUEST", _quest);
	pUserDefault->setIntegerForKey("CHAPTER", _chapter);

//	chapter = pUserDefault->getIntegerForKey("CHAPTER", 0);
//	currentQuest = pUserDefault->getIntegerForKey("QUEST", 0);
//	progress = pUserDefault->getIntegerForKey("PROGRESS", 0);

	pUserDefault->flush();
}

void LogManagement::printAll() {
	UserDefault *pUserDefault = UserDefault::getInstance();
}

void LogManagement::setPrologue(){
	UserDefault *pUserDefault = UserDefault::getInstance();
	//pUserDefault->setBoolForKey("PROLOGUE", true);
	pUserDefault->setBoolForKey("PROLOGUE", false);
	pUserDefault->setIntegerForKey("QUEST", 1);
	pUserDefault->flush();
}

void LogManagement::setQuest(int _quest){
	UserDefault *pUserDefault = UserDefault::getInstance();
	this->currentQuest = _quest;
	pUserDefault->setIntegerForKey("QUEST", _quest);
	pUserDefault->flush();
}
void LogManagement::setChapterProgress(int _progress) {
	UserDefault *pUserDefault = UserDefault::getInstance();
	this->chapterProgress = _progress;
	pUserDefault->setIntegerForKey("ChapterPROGRESS", _progress);
	pUserDefault->flush();
}

// setChapterProgress랑 같음
LogManagement::~LogManagement() {}