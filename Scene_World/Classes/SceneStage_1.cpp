﻿//기본 헤더
#include "SceneStage_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneWorld.h"
#include "SceneExplainCreator.h"
#include "SceneCharacterInfo.h"

USING_NS_CC;
using namespace ui;

SceneStage_1::SceneStage_1(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	cP = new checkProgress;
	BGM = "Game_Resource/Sound/BGM_1.mp3";
}

SceneStage_1::~SceneStage_1(){
	delete logManager;
	delete cP;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneStage_1::createScene()
{
	auto scene = Scene::create();

	auto layer = SceneStage_1::create();
	scene->addChild(layer);

	return scene;
}

// on "init" you need to initialize your instance
bool SceneStage_1::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//Import csb file
	auto CSB_SceneStage_1 = CSLoader::createNode("Scene_Stage_1.csb");
	addChild(CSB_SceneStage_1);
	
	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneStage_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneStage_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneStage_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneStage_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneStage_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneStage_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneStage_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneStage_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneStage_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	ButtonPlayerInfo = (Button*)CSB_SceneStage_1->getChildByTag(TAG_QUEST_INFO);
	ButtonPlayerInfo->addTouchEventListener(CC_CALLBACK_2(SceneStage_1::onTouch, this));

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneStage_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//Map
	for (int i = 0; i < 4; i++) {
		Notice[i] = (Sprite*)CSB_SceneStage_1->getChildByTag(TAG_NOTICE + i);
		Map[i] = (Button*)CSB_SceneStage_1->getChildByTag(TAG_MAP + i);
		Map[i]->addTouchEventListener(CC_CALLBACK_2(SceneStage_1::onTouch, this));
	}

	cP->checkChapter1(logManager->getChapterProgress());

	if (logManager->getChapter() < 2) {
		for (int i = 0; i < 4; i++) {
			Notice[i]->setVisible(cP->getNoticeFlag(i));
		}
	}
	else {
		for (int i = 0; i < 4; i++) {
			Notice[i]->setVisible(false);
		}
	}

	return true;
}

void SceneStage_1::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		
		auto SelectButton = (Button*)sender;

		Scene* scene;
		
		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);
			
			ButtonPlayerInfo->setTouchEnabled(false);

			for (int i = 0; i < 4; i++)
				Map[i]->setTouchEnabled(false);				

			break;
		}
		case TAG_BUTTON_BACK:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();

			scene = TransitionFade::create(0.5, SceneWorld::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonPlayerInfo->setTouchEnabled(true);
			for (int i = 0; i < 4; i++)
				Map[i]->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_MAP:
		{
			
			if (logManager->getChapter() < 2) {
				if (logManager->getChapterProgress() <= 10) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 1));
					Director::getInstance()->replaceScene(scene);
					//logManager->setChapter(1);
					//logManager->setQuest(1);
				}
				else if (logManager->getChapterProgress() == 50) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 6));
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 80) {
					logManager->setChapterProgress(90);
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					
					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 8));
					Director::getInstance()->replaceScene(scene);
					logManager->setQuest(3);
					logManager->setChapter(2);
					logManager->printAll();
				}
				else {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 9));
					Director::getInstance()->replaceScene(scene);
				}
			}
			else {
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 9));
				Director::getInstance()->replaceScene(scene);
			}
			break;
		}
		case TAG_MAP + 1:
		{
			
			if (logManager->getChapter() < 2) {
				if (logManager->getChapterProgress() == 20) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 2));
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 35) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 5));
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 40) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneExplainCreator::createScene(1));
					Director::getInstance()->replaceScene(scene);
					logManager->setQuest(2);
				}
				else if (logManager->getChapterProgress() == 60) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 7));
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 70) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneExplainCreator::createScene(2));
					Director::getInstance()->replaceScene(scene);
				}
				else {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 9));
					Director::getInstance()->replaceScene(scene);
				}
			}
			else {
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 9));
				Director::getInstance()->replaceScene(scene);
			}
			break;
		}
		case TAG_MAP + 2:
		{
			
			if (logManager->getChapter() < 2) {
				if (logManager->getChapterProgress() == 25) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 3));
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 40) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneExplainCreator::createScene(1));
					Director::getInstance()->replaceScene(scene);
				}
				else {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 9));
					Director::getInstance()->replaceScene(scene);
				}
			}
			else {
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 9));
				Director::getInstance()->replaceScene(scene);
			}
			break;
		}
		case TAG_MAP + 3:
		{
			
			if (logManager->getChapter() < 2) {
				if (logManager->getChapterProgress() == 30) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 4));
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 40) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneExplainCreator::createScene(1));
					Director::getInstance()->replaceScene(scene);
				}
				else {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 9));
					Director::getInstance()->replaceScene(scene);
				}
			}
			else {
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				scene = TransitionFade::create(0.5, SceneCreator::createScene(1, 9));
				Director::getInstance()->replaceScene(scene);
			}
			break;
		}
		case TAG_QUEST_INFO:
		{
			auto scene = TransitionFade::create(0.5, SceneCharacterInfo::createScene());
			Director::getInstance()->pushScene(scene);
		}
		default:
			break;
		}
			
	}
}

void SceneStage_1::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneStage_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);
	}
}