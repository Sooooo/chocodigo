﻿#ifndef __SCENE_GAME_6_1_1_H__
#define __SCENE_GAME_6_1_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"

/*************** 태그 정보 ***************/
#define  TAG_CHECKBOX_Q1_A1	701
#define  TAG_CHECKBOX_Q1_A2	702
#define  TAG_CHECKBOX_Q1_A3	703
#define  TAG_CHECKBOX_Q1_A4	704
/****************************************/


class SceneGame_6_1_1 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	Button* ButtonPlay;
	Button* Button_O;
	Button* Button_X;

	CheckBox* C_A1;
	CheckBox* C_A2;
	CheckBox* C_A3;
	CheckBox* C_A4;

	int Answer;

public:
	SceneGame_6_1_1();
	~SceneGame_6_1_1();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_6_1_1);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	void problemInit();
	void AnswerCheck();
};


#endif