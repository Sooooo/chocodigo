﻿#include "Player.h"

using namespace std;

Player::Player()
{
	this->setName("Themo");
	this->setHP(1000);
	this->setChangeHP(1000);
	this->setImgSrc("./People/themo.png");
	this->effect = "./Game_Resource/Battle_System/";

	this->flag = true;
}

std::string Player::attack(Character* charater)
{
	int change = charater->getChangeHP() - 1;

	if (change < 0)
		change = 0;

	charater->setChangeHP(change);
	return this->getEffect();
}

void Player::setEffect(int num) {
	
	string effect[7] = {
		"Fire", "Ice", "Poison",
		"Fire", "Ice", "Poison",
		"Fire"
	};

	if (flag) {
		this->effect.append(effect[num]);
		this->effect.append("_effect.plist");
		flag = false;
	}
}

std::string Player::getEffect()
{
	return this->effect;
	//효과보여주기
}