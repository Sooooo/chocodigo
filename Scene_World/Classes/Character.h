﻿#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include <string>

using namespace std;

class Character
{
private:
	string Name;
	string ImgSrc;
	int HP;
	int ChangeHP;

public:
	
	Character();
	Character(Character* newCharacter);
	
	string getName();
	void setName(string newName);

	string getImgSrc();
	void setImgSrc(string newImgSrc);

	int getHP();
	void setHP(int newHP);
	
	int getChangeHP();
	void setChangeHP(int newChangeHP);

	float getHPPercentage();
};

#endif