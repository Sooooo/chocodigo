﻿#ifndef __SCENE_GAME_2_2_2_H__
#define __SCENE_GAME_2_2_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "SceneCreator.h"

/*************** 태그 정보 ***************/
#define TAG_NUM_1				701
#define TAG_NUM_2				702
#define TAG_NUM_3				703
#define TAG_NUM_4				704
#define TAG_NUM_5				705
#define TAG_NUM_6				706
#define TAG_NUM_7				707
#define TAG_NUM_8				708
#define TAG_NUM_9				709
#define TAG_NUM_10				710
#define TAG_NUM_11				711
#define TAG_NUM_12				712
#define TAG_NUM_13				713
#define TAG_NUM_14				714
#define TAG_NUM_BUTTON_1		715
#define TAG_NUM_BUTTON_2		716
#define TAG_NUM_BUTTON_3		717
#define TAG_NUM_BUTTON_4		718
#define TAG_NUM_BUTTON_5		719
#define TAG_NUM_BUTTON_6		720
#define TAG_NUM_BUTTON_7		721
#define TAG_NUM_BUTTON_8		722
#define TAG_NUM_BUTTON_9		723
#define TAG_NUM_BUTTON_10		724
/****************************************/


class SceneGame_2_2_2 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	bool Problem[4];
	bool ProblemAnswer[10];
	bool UserAnswer[10];

	TextField* TextNum[14];
	Button* ButtonNum[10];

	Button* ButtonConfirm;
	Button* Button_O;
	Button* Button_X;

public:
	SceneGame_2_2_2();
	~SceneGame_2_2_2();

	static Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_2_2_2);
	
	void setProblemNum(int Index, bool Value);
	void problemInit();	//문제 초기화
	void problemCalculate();	//답 계산하기
	void touchButtonNum(int ButtonTag);		//버튼 클릭시 호출
	void UserAnswerInit();	//정답 초기화
	bool UserAnswerCheck();	//계산 결과 확인
	void play();	//실행하기

	void onTouch(Ref* sender, Widget::TouchEventType type);		//터치 이벤트
	void appplicationExit();
	void ScheduleAudio(float dt);
};

#endif
