﻿#ifndef __SCENE_GAME_2_2_1_H__
#define __SCENE_GAME_2_2_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

#include "Star.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "SceneCreator.h"

/*************** 태그 정보 ***************/
#define TAG_OPERATION			701
#define TAG_STAR_1				702
#define TAG_STAR_2				703
#define TAG_STAR_3				704
#define TAG_STAR_4				705
#define TAG_STAR_5				706
#define TAG_STAR_6				707
#define TAG_STAR_7				708
#define TAG_STAR_8				709
#define TAG_STAR_9				710
/****************************************/


class SceneGame_2_2_1 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	int ProblemNum;
	bool ProblemAnswer[3];
	bool UserAnswer[3];

	Star* Stars[9];
	Button* ButtonConfirm;
	TextField* ProblemState;
	TextField* Operation;

	Button* Button_O;
	Button* Button_X;

public:
	SceneGame_2_2_1();
	~SceneGame_2_2_1();

	static Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_2_2_1);
	
	void setProblemNum(int Index, bool Value);
	void problemInit();	//문제 초기화
	void UserAnswerInit();	//정답 초기화
	bool UserAnswerCheck();	//계산 결과 확인
	void play(bool Ck);	//실행하기

	void onTouch(Ref* sender, Widget::TouchEventType type);		//터치 이벤트
	void appplicationExit();
	void ScheduleAudio(float dt);
};

#endif
