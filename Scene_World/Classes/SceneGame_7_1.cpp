﻿// 기본 헤더
#include "SceneGame_7_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "ui\CocosGUI.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"
//#include <ctime>
#include "SceneCreator.h"

//화면 전환
#include "SceneStage_7.h"

USING_NS_CC;
using namespace ui;

bool isReplay71;

SceneGame_7_1::SceneGame_7_1()
{
	//Option & System
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_7.mp3";

	//Game
	Corrdinate[0] = " ◇● "; Corrdinate[1] = " ○● ";
	Corrdinate[2] = " ▲● "; Corrdinate[3] = " ★● ";
	Corrdinate[4] = " ◇☆ "; Corrdinate[5] = " ○☆ ";
	Corrdinate[6] = " ▲☆ "; Corrdinate[7] = " ★☆ ";
	Corrdinate[8] = " ◇◆ "; Corrdinate[9] = " ○◆ ";
	Corrdinate[10] = " ▲◆ "; Corrdinate[11] = " ★◆ ";
	Corrdinate[12] = " ◇△ "; Corrdinate[13] = " ○△ ";
	Corrdinate[14] = " ▲△ "; Corrdinate[15] = " ★△ ";
	Problem = "";
	for (int i = 0; i < 4; i++){
		ProblemNum[i] = 0;
		UserAnswer[i] = 0;
	}
}

SceneGame_7_1::~SceneGame_7_1()
{
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_7_1::createScene(bool _booltmp)
{
	isReplay71 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_7_1::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_7_1::init()
{

	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_7_1 = CSLoader::createNode("Scene_Game_7_1.csb");
	addChild(CSB_SceneGame_7_1);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_7_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_7_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_7_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneGame_7_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//버튼 가져오기
	ButtonPlay = (Button*)CSB_SceneGame_7_1->getChildByTag(TAG_BUTTON_START);
	ButtonReset = (Button*)CSB_SceneGame_7_1->getChildByTag(TAG_BUTTON_RESET);
	Button_O = (Button*)CSB_SceneGame_7_1->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_7_1->getChildByTag(TAG_X);

	
	for (int i = 0; i < 16; i++){
		int  Tag = TAG_BUTTON_P1 + i;
		ButtonPanels[i] = (Panel*)CSB_SceneGame_7_1->getChildByTag(Tag);
		ButtonPanels[i]->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));
	}

	//텍스트 필드 가져오기
	ProblemText = (TextField*)CSB_SceneGame_7_1->getChildByTag(TAG_TEXTFIELD_PROB);

	//터치이벤트
	ButtonPlay->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));
	ButtonReset->addTouchEventListener(CC_CALLBACK_2(SceneGame_7_1::onTouch, this));

	problemInit();

	return true;
}

void SceneGame_7_1::problemInit(){
	Problem = "";
	ProblemText->setString(Problem);

	SelectCount = 0;

	//버튼 선택 초기화
	for (int i = 0; i < 16; i++){
		ButtonPanels[i]->setTouchEnabled(true);
		ButtonPanels[i]->setBright(true);
		AnswerExist[i] = false;
	}

	//문제 생성
	for (int j = 0; j < 4;){
		int random = rand() % 15;

		if (AnswerExist[random] == false){
			AnswerExist[random] = true;
			ProblemNum[j] = random;
			++j;
		}
	}

	//문제 표시&사용자입력값 초기화
	for (int k = 0; k < 4; k++){
		int index = ProblemNum[k];
		Problem.append(Corrdinate[index]);
		UserAnswer[k] = 0;
	}

	ProblemText->setString(Problem);

}

void SceneGame_7_1::inputPanel(int tag){
	if (tag >= 701 && tag <= 716){
		if (SelectCount == 4) {
		}
		else {
			int index = tag - 701;
			ButtonPanels[index]->setBright(false);
			ButtonPanels[index]->setTouchEnabled(false);
			UserAnswer[SelectCount] = index;
			SelectCount++;
		}
	}

}

void SceneGame_7_1::play(bool AC){

	if (AC == true){ 
		Button_O->setVisible(true); 
	}else if (AC == false){ 
		Button_X->setVisible(true); 
	}
}

bool SceneGame_7_1::answerCheck(){
	int Count = 0;

	for (int i = 0; i < 4; i++){
		if (ProblemNum[i] != UserAnswer[i]){
			UserAnswerCheck[i] = false;
		}
		else{
			UserAnswerCheck[i] = true;
			Count = Count + 1;
		}
	}

	//정답 4개 모두 맞췄다면
	if (Count == 4){
		return true;
	}
	else
		return false;
}

void SceneGame_7_1::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){
		

		auto SelectButton = (Button*)sender;
		int Tag = SelectButton->getTag();

		switch (Tag)
		{	
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();

				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_BACK:{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				
				Scene* scene;

				if (isReplay71) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
				}
				else {
					scene = TransitionFade::create(0.5, SceneStage_7::createScene());
				}
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_BUTTON_EXIT:
			{
				


				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);

				ButtonReset->setTouchEnabled(false);
				ButtonPlay->setTouchEnabled(false);
				Button_O->setTouchEnabled(false);
				Button_X->setTouchEnabled(false);

				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);

				ButtonReset->setTouchEnabled(true);
				ButtonPlay->setTouchEnabled(true);
				Button_O->setTouchEnabled(true);
				Button_X->setTouchEnabled(true);

				break;
			}
			case TAG_EXIT:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();

				break;
			}
			case TAG_BUTTON_START:{
				
				play(answerCheck());
				break;
			}
			case TAG_BUTTON_RESET:{
				
				problemInit();
				break;
			}
			case TAG_O:{
				
				Button_O->setVisible(false);
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				Scene* scene;

				if (isReplay71) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
				}
				else {
					logManager->tmpFunction(2);
					logManager->setLog(0, 0, 0);					
					scene = TransitionFade::create(0.5, SceneCreator::createScene(7, 4));
				}
				Director::getInstance()->replaceScene(scene);
				break; 
			}
			case TAG_X:{
				
				Button_X->setVisible(false);
				problemInit();
				break; 
			}
			default:
				inputPanel(Tag);
				break;
		}
	}
}

void SceneGame_7_1::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_7_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}