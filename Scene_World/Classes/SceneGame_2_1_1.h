﻿#ifndef __SCENE_GAME_2_1_1_H__
#define __SCENE_GAME_2_1_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

#include "Binary.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "SceneCreator.h"

/*************** 태그 정보 ***************/
#define TAG_BINARY_1			701
#define TAG_BINARY_2			702
#define TAG_BINARY_3			703
#define TAG_BINARY_4			704
#define TAG_PROBLEM				705
/****************************************/


class SceneGame_2_1_1 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

public:

	int ProblemDecimal;
	int ProblemNum;
	bool ProblemAnswer[4];
	bool UserAnswer[4];

	Button* Button_O;
	Button* Button_X;
	TextField* ProblemTextField;

	Button* ButtonStart;
	Button* ButtonReset;

	Binary* Binaries[4];

	SceneGame_2_1_1();
	~SceneGame_2_1_1();

	static cocos2d::Scene* createScene(int i, bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_2_1_1);

	void setProblemNum(int Index, bool Value);
	void problemInit();	//문제 초기화
	void UserAnswerInit();	//정답 초기화
	bool UserAnswerCheck();	//계산 결과 확인
	void play(bool Ck);	//실행하기

	void CheckOX();

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

};

#endif 