﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_GAME_7_1_H__
#define __SCENE_GAME_7_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "Panel.h"

/*************** 태그 정보 ***************/
#define  TAG_BUTTON_P1			701
#define  TAG_BUTTON_P2			702
#define  TAG_BUTTON_P3			703
#define  TAG_BUTTON_P4			704
#define  TAG_BUTTON_P5			705
#define  TAG_BUTTON_P6			706
#define  TAG_BUTTON_P7			707
#define  TAG_BUTTON_P8			708
#define  TAG_BUTTON_P9			709
#define  TAG_BUTTON_P10			710
#define  TAG_BUTTON_P11			711
#define  TAG_BUTTON_P12			712
#define  TAG_BUTTON_P13			713
#define  TAG_BUTTON_P14			714
#define  TAG_BUTTON_P15			715
#define  TAG_BUTTON_P16			716
#define  TAG_TEXTFIELD_PROB		717
/****************************************/


class SceneGame_7_1 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	Button* ButtonReset;
	Button* ButtonPlay;
	Button* Button_O;
	Button* Button_X;

	TextField* ProblemText;

	Panel* ButtonPanels[16];

	std::string Corrdinate[16];

	int SelectCount;
	int ProblemNum[4];
	int UserAnswer[4];
	bool UserAnswerCheck[4];
	bool AnswerExist[16];
	std::string Problem;

public:
	
	SceneGame_7_1();
	~SceneGame_7_1();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_7_1);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	void problemInit();			//초기화함수
	void inputPanel(int tag);
	void play(bool AC);
	bool answerCheck();
};


#endif