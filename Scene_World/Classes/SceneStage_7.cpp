﻿//기본 헤더
#include "SceneStage_7.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneWorld.h"
#include "BattleSystem.h"
#include "SceneExplainCreator.h"

USING_NS_CC;
using namespace ui;


SceneStage_7::SceneStage_7(){
	FlagOption = OFF;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_7.mp3";
}

SceneStage_7::~SceneStage_7(){

}


Scene* SceneStage_7::createScene()
{
	auto scene = Scene::create();

	auto layer = SceneStage_7::create();
	scene->addChild(layer);

	return scene;
}

bool SceneStage_7::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//Import csb file
	auto CSB_SceneStage_7 = CSLoader::createNode("Scene_Stage_7.csb");
	addChild(CSB_SceneStage_7);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneStage_7->getChildByTag(TAG_LAYER_OPTION);
	ButtonOption = (Button*)CSB_SceneStage_7->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneStage_7::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneStage_7::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneStage_7::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneStage_7::onTouch, this));
	LayerOption->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}
	
	//Scroll
	Scroll = (ScrollView*)CSB_SceneStage_7->getChildByTag(TAG_SCROLL);

	//Map
	Map_1 = (Button*)Scroll->getChildByTag(TAG_MAP_1);
	Map_2 = (Button*)Scroll->getChildByTag(TAG_MAP_2);
	Map_3 = (Button*)Scroll->getChildByTag(TAG_MAP_3);

	Map_1->addTouchEventListener(CC_CALLBACK_2(SceneStage_7::onTouch, this));
	Map_2->addTouchEventListener(CC_CALLBACK_2(SceneStage_7::onTouch, this));
	Map_3->addTouchEventListener(CC_CALLBACK_2(SceneStage_7::onTouch, this));
		
	return true;
}

void SceneStage_7::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		
		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_EXIT:
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();
				break;
			}
			case TAG_BUTTON_BACK:
			{
				
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();

				auto scene = TransitionFade::create(0.5, SceneWorld::createScene());
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_MAP_1:
			{
				
				auto scene = TransitionFade::create(0.5, SceneGame_7_1::createScene());
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_MAP_2:
			{
				
			
				auto scene = TransitionFade::create(0.5, BattleSystem::createScene(3));
				Director::getInstance()->pushScene(scene);
			
				break;
			}
			case TAG_MAP_3:
			{
				
				
				break;
			}
			default:
				break;
		}
	}
}

void SceneStage_7::appplicationExit()
{
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}