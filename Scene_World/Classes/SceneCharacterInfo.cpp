#include "SceneCharacterInfo.h"


SceneCharacterInfo::SceneCharacterInfo()
{
	logManager = new LogManagement;
}


SceneCharacterInfo::~SceneCharacterInfo()
{
	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneCharacterInfo::createScene()
{
	auto scene = Scene::create();

	auto layer = SceneCharacterInfo::create();
	scene->addChild(layer);

	return scene;
}


// on "init" you need to initialize your instance
bool SceneCharacterInfo::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//타이틀 씬 불러오기
	auto CSB_CharacterInfo = CSLoader::createNode("Scene_CharacterInfo.csb");
	addChild(CSB_CharacterInfo);

	ButtonReplay = (Button*)CSB_CharacterInfo->getChildByTag(TAG_REPLAY);
	ButtonBack = (Button*)CSB_CharacterInfo->getChildByTag(TAG_BUTTON_BACK);

	Chapter_Progress = (TextField*)CSB_CharacterInfo->getChildByTag(TAG_TEXT_PROGESS);
	Chapter_Progress->setString("");

	for (int i = 0; i < 6; i++){
		Ruby[i] = (Sprite*)CSB_CharacterInfo->getChildByTag(TAG_RUBY_1 + i);
		Ruby[i]->setVisible(true);
	}

	ButtonReplay->addTouchEventListener(CC_CALLBACK_2(SceneCharacterInfo::onTouch, this));
	ButtonBack->addTouchEventListener(CC_CALLBACK_2(SceneCharacterInfo::onTouch, this));

	rubyState();

	return true;
}

void SceneCharacterInfo::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		
		auto SelectButton = (Button*)sender;

		int tmp = SelectButton->getTag();
		switch (tmp)
		{
		case TAG_REPLAY:
		{
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			auto scene = TransitionFade::create(0.5, QuestReplay::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_BUTTON_BACK : {
			Director::getInstance()->popScene();
			break;
		}
		default:
			break;
		}
	}

}

void SceneCharacterInfo::rubyState(){

	int chapter = logManager->getChapter();
	Chapter_Progress->setString(commonLib::to_string(chapter));
	if (chapter == 7){
		for (int i = 0; i < 6; i++){
			Ruby[i]->setVisible(true);
		}
	}
	else if (chapter == 0 || chapter == 1 || chapter == 2){
		for (int i = 1; i < 6; i++){
			Ruby[i]->setVisible(false);
		}
	}
	else{
		for (int i = chapter-1; i < 6; i++){
			Ruby[i]->setVisible(false);
		}

	}

	
}