﻿#ifndef COMMON_TAG_SET__H
#define COMMON_TAG_SET__H

/*************** 태그 정보 ***************/

//시스템 메뉴
#define TAG_BACKGROUND			1
#define TAG_SCROLL				2
#define TAG_BUTTON_BACK			3
#define TAG_BUTTON_OPTION		4
#define TAG_BUTTON_PLAY			5
#define TAG_BUTTON_EXIT			6
#define	TAG_AUDIO				7
#define TAG_CANCLE				8
#define TAG_EXIT				9
#define TAG_QUEST_INFO		10

#define TAG_LAYER_OPTION		11
#define TAG_LAYER_EXIT			12
#define TAG_LAYER_QUEST			13

#define TAG_BUTTON_PLAY_CONTINUE		14

//파티클
#define TAG_PARTICLE_CHARACTER	50
#define TAG_PARTICLE_MONSTER	51

//음악 및 효과음
#define TAG_BGM					101

#define TAG_MAP					201
#define TAG_NOTICE				251
//푸쉬

//대화 소스
#define TAG_LEFT_POTRAIT		401
#define	TAG_RIGHT_POTRAIT		402

#define TAG_SCRIPT_NAME			403
#define	TAG_SCRIPT_DIALOGUE		404

//게임 - 공통
#define	TAG_BUTTON_START		601
#define TAG_BUTTON_CONFIRM		602
#define TAG_BUTTON_RESET		603
#define TAG_PROBLEM_STATE		604
#define TAG_O					605
#define TAG_X					606
#define TAG_ACCEPT				607
#define TAG_REFUSE				608

//게임  - 개별 (701 ~ )

//퀘스트 관리
#define TAG_BUTTON_ACCEPT		801
#define TAG_BUTTON_REFUSE		802
#define TAG_LABEL_CHAPTER		803
#define TAG_LABEL_QUEST			804
#define TAG_LABEL_QUEST_TITLE	805
#define TAG_TEXTFIELD_INFORM	806
#define TAG_TEXTFIELD_GOAL		807
#endif