﻿#ifndef __SCENE_TITLE_H__
#define __SCENE_TITLE_H__
//#define COCOS2D_DEBUG 1

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include "CommonTagSet.h"
#include "LogManagement.h"

USING_NS_CC;
using namespace ui;

class SceneTitle : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

public:
	SceneTitle();
	~SceneTitle();

	LogManagement* logManager;

	Button* ButtonStart;
	Button* ButtonQuit;
	Button* ButtonStartContinue;
	
    static Scene* createScene();
    virtual bool init();
	CREATE_FUNC(SceneTitle);

	void onTouch(Ref* sender, Widget::TouchEventType type);	//터치 이벤트
	void appplicationExit();								//어플리케이션 종료	
};

#endif
