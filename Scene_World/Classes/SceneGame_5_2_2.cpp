﻿//기본 헤더
#include "SceneGame_5_2_2.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "SceneGame_5_2_1.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneStage_5.h"

USING_NS_CC;
using namespace ui;

std::string UserAnswer;

bool isReplay522;

SceneGame_5_2_2::SceneGame_5_2_2()
{
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_5.mp3";

	int maze[MAZE_SIZE][MAZE_SIZE] = {
		{ 1, 1, 0, 0, 0, 0, 0, 0},
		{ 0, 1, 1, 0, 0, 0, 0, 0},
		{ 0, 0, 1, 1, 0, 1, 1, 1},
		{ 0, 0, 0, 1, 0, 1, 0, 1},
		{ 0, 1, 1, 2, 0, 2, 0, 1},
		{ 0, 1, 0, 0, 2, 1, 0, 1},
		{ 0, 1, 0, 2, 1, 0, 0, 1},
		{ 0, 1, 1, 1, 0, 0, 0, 1}
	};

	for (int i = 0; i < MAZE_SIZE; i++){
		for (int j = 0; j < MAZE_SIZE; j++){
			Maze[i][j] = maze[i][j];
		}
	}
}

SceneGame_5_2_2::~SceneGame_5_2_2()
{
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_5_2_2::createScene(std::string useranswer, bool _booltmp)
{
	isReplay522 = _booltmp;

	UserAnswer = useranswer;
	auto scene = Scene::create();

	auto layer = SceneGame_5_2_2::create();
	scene->addChild(layer);
	
	return scene;
}

Scene* SceneGame_5_2_2::createScene(bool _booltmp)
{
	isReplay522 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_5_2_2::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_5_2_2::init()
{

	if (!Layer::init())
	{
		return false;
	}
	
	auto CSB_SceneGame_5_2_2 = CSLoader::createNode("Scene_Game_5_2_2.csb");
	addChild(CSB_SceneGame_5_2_2);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_5_2_2->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_5_2_2->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_5_2_2->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_5_2_2::ScheduleAudio), 1.5f);
	this->schedule(schedule_selector(SceneGame_5_2_2::scheduleCollision), 0.1f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//버튼 및 스프라이트 가져오기
	ButtonPlay = (Button*)CSB_SceneGame_5_2_2->getChildByTag(TAG_BUTTON_PLAY);
	ButtonNext = (Button*)CSB_SceneGame_5_2_2->getChildByTag(TAG_NEXT);
	Button_O = (Button*)CSB_SceneGame_5_2_2->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_5_2_2->getChildByTag(TAG_X);

	Player = (Sprite*)CSB_SceneGame_5_2_2->getChildByTag(TAG_PLAYER);

	for (int i = 0; i < 4; i++)
		Heart[i] = (Sprite*)CSB_SceneGame_5_2_2->getChildByTag(TAG_HEART +  i);

	
	//터치이벤트 추가
	ButtonPlay->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));
	ButtonNext->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));

	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_2::onTouch, this));
		
	Button_O->setVisible(false);
	Button_X->setVisible(false);

	ButtonPlay->setVisible(true);
	ButtonNext->setVisible(false);
	
	return true;
}

bool SceneGame_5_2_2::Simulation()
{
	auto callBack = CallFunc::create(CC_CALLBACK_0(SceneGame_5_2_2::play, this));
	MoveBy* actions[ACTION_MAX];

	for (int i = 0; i < ACTION_MAX; i++)
		actions[i] = MoveBy::create(1.0f, Vec2(0, 0));
	
	int x = 0, y = 0;
	int actionIndex = 0;
	int index = 0;

	//test
	//UserAnswer = "412141214122513223421151411151411151124225";

	while (index < UserAnswer.length())
	{		
		int Direction = UserAnswer.at(index) - '0';
		int CountOfBlock = UserAnswer.at(index + 1) - '0';
				
		if(Direction == UP)
		{
			int Move = IsMoveUp(CountOfBlock, x, y);
			actions[actionIndex] = MoveBy::create(SPEED, Vec2(0, MAZE_SIZE_HEIGHT * Move));

			x = x - Move;

			if (Move < CountOfBlock)
				break;
			
		}
		else if(Direction == DOWN)
		{
			int Move = IsMoveDown(CountOfBlock, x, y);
			actions[actionIndex] = MoveBy::create(SPEED, Vec2(0, -MAZE_SIZE_HEIGHT * Move));

			x = x + Move;

			if (Move < CountOfBlock)
				break;			
		}
		else if (Direction == LEFT)
		{
			int Move = IsMoveLeft(CountOfBlock, x, y);
			actions[actionIndex] = MoveBy::create(SPEED, Vec2(-MAZE_SIZE_WIDTH * Move, 0));

			y = y - Move;

			if (Move < CountOfBlock)
				break;
		}
		else if (Direction == RIGHT)
		{
			int Move = IsMoveRight(CountOfBlock, x, y);
			actions[actionIndex] = MoveBy::create(SPEED, Vec2(MAZE_SIZE_WIDTH * Move, 0));

			y = y + Move;

			if (Move < CountOfBlock)
				break;
		}
		else if (Direction == PICK)
		{
			if (Maze[x][y] == 2)
			{
				Maze[x][y] = PICK;
			}
		}

		index = index + 2;
		actionIndex = actionIndex + 1;
	}
	
	auto sequence = Sequence::create(actions[0], actions[1], actions[2], actions[3],
		actions[4], actions[5],	actions[6], actions[7], actions[8], actions[9],
		actions[10], actions[11], actions[12], actions[13], actions[14], actions[15],
		actions[16], actions[17], actions[18], actions[19], actions[20], actions[21],
		actions[22], actions[23], actions[24], actions[25], actions[26], actions[27],
		actions[28], actions[29], actions[30], actions[31], actions[32], actions[33],
		actions[34], actions[35], actions[36], actions[37], actions[38], actions[39],
		callBack, NULL);
	
	Player->runAction(sequence);
	
	if (x == DESTINATION_X && y == DESTINATION_Y)
		return true;
	else
		return false;
}

int SceneGame_5_2_2::IsMoveUp(int count, int x, int y)
{
	int Move = 0;
	int MaxCount = x;
	
	if (count > MaxCount)
		count = MaxCount;
	
	while(Move < count)
	{		
		if (Maze[x - Move][y] == 0)
			break;
		
		Move++;
	}

	return Move;
}

int SceneGame_5_2_2::IsMoveDown(int count, int x, int y)
{
	int Move = 0;
	int MaxCount = MAZE_SIZE - x - 1;

	if (count > MaxCount)
		count = MaxCount;
	
	while (Move < count)
	{
		if (Maze[x + Move + 1][y] == 0)
			break;

		Move++;
	}
	
	return Move;
}

int SceneGame_5_2_2::IsMoveLeft(int count, int x, int y)
{
	int Move = 0;
	int MaxCount = y;

	if (count > MaxCount)
		count = MaxCount;

	while (Move < count)
	{
		if (Maze[x][y - Move] == 0)
			break;
		
		Move++;
	}
	return Move;
}

int SceneGame_5_2_2::IsMoveRight(int count, int x, int y)
{
	int Move = 0;
	int MaxCount = MAZE_SIZE - y - 1;

	if (count > MaxCount)
		count = MaxCount;

	while (Move < count)
	{
		if (Maze[x][y + Move + 1] == 0)
			break;

		Move++;
	}
	
	return Move;
}

void SceneGame_5_2_2::scheduleCollision(float dt)
{
	Rect HeartBoundingBox[4];
	HeartBoundingBox[0] = Heart[0]->getBoundingBox();
	HeartBoundingBox[1] = Heart[1]->getBoundingBox();
	HeartBoundingBox[2] = Heart[2]->getBoundingBox();
	HeartBoundingBox[3] = Heart[3]->getBoundingBox();

	Rect PlayerBoundingBox = Player->getBoundingBox();

	if (HeartBoundingBox[0].intersectsRect(PlayerBoundingBox))
	{
		if (Maze[4][3] == PICK)
			Heart[0]->setVisible(false);
	}
	else if (HeartBoundingBox[1].intersectsRect(PlayerBoundingBox))
	{
		if (Maze[6][3] == PICK)
			Heart[1]->setVisible(false);
	}
	else if (HeartBoundingBox[2].intersectsRect(PlayerBoundingBox))
	{
		if (Maze[5][4] == PICK)
			Heart[2]->setVisible(false);
	}
	else if (HeartBoundingBox[3].intersectsRect(PlayerBoundingBox))
	{
		if (Maze[4][5] == PICK)
			Heart[3]->setVisible(false);
	}
	else
	{
	}
	
}

void SceneGame_5_2_2::play()
{
	bool result = Simulation();
	int HeartPick = 0;

	if (Maze[4][3] == PICK)
		HeartPick++;

	if (Maze[6][3] == PICK)
		HeartPick++;;

	if (Maze[5][4] == PICK)
		HeartPick++;

	if (Maze[4][5] == PICK)
		HeartPick++;

	ButtonPlay->setVisible(false);
	if (result == true && HeartPick == 4)	//정답인 경우
	{
		logManager->setChapterProgress(50);
		Button_O->setVisible(true);
	}
	else if (result == false || HeartPick != 4)	//오답인 경우
	{
		Button_X->setVisible(true);
	}
	

}

void SceneGame_5_2_2::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED)
	{
		

		auto SelectButton = (Button*)sender;
		int Tag = SelectButton->getTag();

		switch (Tag)
		{
		case TAG_BUTTON_PLAY:
		{
			play();
			break;
		}
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			ButtonNext->setTouchEnabled(false);
			ButtonPlay->setTouchEnabled(false);
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
									
			Director::getInstance()->popScene();			
			break;
		}	
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonNext->setTouchEnabled(true);
			ButtonPlay->setTouchEnabled(true);
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_O:
		{
			
			
			Button_O->setVisible(false);
			ButtonPlay->setVisible(false);
			ButtonNext->setVisible(true);
			
			break;
		}
		case TAG_X:
		{
			

			Button_X->setVisible(false);
			break;
		}
		case TAG_NEXT:
		{
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			
			Scene* scene;

			if (isReplay522) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_5::createScene());
			}
			Director::getInstance()->replaceScene(scene);
		}
		default:
			break;
		}
	}
}

void SceneGame_5_2_2::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_5_2_2::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}