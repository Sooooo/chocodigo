﻿#ifndef __SCENE_STAGE_6_H__
#define __SCENE_STAGE_6_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "checkProgress.h"
#include "SceneCreator.h"

/*************** 태그 정보 ***************/
#define TAG_MAP_1				201
#define TAG_MAP_2				202
#define TAG_MAP_3				203
/****************************************/


class SceneStage_6 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	///Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;
	checkProgress* cP; 

	//Map
	Button* Map[3];
		
	//Notice
	Sprite* Notice[3];
	Button* ButtonPlayerInfo;
public:
	SceneStage_6();
	~SceneStage_6();
	
	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(SceneStage_6);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};

#endif