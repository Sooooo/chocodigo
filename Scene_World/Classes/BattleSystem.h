﻿#pragma execution_character_set("UTF-8")

#ifndef __BATTLE_SYSTEM_H__
#define __BATTLE_SYSTEM_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include "Monsters.h"
#include "Player.h"

#include "CommonTagSet.h"
#include "LogManagement.h"

USING_NS_CC;
using namespace ui;

/*************** 태그 정보 ***************/
#define TAG_BOSS				701
#define TAG_BOSS_NAME			702
#define TAG_BOSS_HP				703
#define TAG_BOSS_TEXTURE		704
#define TAG_BATTLE_NEXT			705

#define TAG_LABEL				708
/****************************************/

#define SKILL_DURATION	3
#define HP_REDUCE_SPEED	4
#define FONT_FILE	"Game_Resource/System/Font/08SeoulNamsanB_0.ttf"

class BattleSystem : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	//Battle
	Label* BossName;
	Label* LabelNotice;
	Label* BossHP;
	Label* label;
		
	Sprite* SpriteBoss;
	Button* ButtonBoss;
	Button* ButtonNext;

	ProgressTimer* BossHPProgress;

	Monsters* monsters;
	Player* player;

	bool FlagWin;
	bool Click;
public:

	BattleSystem();
	~BattleSystem();

	void labelInit();

	static cocos2d::Scene* createScene(int bossNum);
	virtual bool init();
	CREATE_FUNC(BattleSystem);
	
	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

};

#endif 