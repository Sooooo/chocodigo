﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_GAME_5_1_H__
#define __SCENE_GAME_5_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "Dictionary.h"
#include "LogManagement.h"


/*************** 태그 정보 ***************/
#define  TAG_TEXT_PROB			701
#define  TAG_TEXTBOOK_LEFT		702
#define  TAG_TEXTBOOK_RIGHT		703
#define  TAG_BUTTON_BOOK_PREV	704
#define  TAG_BUTTON_BOOK_NEXT	705
#define  TAG_BUTTON_FAIL		706	
/****************************************/


class SceneGame_5_1 : public Layer , public Dic
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	Button* ButtonStart;
	Button* ButtonReset;
	Button* Button_O;
	Button* Button_X;
	Button* ButtonFail;

	Button* ButtonLeft;
	Button* ButtonRight;

	TextField* TextLeft;
	TextField* TextRight;
	TextField* TextProblem;

	int ProblemIndex;
	int page_L;
	int page_R;
	int L_backup;
	int R_backup;
	int first;
	int last;
	int Random;
	int L_init;
	int R_init;
	int temp;

public:
	
	SceneGame_5_1();
	~SceneGame_5_1();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_5_1);


	void problemInit();
	void ProblemAnswerCheck();
	void UserAnswerInit();
	void play();
	void PageLeft();
	void PageRight();

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};


#endif 