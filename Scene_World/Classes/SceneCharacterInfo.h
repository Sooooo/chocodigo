#ifndef __SCENE_C_INFO_H__
#define __SCENE_C_INFO_H__


#include "cocos2d.h"
#include "ui\CocosGUI.h"

#include "CommonTagSet.h"
#include "QuestReplay.h"
#include "LogManagement.h"

USING_NS_CC;
using namespace ui;

#define TAG_REPLAY			701
#define TAG_TEXT_PROGESS	702
#define TAG_RUBY_1			703
#define TAG_RUBY_2			704
#define TAG_RUBY_3			705
#define TAG_RUBY_4			706
#define TAG_RUBY_5			707
#define TAG_RUBY_6			708

class SceneCharacterInfo : public Layer
{
private : 
	Button* ButtonReplay;
	Button* ButtonBack;
	Sprite* Ruby[6];
	TextField* Chapter_Progress;
	//Log Management
	LogManagement* logManager;

public:

	SceneCharacterInfo();
	~SceneCharacterInfo();

	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(SceneCharacterInfo);

	void rubyState();
	void onTouch(Ref* sender, Widget::TouchEventType type);		//터치 이벤트
};

#endif