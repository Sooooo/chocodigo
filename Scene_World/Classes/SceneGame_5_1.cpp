﻿// 기본 헤더
#include "SceneGame_5_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneStage_5.h"

USING_NS_CC;
using namespace ui;
using namespace std;

bool isReplay51;

SceneGame_5_1::SceneGame_5_1(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_5.mp3";

	Dic();
}

SceneGame_5_1::~SceneGame_5_1(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_5_1::createScene(bool _booltmp)
{
	isReplay51 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_5_1::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_5_1::init()
{

	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_5_1 = CSLoader::createNode("Scene_Game_5_1.csb");
	addChild(CSB_SceneGame_5_1);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_5_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_5_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_5_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_5_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//버튼 가져오기
	ButtonStart = (Button*)CSB_SceneGame_5_1->getChildByTag(TAG_BUTTON_START);
	ButtonLeft = (Button*)CSB_SceneGame_5_1->getChildByTag(TAG_BUTTON_BOOK_PREV);
	ButtonRight = (Button*)CSB_SceneGame_5_1->getChildByTag(TAG_BUTTON_BOOK_NEXT);
	Button_O = (Button*)CSB_SceneGame_5_1->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_5_1->getChildByTag(TAG_X);
	ButtonReset = (Button*)CSB_SceneGame_5_1->getChildByTag(TAG_BUTTON_RESET);
	ButtonFail = (Button*)CSB_SceneGame_5_1->getChildByTag(TAG_BUTTON_FAIL);

	//텍스트필드 가져오기
	TextLeft = (TextField*)CSB_SceneGame_5_1->getChildByTag(TAG_TEXTBOOK_LEFT);
	TextRight = (TextField*)CSB_SceneGame_5_1->getChildByTag(TAG_TEXTBOOK_RIGHT);
	TextProblem = (TextField*)CSB_SceneGame_5_1->getChildByTag(TAG_TEXT_PROB);

	//터치이벤트 추가
	ButtonStart->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	ButtonLeft->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	ButtonRight->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	ButtonReset->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));
	ButtonFail->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_1::onTouch, this));

	problemInit();

	return true;
}


void SceneGame_5_1::problemInit(){

	ButtonLeft->setVisible(true);
	ButtonRight->setVisible(true);

	srand((unsigned int)time(NULL));
	ProblemIndex = rand() % 41 + 0;
	Random = (rand() % 42 + 1);

	if (Random % 2 == 0){
		page_R = Random;
		page_L = Random - 1;
	}
	else{
		page_L = Random;
		page_R = Random + 1;
	}

	if (ProblemIndex == page_L || ProblemIndex == page_R){
		ProblemIndex = rand() % 41 + 0;
	}

	//페이지 저장
	L_backup = page_L;
	R_backup = page_R;
	L_init = page_L;
	R_init = page_R;
	first = 1;
	last = 42;

	TextProblem->setString(dic[ProblemIndex]);
	TextLeft->setString(dic[page_L-1]);
	TextRight->setString(dic[page_R-1]);

}

void SceneGame_5_1::PageLeft(){
	temp = ((page_L-first) / 2) + first;
	if (temp % 2 == 0){
		page_R = temp;
		page_L = temp - 1;
	}
	else{
		page_L = temp;
		page_R = temp + 1;
	}
	last = L_backup - 1;

	L_backup = page_L;
	R_backup = page_R;

	TextLeft->setString(dic[page_L - 1]);
	TextRight->setString(dic[page_R - 1]);
		
}
void SceneGame_5_1::PageRight(){
	temp = (last - page_R) / 2 + page_R;
	if (temp % 2 == 0){
		page_R = temp;
		page_L = temp - 1;
	}
	else{
		page_L = temp;
		page_R = temp + 1;
	}

	first = R_backup + 1;

	L_backup = page_L;
	R_backup = page_R;

	TextLeft->setString(dic[page_L - 1]);
	TextRight->setString(dic[page_R - 1]);
}

void SceneGame_5_1::play()
{
	if (ProblemIndex == page_L - 1 || ProblemIndex == page_R - 1)	//정답인 경우
	{
		Button_O->setVisible(true);
	}
	else	//오답인 경우
	{
		Button_X->setVisible(true);

	}

}
void SceneGame_5_1::UserAnswerInit(){
	page_L = L_init;
	page_R = R_init;

	first = 1;
	last = 42;

	TextLeft->setString(dic[page_L - 1]);
	TextRight->setString(dic[page_R - 1]);

}

void SceneGame_5_1::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){
		

		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_EXIT:
			{
				

				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);
				
				ButtonStart->setTouchEnabled(false);
				ButtonReset->setTouchEnabled(false);
				Button_O->setTouchEnabled(false);
				Button_X->setTouchEnabled(false);
				ButtonFail->setTouchEnabled(false);
				ButtonLeft->setTouchEnabled(false);
				ButtonRight->setTouchEnabled(false);

				break;
			}
			case TAG_BUTTON_BACK:{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				Scene* scene;

				if (isReplay51) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
				}
				else {
					scene = TransitionFade::create(0.5, SceneStage_5::createScene());
				}
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);

				ButtonStart->setTouchEnabled(true);
				ButtonReset->setTouchEnabled(true);
				Button_O->setTouchEnabled(true);
				Button_X->setTouchEnabled(true);
				ButtonFail->setTouchEnabled(true);
				ButtonLeft->setTouchEnabled(true);
				ButtonRight->setTouchEnabled(true);

				break;
			}
			case TAG_EXIT:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();

				break;
			}
			case TAG_BUTTON_START:{
				
				play();
				break;
			}
			case TAG_BUTTON_RESET:{
				
				UserAnswerInit();
				break;
			}
				
			case TAG_BUTTON_BOOK_PREV:{
				if (page_L == 1){
				}
				else if (first == page_L && last == page_R){
					if (page_L != (ProblemIndex + 1) && page_R != (ProblemIndex + 1)){
						ButtonFail->setVisible(true);
						UserAnswerInit();
					}
					break;
				}
				else
				{
					PageLeft();
				}

				break;
			}
			case TAG_BUTTON_BOOK_NEXT:{
				if (page_R == 42){
				}
				else if (first == page_L && last == page_R){
					if (page_L != (ProblemIndex + 1) && page_R != (ProblemIndex + 1)){
						ButtonFail->setVisible(true);
						UserAnswerInit();
					}
					break;

				}
				else
				{
					PageRight();
				}

				break;
			}
			case TAG_O:{

				Button_O->setVisible(false);
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				Scene* scene;

				if (isReplay51) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
				}
				else {
					logManager->setChapterProgress(20);	
					scene = TransitionFade::create(0.5, SceneStage_5::createScene());
				}

				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_X:{
				
				Button_X->setVisible(false);
				break;
			}
			case TAG_BUTTON_FAIL:{
				ButtonFail->setVisible(false);
				break;
			}

			default:
				break;
		}
	}
}

void SceneGame_5_1::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_5_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}