﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_GAME_5_2_1_H__
#define __SCENE_GAME_5_2_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include "CommonLib.h"

USING_NS_CC;
using namespace ui;
using namespace std;

#include "CommonTagSet.h"
#include "LogManagement.h"

//명령버튼 정의
enum MazeCommend{ Up=1, Down, Left, Right, Pick, Loop };

/******************** 태그 정보 ********************/

//		TAG_BUTTON_START					601

//		TAG_O								605
//		TAG_X								606

#define TAG_BUTTON_HINT						701
#define TAG_SPRITE_HINT						702
#define TAG_TEXTFIELD_HINT					703

#define TAG_BUTTON_MAP						704
#define TAG_TEXTFIELD_INPUT					705
#define TAG_BUTTON_MINUS					707
#define TAG_BUTTON_TEST						708

#define TAG_BUTTON_INPUT					710
//
#define TAG_BUTTON_UP						711
#define TAG_BUTTON_DOWN						712
#define TAG_BUTTON_LEFT						713
#define TAG_BUTTON_RIGHT					714
#define TAG_BUTTON_PICK						715
#define TAG_BUTTON_LOOP						716

#define TAG_BUTTON_1						717
#define TAG_BUTTON_2						718
#define TAG_BUTTON_3						719
#define TAG_BUTTON_4						720
#define TAG_BUTTON_5						721
#define TAG_BUTTON_6						722

/**************************************************/

class SceneGame_5_2_1 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	Button* Button_O;
	Button* Button_X;
	Button* ButtonConfirm;
	Button* ButtonReset;
	Button* ButtonMap;

	Button* ButtonHint;
	Sprite* SpriteHint;
	TextField* TextHint;

	Button* ButtonMinus;

	Button* B_UP;
	Button* B_DOWN;
	Button* B_LEFT;
	Button* B_RIGHT;
	Button* B_PICK;
	Button* B_LOOP;

	Button* B_1;
	Button* B_2;
	Button* B_3;
	Button* B_4;
	Button* B_5;
	Button* B_6;

	TextField* TextInput;

	ScrollView* Scroll;//스크롤 추가

	string InputText;			//화면에 출력할 스트링
	string MazeText;			//수로만 저장되는 스트링
	string beforeState;			//직전의 명령을 저장
	string LoopIn;				//루프안에 있는 스트링
	string FinalLoopIn;			//최종적으로 넘겨줄 루프값
	string DeleteLoopIn;

	char Num[6];				//넘버버튼
	string Commend[6];			//명령버튼 스트링
	int loopCheck;				//루프 판단
	int loopNum;				//루프 횟수

	bool hintVisible;
public:

	SceneGame_5_2_1();
	~SceneGame_5_2_1();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();

	CREATE_FUNC(SceneGame_5_2_1);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	void setRandomQuestion();
	
	void setInputButton();
	void setNumVisible();
	
	void problemInit();			//문제초기화
	void stateInit();			//추가 후 초기화 되는 버튼설정
	void inputDelete();			//빼기 함수
	void loopTrans();
	void loopString(string loop);	
};

#endif 