﻿#ifndef __SCENE_GAME_3_3_2_H__
#define __SCENE_GAME_3_3_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "SceneCreator.h"

/*************** 태그 정보 ***************/
#define TAG_FLOUR			701
#define TAG_EGG				702
#define TAG_MILK			703
#define TAG_INPUT_NUM_1		704
#define TAG_INPUT_NUM_2		705
#define TAG_INPUT_NUM_3		706

#define TAG_TEXT_NUM_1		708
#define TAG_TEXT_NUM_10		709
#define TAG_BUTTON_NUM_1	710
#define TAG_BUTTON_NUM_10	711

#define TAG_PROBNUM_1		712
#define TAG_PROBNUM_2		713
#define TAG_PROBNUM_3		714

#define TAG_Q1_SQUARE		715
#define TAG_Q2_SQUARE		716
#define TAG_Q2_CIRCLE_2		717
#define TAG_Q3_CIRCLE_1		718
#define TAG_Q3_DIAMOND		719
/****************************************/


class SceneGame_3_3_2 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;
public:

	Button* Button_O;
	Button* Button_X;
	Button* ButtonStart;

	Button* ButtonNum1;
	Button* ButtonNum10;

	Sprite* Flour;
	Sprite* Egg;
	Sprite* Milk;

	TextField* QuestCount;
	TextField* InputNum1;
	TextField* InputNum2;
	TextField* InputNum3;
	TextField* Text_1;
	TextField* Text_10;

	TextField* ProbNum1;
	TextField* ProbNum2;
	TextField* ProbNum3;
	
	Sprite* Q1;
	Sprite* Q2_1;
	Sprite* Q2_2;
	Sprite* Q3_1;
	Sprite* Q3_2;


	int ProblemNum;
	bool ProblemAnswer;
	int UserAnswer;
	int NumTen;
	int NumOne;

	int Input1;
	int Input2;
	int Input3;

	int CorrectAnswer;

	SceneGame_3_3_2();
	~SceneGame_3_3_2();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_3_3_2);

	void problemInit();
	void ProblemAnswerCheck();
	void UserAnswerInit();
	void UserAnswerCheck();
	void play();

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};


#endif 