﻿#ifndef __LOG_MANAGEMENT_H__
#define __LOG_MANAGEMENT_H__

#include "cocos2d.h"
#include "CommonLib.h"

using namespace cocos2d;

class LogManagement {
private :

	static LogManagement *_instance;

	bool prologue;

	int chapter;
	int currentQuest;
	int chapterProgress;
	int quest[16];

	bool quest2_2[6];
	
	bool systemsound;	// 0 : OFF, 1 : ON
	
	//item은 bool형 배열로 선언 예정

public : 
	LogManagement();
	~LogManagement();

	static LogManagement* sharedInstance(void)
	{
		if (_instance == nullptr)
			_instance = new LogManagement();
		return _instance;
	}

	static void release(void)
	{
		if (_instance != nullptr)
			delete _instance;
		_instance = nullptr;
	}

	void setQuest(int _quest);
	void setLog(int _progress, int _quest, int _chapter);
	void setChapterProgress(int _progress);
	int getChapter() { 			return chapter;	}
	int getQuest() {			return currentQuest;	}
	//int getProgress() { return progress; }
	int getChapterProgress() { return chapterProgress; }

	void setSoundOption(bool _tmp);
	bool getSystemSound() {		return systemsound;	}

	void setPrologue();
	bool getPrologue() { return prologue; }

	void setChapter(int _chapter);

	void printAll();

	void setQ2_2_Flag(int i);
	bool getQ2_2_Flag(int i) { return quest2_2[i]; };
	void reloadQ2_2();

	void tmpFunction(int i);

};

#endif