﻿#ifndef __PLAYER_H__
#define __PLAYER_H__

//#include <iostream>
#include "Character.h"

class Player : public Character
{
private:
	string effect;
	bool flag;
public:
	Player();

	string attack(Character* charater);
	void setEffect(int num);
	string getEffect();
};

#endif