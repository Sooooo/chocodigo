﻿#include "QuestReplay.h"

#include "SceneWorld.h"


QuestReplay::QuestReplay(){
}

QuestReplay::~QuestReplay(){
	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* QuestReplay::createScene()
{
	auto scene = Scene::create();

	auto layer = QuestReplay::create();
	scene->addChild(layer);

	return scene;
}

// on "init" you need to initialize your instance
bool QuestReplay::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//타이틀 씬 불러오기
	auto CSB_QuestReplay = CSLoader::createNode("Scene_Quest_Replay.csb");
	addChild(CSB_QuestReplay);

	//태그로 버튼 이미지 가져오기
	for (int i = 0; i < 17; i++) {
		Quest[i] = (Button*)CSB_QuestReplay->getChildByTag(TAG_QUEST + i);
		Quest[i]->addTouchEventListener(CC_CALLBACK_2(QuestReplay::onTouch, this));
	}
	
	Button_toWorld = (Button*)CSB_QuestReplay->getChildByTag(TAG_TO_WORLD);
	Button_toWorld->addTouchEventListener(CC_CALLBACK_2(QuestReplay::onTouch, this));
	
	return true;
}

void QuestReplay::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		
		auto SelectButton = (Button*)sender;

		int tmp = SelectButton->getTag();
		switch (tmp)
		{
			case TAG_TO_WORLD:
			{
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				auto scene = TransitionFade::create(0.5, SceneWorld::createScene());
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_QUEST:
			case TAG_QUEST + 1:
			case TAG_QUEST + 2:
			case TAG_QUEST + 3:
			case TAG_QUEST + 4:
			case TAG_QUEST + 5:
			case TAG_QUEST + 6:
			case TAG_QUEST + 7:
			case TAG_QUEST + 8:
			case TAG_QUEST + 9:
			case TAG_QUEST + 10:
			case TAG_QUEST + 11:
			case TAG_QUEST + 12:
			case TAG_QUEST + 13:
			case TAG_QUEST + 14:
			case TAG_QUEST + 15:
			case TAG_QUEST + 16:
			{
				tmp = tmp - TAG_QUEST + 1;
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				auto Scene = TransitionFade::create(0.5, SceneExplainCreator::createScene(tmp, true));
				Director::getInstance()->replaceScene(Scene);			
				break;
			}
			default:
				break;
		}
		}

}