﻿//기본 헤더
#include "BattleSystem.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "CommonLib.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneStage_2.h"
#include "SceneStage_3.h"
#include "SceneStage_4.h"
#include "SceneStage_5.h"
#include "SceneStage_6.h"
#include "SceneCreator.h"
#include "SceneWorld.h"

USING_NS_CC;
using namespace ui;

static int BossNum;

BattleSystem::BattleSystem()
{	
	//Option & System
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_Battle";

	//Battle
	monsters = new Monsters();
	player = new Player();

	FlagWin = false;
	Click = false;
}

BattleSystem::~BattleSystem()
{
	delete logManager;
	
	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* BattleSystem::createScene(int bossNum)
{
	BossNum = bossNum;
	auto scene = Scene::create();

	auto layer = BattleSystem::create();
	scene->addChild(layer);

	return scene;
}


bool BattleSystem::init()
{

	if (!Layer::init())
	{
		return false;
	}		

	//Import csb file
	auto CSB_SceneBattleSystem = CSLoader::createNode("Scene_Battle_System.csb");
	addChild(CSB_SceneBattleSystem);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneBattleSystem->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneBattleSystem->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneBattleSystem->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(BattleSystem::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(BattleSystem::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(BattleSystem::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(BattleSystem::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(BattleSystem::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(BattleSystem::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(BattleSystem::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}
	
	//Battle Widget
	SpriteBoss = (Sprite*)CSB_SceneBattleSystem->getChildByTag(TAG_BOSS_TEXTURE);
	ButtonNext = (Button*)CSB_SceneBattleSystem->getChildByTag(TAG_BATTLE_NEXT);
	BossName = (Label*)CSB_SceneBattleSystem->getChildByTag(TAG_BOSS_NAME);
	BossHP = (Label*)CSB_SceneBattleSystem->getChildByTag(TAG_BOSS_HP);
	
	//Boss Widget
	string imgSrc = monsters->getMonster(BossNum)->getImgSrc();
	SpriteBoss->setTexture(imgSrc);
	ButtonBoss = Button::create(imgSrc, "", "");
	ButtonBoss->setAnchorPoint(Point(0.5, 0.5));
	ButtonBoss->setPosition(SpriteBoss->getPosition());
	ButtonBoss->setSize(Size(523, 915));
	ButtonBoss->setTag(TAG_BOSS);
	ButtonBoss->addTouchEventListener(CC_CALLBACK_2(BattleSystem::onTouch, this));
	this->addChild(ButtonBoss);

	ButtonNext->addTouchEventListener(CC_CALLBACK_2(BattleSystem::onTouch, this));
	ButtonBoss->setTouchEnabled(true);
	
	//Label Notice
	LabelNotice = Label::createWithTTF("", FONT_FILE, 100);
	LabelNotice->setString("전투를 시작합니다.");
	LabelNotice->setColor(Color3B::WHITE);
	LabelNotice->enableOutline(Color4B::BLACK, 2);
	LabelNotice->setPosition(Point(960,500));
	this->addChild(LabelNotice);

	auto noticeBlink = Blink::create(2, 2);
	auto noticeVisible = ToggleVisibility::create();
	auto sequenceAction = Sequence::create(noticeBlink, noticeVisible, NULL);
	LabelNotice->runAction(sequenceAction);

	//Label
	labelInit();
	
	//Progress
	BossHPProgress = ProgressTimer::create(Sprite::create("Game_Resource/Battle_System/HP_Bar1.png"));
	BossHPProgress->setType(ProgressTimer::Type::BAR);
	BossHPProgress->setMidpoint(Point(0, 1));			//change direction
	BossHPProgress->setBarChangeRate(Point(1, 0));	//(1,0) : Horizontal, (1,0) : Vertical
	BossHPProgress->setPosition(Point(960, 95));

	this->addChild(BossHPProgress);
	BossHPProgress->setPercentage(100);


	return true;
}

void BattleSystem::labelInit()
{
	string Name = monsters->getMonster(BossNum)->getName();
	label = Label::createWithTTF("Boss", FONT_FILE, 100);
	//label->setString(Name);
	label->setColor(Color3B::BLACK);
	label->enableOutline(Color4B::WHITE, 2);
	BossName->setVisible(false);
	label->setPosition(BossName->getPosition());
	this->addChild(label);
}

void BattleSystem::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED)
	{

		auto SelectButton = (Button*)sender;
		int Tag = SelectButton->getTag();

		switch (Tag)
		{		
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			ButtonBoss->setTouchEnabled(false);
			ButtonNext->setTouchEnabled(false);

			for (int i = 0; i < 3; i++)
				Exit->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			
			if (BossNum == 1)
			{
				auto Scene = TransitionFade::create(0.5, SceneStage_2::createScene());
				Director::getInstance()->replaceScene(Scene);
			}
			else if (BossNum == 2)
			{
				auto Scene = TransitionFade::create(0.5, SceneStage_3::createScene());
				Director::getInstance()->replaceScene(Scene);
			}
			else if (BossNum == 3)
			{
				auto Scene = TransitionFade::create(0.5, SceneStage_4::createScene());
				Director::getInstance()->replaceScene(Scene);
			}
			else if (BossNum == 4)
			{
				auto Scene = TransitionFade::create(0.5, SceneStage_5::createScene());
				Director::getInstance()->replaceScene(Scene);
			}
			else if (BossNum == 5)
			{
				auto Scene = TransitionFade::create(0.5, SceneStage_6::createScene());
				Director::getInstance()->replaceScene(Scene);
			}
			else if (BossNum == 6)
			{
				//ending
				logManager->setChapterProgress(0);
				auto Scene = TransitionFade::create(0.5, SceneWorld::createScene());
				Director::getInstance()->replaceScene(Scene);
			}

			break;
		}
		case TAG_CANCLE:
		{
			
			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonBoss->setTouchEnabled(true);
			ButtonNext->setTouchEnabled(true);

			for (int i = 0; i < 3; i++)
				Exit->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_BOSS:
		{
			string s = player->attack(monsters->getMonster(BossNum));
			ParticleSystemQuad* particle = ParticleSystemQuad::create(s);
			this->addChild(particle);
			BossHPProgress->setPercentage(monsters->getMonster(BossNum)->getHPPercentage());

			if (monsters->getMonster(BossNum)->getChangeHP() <= 0)
			{
				//FlagWin = true;
				Label* Win = Label::createWithTTF("", FONT_FILE, 100);
				Win->setString("보스를 쓰러뜨렸다.");
				Win->setColor(Color3B::RED);
				Win->enableOutline(Color4B::WHITE, 2.0);
				Win->setPosition(Point(960, 500));
				this->addChild(Win);

				auto clickBlink = Blink::create(2, 2);
				auto clickVisible = ToggleVisibility::create();
				auto sequenceAction = Sequence::create(clickBlink, clickVisible, NULL);
				Win->runAction(sequenceAction);

				ButtonNext->setVisible(true);
				SpriteBoss->setVisible(false);
				ButtonBoss->setTouchEnabled(false);
				return;
			}

			//Click = true;
			break;
		}
		case TAG_BATTLE_NEXT:
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			Scene* scene;

			if (BossNum == 1)
			{
				logManager->setChapter(3);
				scene = TransitionFade::create(0.5, SceneCreator::createScene(2, 9));
				Director::getInstance()->replaceScene(scene);
			}
			else if (BossNum == 2)
			{
				logManager->setChapter(4);
				scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 12));
				Director::getInstance()->replaceScene(scene);
			}
			else if (BossNum == 3)
			{
				logManager->setChapter(5);
				scene = TransitionFade::create(0.5, SceneCreator::createScene(4, 9));
				Director::getInstance()->replaceScene(scene);
			}
			else if (BossNum == 4)
			{
				logManager->setChapterProgress(80);
				logManager->setChapter(6);
				scene = TransitionFade::create(0.5, SceneCreator::createScene(5, 9));
				Director::getInstance()->replaceScene(scene);
			}
			else if (BossNum == 5)
			{
				scene = TransitionFade::create(0.5, SceneCreator::createScene(6, 7));
				Director::getInstance()->replaceScene(scene);
			}
			else if (BossNum == 6)
			{
				//ending
				scene = TransitionFade::create(0.5, SceneCreator::createScene(7, 3));
				Director::getInstance()->replaceScene(scene);
			}
			break;
		}	
		default:
			break;
		}
	}
}

void BattleSystem::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void BattleSystem::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		if (BossNum = 6)
			BGM.append("2");
		BGM.append(".mp3");

		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}