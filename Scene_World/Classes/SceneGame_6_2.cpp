﻿//기본 헤더
#include "SceneGame_6_2.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
//#include <stdlib.h>

//화면 전환
#include "SceneStage_6.h"

USING_NS_CC;
using namespace ui;

bool isReplay62;

SceneGame_6_2::SceneGame_6_2(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_6.mp3";
}

SceneGame_6_2::~SceneGame_6_2(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_6_2::createScene(bool _booltmp)
{
	isReplay62 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_6_2::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_6_2::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_6_2 = CSLoader::createNode("Scene_Game_6_2.csb");
	addChild(CSB_SceneGame_6_2);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_6_2->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_6_2->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_6_2->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_6_2::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	/* System Button */

	Button_O = (Button*)CSB_SceneGame_6_2->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_6_2->getChildByTag(TAG_X);
	ButtonConfirm = (Button*)CSB_SceneGame_6_2->getChildByTag(TAG_BUTTON_START);

	//Add Touch Event

	ButtonConfirm->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));

	/* System Button End */
	
	Sprite_Mouse = (Sprite*)CSB_SceneGame_6_2->getChildByTag(TAG_SPRITE_MOUSE);
	Sprite_Ribbon = (Sprite*)CSB_SceneGame_6_2->getChildByTag(TAG_SPRITE_RIBBON);
	Sprite_Cheese = (Sprite*)CSB_SceneGame_6_2->getChildByTag(TAG_SPRITE_CHEESE);
	Sprite_Dot = (Sprite*)CSB_SceneGame_6_2->getChildByTag(TAG_SPRITE_DOT);
	TextField_Gram = (TextField*)CSB_SceneGame_6_2->getChildByTag(TAG_TEXTFIELD_GRAM);

	for (int i = 0; i < 6; i++) {
		Button_House[i] = (Button*)CSB_SceneGame_6_2->getChildByTag(TAG_BUTTON_HOUSE + i);
		Button_House[i]->addTouchEventListener(CC_CALLBACK_2(SceneGame_6_2::onTouch, this));
	}
	
	flag[0] = false;
	flag[1] = false;
	flag[2] = true;
	flag[3] = true;
	flag[4] = true;

	

	selAnswer = 6;

	//Set Randomize
	srand((unsigned int)time(NULL));
	setRandomQuestion();

	return true;
}
	
void SceneGame_6_2::setRandomQuestion(){

	auto action = TintTo::create(0, 255, 255, 255);
	Sprite_Mouse->runAction(action);
	gameAnswer = rand() % 4;

	if (rand() % 2) {
		TextField_Gram->setString("80g");
		flag[0] = true;
	}

	if (rand() % 2) {
		Sprite_Dot->setVisible(false);
		flag[1] = true;
	}

	if (rand() % 2) {
		Sprite_Cheese->setVisible(false);
		flag[2] = false;
	}

	if (rand() % 2) {
		Sprite_Ribbon->setVisible(false);
		flag[3] = false;
	}
	
	if (rand() % 2) {
		action = TintTo::create(0, 50, 50, 50);
		Sprite_Mouse->runAction(action);
		flag[4] = false;
	}
	
	if (flag[0]) {
		//초록
		if (flag[1]) {
			//점박X
			if (flag[2]) gameAnswer = 0;
			else gameAnswer = 1;
		}
		else {
			if (flag[3]) gameAnswer = 2;
			else gameAnswer = 3;
		}
	}
	else {
		if (flag[4]) gameAnswer = 4;
		else gameAnswer = 5;
	}
}

bool SceneGame_6_2::isCorrect() {

	if (selAnswer == gameAnswer)
		return true;
	else
		return false;

	//정답 확인
}


void SceneGame_6_2::onTouch(Ref* sender, Widget::TouchEventType type){

	if (type == Widget::TouchEventType::ENDED){

		auto SelectButton = (Button*)sender;

		int selectedbuf = SelectButton->getTag();

		switch (selectedbuf)
		{
			//System Button
		case TAG_BUTTON_START:

			if (isCorrect()) {
				Button_O->setVisible(true);
			}
			else {
				Button_X->setVisible(true);
			}

			break;
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);
			ButtonConfirm->setTouchEnabled(false);

			for (int i = 0; i < 6; i++)
				Button_House[i]->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			Scene* scene;

			if (isReplay62) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_6::createScene());
			}
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);
			ButtonConfirm->setTouchEnabled(true);

			for (int i = 0; i < 6; i++)
				Button_House[i]->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_O:
		{
			Button_O->setVisible(false);
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			Scene* scene;

			if (isReplay62) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				logManager->setChapterProgress(40);
				scene = TransitionFade::create(0.5, SceneStage_6::createScene());
			}
			Director::getInstance()->replaceScene(scene);
		}
		break;

		case TAG_X:
		{
			
			Button_X->setVisible(false);
			if (selAnswer == 6) {}
			else {
				for (int i = 0; i < 6; i++) {
					Button_House[i]->setTouchEnabled(true);
					Button_House[selAnswer]->setBright(true);
				}
			}
			setRandomQuestion();
		}
		break;

		//Game Button
		case TAG_BUTTON_HOUSE:
		case TAG_BUTTON_HOUSE + 1:
		case TAG_BUTTON_HOUSE + 2:
		case TAG_BUTTON_HOUSE + 3:
		case TAG_BUTTON_HOUSE + 4:
		case TAG_BUTTON_HOUSE + 5:

			if (selAnswer < 6) {
				Button_House[selAnswer]->setTouchEnabled(true);
				Button_House[selAnswer]->setBright(true);
			}
			selAnswer = selectedbuf - 710;
			Button_House[selAnswer]->setTouchEnabled(false);
			Button_House[selAnswer]->setBright(false);
			

			break;

		default:{ break; }

		}
	}
}


void SceneGame_6_2::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_6_2::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}