﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_GAME_5_3_H__
#define __SCENE_GAME_5_3_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

#include "CommonTagSet.h"
#include "CommonLib.h"
#include "LogManagement.h"

USING_NS_CC;
using namespace ui;


/******************** 태그 정보 ********************/

//		TAG_BUTTON_START					601

//		TAG_O								605
//		TAG_X								606

#define TAG_BUTTON_HINT						701
#define TAG_SPRITE_HINT						702
#define TAG_HINT_NUM						703

#define TAG_CIPHER_QUESTION					704
#define TAG_CIPHER_ANSWER					705

#define TAG_BUTTON_INPUT					710

/**************************************************/

class SceneGame_5_3 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	Button* Button_O;
	Button* Button_X;
	Button* ButtonConfirm;
	Button* ButtonReset;

	Button* ButtonHint;
	Sprite* SpriteHint;
	TextField* TextHint;

	TextField* TextQuestion;
	TextField* TextAnswer;

	Button* ButtonInput[26];

	bool flag = true;

public:
	SceneGame_5_3();
	~SceneGame_5_3();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();

	CREATE_FUNC(SceneGame_5_3);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	void setRandomQuestion();
	void setHELLOWORLD(int key);

	bool isCorrect();
	bool hintVisible;

	int key;
	std::string answer;

	std::string textStr[26];

	int cnt;

	//std::string textStr[26];
};

#endif 