﻿//기본 헤더
#include "SceneExplainCreator.h"

USING_NS_CC;
using namespace ui;

static int _questNum;

bool boolSEC;
// Stage 4 TAG 3 에서 잠시 기생중........

SceneExplainCreator::SceneExplainCreator(){
}

SceneExplainCreator::~SceneExplainCreator(){
	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneExplainCreator::createScene(int q, bool b)
{
	boolSEC = b;
	auto scene = Scene::create();

	_questNum = q;

	auto layer = SceneExplainCreator::create();
	scene->addChild(layer);


	return scene;
}

// on "init" you need to initialize your instance
bool SceneExplainCreator::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//Import csb file
	auto CSB_SceneExplainCreator = CSLoader::createNode("Scene_Explain_Creator.csb");
	addChild(CSB_SceneExplainCreator);

	//Option Layer & Button
	Button_Next = (Button*)CSB_SceneExplainCreator->getChildByTag(TAG_BUTTON_NEXT_PAGE);
	Button_Prev = (Button*)CSB_SceneExplainCreator->getChildByTag(TAG_BUTTON_PREV_PAGE);
	Button_All = (Button*)CSB_SceneExplainCreator->getChildByTag(TAG_BUTTON_ALL_PAGE);

	Button_Next->addTouchEventListener(CC_CALLBACK_2(SceneExplainCreator::onTouch, this));
	Button_Prev->addTouchEventListener(CC_CALLBACK_2(SceneExplainCreator::onTouch, this));
	Button_All->addTouchEventListener(CC_CALLBACK_2(SceneExplainCreator::onTouch, this));
	
	str.append(commonLib::to_string(_questNum));
	str.append("_");
	string tmp = str;
	tmp.append("1.png");
	Button_All->loadTextures(tmp, tmp);

	allpage[0] = 1;
	allpage[1] = 1;
	allpage[2] = 3;
	allpage[3] = 2;
	allpage[4] = 2;
	allpage[5] = 3;
	allpage[6] = 1;
	allpage[7] = 2;
	allpage[8] = 1;
	allpage[9] = 1;
	allpage[10] = 2;
	allpage[11] = 3;
	allpage[12] = 2;
	allpage[13] = 2;
	allpage[14] = 2;
	allpage[15] = 2;
	allpage[16] = 2;
	
	if (allpage[_questNum - 1] == 1) {
		Button_Next->setTouchEnabled(false);
		Button_Next->setBright(false);
	}

	Button_Prev->setTouchEnabled(false);
	Button_Prev->setBright(false);

	return true;
}

void SceneExplainCreator::changePage(bool next) {

	if (next) {
		//다음 페이지로 가는 경우
		if (currentpage < allpage[_questNum - 1]) {
			currentpage += 1;
		}
			
		Button_Prev->setTouchEnabled(true);
		Button_Prev->setBright(true);

		if (currentpage >= allpage[_questNum - 1]) {
			Button_Next->setTouchEnabled(false);
			Button_Next->setBright(false);
		}

	}
	else {
		if (currentpage > 1) {
			currentpage -= 1;
		}		

		Button_Next->setTouchEnabled(true);
		Button_Next->setBright(true);

		if (currentpage <= 1) {
			Button_Prev->setTouchEnabled(false);
			Button_Prev->setBright(false);
		}
	}


	string tmp = str;
	tmp.append(commonLib::to_string(currentpage));
	tmp.append(".png");
	Button_All->loadTextures(tmp,tmp);
	
}

void SceneExplainCreator::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)
	{
		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_NEXT_PAGE:
		{
			changePage(true);
			break;
		}
		case TAG_BUTTON_PREV_PAGE:
		{
			changePage(false);
			break;
		}
		case TAG_BUTTON_ALL_PAGE:
			if (currentpage == allpage[_questNum - 1]) {

				Scene* scene;
				switch (_questNum)
				{
				case 1:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_1_2::createScene(boolSEC));
					break;
				case 2:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_1_1::createScene(boolSEC));
					break;
				case 3:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_2_1::createScene(boolSEC));
					break;
				case 4:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_2_2_1::createScene(boolSEC));
					break;
				case 5:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_2_2_2::createScene(boolSEC));
					break;
				case 6:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_3_1_1::createScene(boolSEC));
					break;
				case 7:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_3_2::createScene(boolSEC));
					break;
				case 8:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_3_3::createScene(boolSEC));
					break;
				case 9:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_4_2::createScene(boolSEC));
					break;
				case 10:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_4_1::createScene(boolSEC));
					break;
				case 11:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_5_1::createScene(boolSEC));
					break;
				case 12:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_5_2_1::createScene(boolSEC));
					break;
				case 13:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_5_3::createScene(boolSEC));
					break;
				case 14:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_6_1_1::createScene(boolSEC));
					break;
				case 15:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_6_2::createScene(boolSEC));
					break;
				case 16:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_6_3::createScene(boolSEC));
					break;
				case 17:
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneGame_7_1::createScene(boolSEC));
					break;
				default:
					break;
				}
				Director::getInstance()->replaceScene(scene);
			}
			else {
				changePage(true);
			}
		{
			break;
		}
		default:
			break;
		}
	}
}
