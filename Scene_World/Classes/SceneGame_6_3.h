﻿#ifndef __SCENE_GAME_6_3_H__
#define __SCENE_GAME_6_3_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "CommonLib.h"

/*************** 태그 정보 ***************/
#define TAG_PEOPLE_1			701
#define TAG_PEOPLE_2			702
#define TAG_PEOPLE_3			703
#define TAG_PEOPLE_4			704
#define TAG_PEOPLE_5			705
#define TAG_PEOPLE_6			706
#define TAG_PEOPLE_7			707
#define TAG_PEOPLE_8			708
#define TAG_PEOPLE_9			709
#define TAG_PEOPLE_10			710
#define TAG_BUTTON_CHANGE		711
#define TAG_BUTTON_NOT_CHANGE	712
/****************************************/

class SceneGame_6_3 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	///Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	CheckBox* People[10];	//내림차순
	int MixedPeople[10];
	int RepetitionCount;
	Vec2 SelectedPeople;

	Button* ButtonChange;
	Button* ButtonPass;

	Button* Button_O;
	Button* Button_X;

public:

	SceneGame_6_3();
	~SceneGame_6_3();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_6_3);
	
	void mixPeople();
	void problemInit();	//Boubble Sort
	void selectPeople();

	int userAnswerCheck();
	void swapPeople();

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};

#endif 