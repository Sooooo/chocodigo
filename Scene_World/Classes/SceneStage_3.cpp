﻿//기본 헤더
#include "SceneStage_3.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneWorld.h"
#include "SceneExplainCreator.h"
#include "SceneCharacterInfo.h"

USING_NS_CC;
using namespace ui;


SceneStage_3::SceneStage_3(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	cP = new checkProgress;
	BGM = "Game_Resource/Sound/BGM_3.mp3";
}

SceneStage_3::~SceneStage_3(){
	delete logManager;
	delete cP;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneStage_3::createScene()
{
	auto scene = Scene::create();

	auto layer = SceneStage_3::create();
	scene->addChild(layer);

	return scene;
}

// on "init" you need to initialize your instance
bool SceneStage_3::init()
{
	if (!Layer::init())
	{
		return false;
	}
	
	//Import csb file
	auto CSB_SceneStage_3 = CSLoader::createNode("Scene_Stage_3.csb");
	addChild(CSB_SceneStage_3);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneStage_3->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneStage_3->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneStage_3->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneStage_3::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneStage_3::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneStage_3::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneStage_3::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneStage_3::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneStage_3::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	ButtonPlayerInfo = (Button*)CSB_SceneStage_3->getChildByTag(TAG_QUEST_INFO);
	ButtonPlayerInfo->addTouchEventListener(CC_CALLBACK_2(SceneStage_3::onTouch, this));

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneStage_3::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}


	//Map
	for (int i = 0; i < 3; i++) {
		Notice[i] = (Sprite*)CSB_SceneStage_3->getChildByTag(TAG_NOTICE + i);
		Map[i] = (Button*)CSB_SceneStage_3->getChildByTag(TAG_MAP + i);
		Map[i]->addTouchEventListener(CC_CALLBACK_2(SceneStage_3::onTouch, this));
	}

	cP->checkChapter3(logManager->getChapterProgress());

	if (logManager->getChapter() == 3) {
		for (int i = 0; i < 3; i++) {
			Notice[i]->setVisible(cP->getNoticeFlag(i));
		}
	}
	else {
		for (int i = 0; i < 3; i++) {
			Notice[i]->setVisible(false);
		}
	}


	return true;
}

void SceneStage_3::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		
		auto SelectButton = (Button*)sender;
		Scene* scene;
		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_OPTION:
		{
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			ButtonPlayerInfo->setTouchEnabled(false);
			for (int i = 0; i < 3; i++)
				Map[i]->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			auto scene = TransitionFade::create(0.5, SceneWorld::createScene());
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonPlayerInfo->setTouchEnabled(true);
			for (int i = 0; i < 3; i++)
				Map[i]->setTouchEnabled(false);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		case TAG_MAP:
		{
			 {
				if (logManager->getChapter() == 3) {
					if (logManager->getChapterProgress() == 20) {
						Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
						scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 3));
						logManager->printAll();
						Director::getInstance()->replaceScene(scene);
					}
					else if (logManager->getChapterProgress() == 30) {
						Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
						scene = TransitionFade::create(0.5, SceneExplainCreator::createScene(6));
						logManager->printAll();
						Director::getInstance()->replaceScene(scene);
					}
					else if (logManager->getChapterProgress() == 35) {
						Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
						scene = TransitionFade::create(0.5, SceneGame_3_1_2::createScene());
						logManager->printAll();
						Director::getInstance()->replaceScene(scene);
					}
					else{}
				}
				else{}

					break;
				}
		case TAG_MAP + 1:
		{
			
			if (logManager->getChapter() == 3) {
				if (logManager->getChapterProgress() == 70) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 11));
					logManager->setChapterProgress(90);
					logManager->printAll();
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 90) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 12));
					logManager->printAll();
					Director::getInstance()->replaceScene(scene);
				}
				else{}
			}
			else{}

			break;
		}
		case TAG_MAP + 2:
		{
			
			if (logManager->getChapter() == 3) {
				if (logManager->getChapterProgress() < 10) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 1));
					logManager->printAll();
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 10) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 2));
					logManager->printAll();
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 40) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 6));
					logManager->printAll();
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 45) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneExplainCreator::createScene(7));
					logManager->printAll();
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 50) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 8));
					logManager->printAll();
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 55) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneExplainCreator::createScene(8));
					logManager->printAll();
					Director::getInstance()->replaceScene(scene);
				}
				else if (logManager->getChapterProgress() == 60) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(3, 10));
					logManager->printAll();
					Director::getInstance()->replaceScene(scene);
				}
				else{}
			}
			else{}
			break;
		}
		case TAG_QUEST_INFO:
		{
			auto scene = TransitionFade::create(0.5, SceneCharacterInfo::createScene());
			Director::getInstance()->pushScene(scene);
		}
		default:
			break;
			}
		}
	}
}


void SceneStage_3::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneStage_3::ScheduleAudio(float dt)
{
	if (FlagAudio == true)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}