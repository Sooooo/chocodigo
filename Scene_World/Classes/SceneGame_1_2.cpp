﻿//기본 헤더
#include "SceneGame_1_2.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
//#include <stdlib.h>

//화면 전환
#include "SceneStage_1.h"

USING_NS_CC;
using namespace ui;

bool isReplay12;

SceneGame_1_2::SceneGame_1_2(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_1.mp3";
}

SceneGame_1_2::~SceneGame_1_2()
{
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_1_2::createScene(bool _bool)
{
	auto scene = Scene::create();

	isReplay12 = _bool;

	auto layer = SceneGame_1_2::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_1_2::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//Import csb file
	auto CSB_SceneGame_1_2 = CSLoader::createNode("Scene_Game_1_2.csb");
	addChild(CSB_SceneGame_1_2);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_1_2->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_1_2->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_1_2->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_1_2::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//Button
	Button_O = (Button*)CSB_SceneGame_1_2->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_1_2->getChildByTag(TAG_X);
	ButtonPlay = (Button*)CSB_SceneGame_1_2->getChildByTag(TAG_BUTTON_START);
	ButtonReset = (Button*)CSB_SceneGame_1_2->getChildByTag(TAG_BUTTON_RESET);


	//Add Touch Event
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));

	ButtonPlay->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));
	ButtonReset->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));

	int cnt = 0;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			sprite[i][j] = (Sprite*)CSB_SceneGame_1_2->getChildByTag(TAG_SPRITE_STUFF + cnt++);
		}
		button[i] = (Button*)CSB_SceneGame_1_2->getChildByTag(TAG_BUTTON_EXCHANGE + i);
		button[i]->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_2::onTouch, this));
		

	}

	flag[0] = 0;
	flag[1] = 1;
	flag[2] = 2;
	
	return true;
}

bool SceneGame_1_2::isCorrect() {
	

	if (flag[0] == 1 && flag[1] == 2 && flag[2] == 0)
		return true;
	else return false;
}

void SceneGame_1_2::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){

		auto SelectButton = (Button*)sender;

		int tmp;

		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_EXCHANGE:
			sprite[0][flag[0]]->setVisible(false);
			sprite[1][flag[1]]->setVisible(false);

			tmp = flag[0];
			flag[0] = flag[1];
			flag[1] = tmp;

			sprite[0][flag[0]]->setVisible(true);
			sprite[1][flag[1]]->setVisible(true);
			break;

		case TAG_BUTTON_EXCHANGE + 1:
			sprite[0][flag[0]]->setVisible(false);
			sprite[2][flag[2]]->setVisible(false);

			tmp = flag[0];
			flag[0] = flag[2];
			flag[2] = tmp;

			sprite[0][flag[0]]->setVisible(true);
			sprite[2][flag[2]]->setVisible(true);
			break;

		case TAG_BUTTON_EXCHANGE + 2:
			sprite[1][flag[1]]->setVisible(false);
			sprite[2][flag[2]]->setVisible(false);

			tmp = flag[1];
			flag[1] = flag[2];
			flag[2] = tmp;

			sprite[1][flag[1]]->setVisible(true);
			sprite[2][flag[2]]->setVisible(true);
			break;

			//System Button
		case TAG_BUTTON_START:

			if (isCorrect()) {
				Button_O->setVisible(true);
			}
			else {
				Button_X->setVisible(true);
			}

			break;

		case TAG_O:
		{
			

			Scene* scene;

			if (isReplay12) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
				Director::getInstance()->replaceScene(scene);
				Button_O->setVisible(false);
			}
			else {

				logManager->setQuest(2);
				logManager->setChapterProgress(50);
				Button_O->setVisible(false);
				scene = TransitionFade::create(0.5, SceneStage_1::createScene());
				Director::getInstance()->replaceScene(scene);
			}
			
		}
		break;

		case TAG_X:
		{
			
			Button_X->setVisible(false);
			init();
		}
		break;
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			ButtonPlay->setTouchEnabled(false);
			ButtonReset->setTouchEnabled(false);
			for (int i = 0; i < 3; i++)
				button[i]->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			

			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			Scene* scene;

			if (isReplay12) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
				Director::getInstance()->replaceScene(scene);
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_1::createScene());
				Director::getInstance()->replaceScene(scene);
			}
	
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			ButtonPlay->setTouchEnabled(true);
			ButtonReset->setTouchEnabled(true);
			for (int i = 0; i < 3; i++)
				button[i]->setTouchEnabled(true);
			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
		default:{ break; }
		}
	}
}


void SceneGame_1_2::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_1_2::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);
	}
}