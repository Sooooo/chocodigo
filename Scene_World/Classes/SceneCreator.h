﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_CREATOR_H__
#define __SCENE_CREATOR_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

#include "json.h"

#include "CommonTagSet.h"
#include "CommonLib.h"
#include "LogManagement.h"
#include "checkProgress.h"

#include "SceneStage_1.h"
#include "SceneStage_2.h"
#include "SceneStage_3.h"
#include "SceneStage_4.h"
#include "SceneStage_5.h"
#include "SceneStage_6.h"

#include "BattleSystem.h"

#include "SceneExplainCreator.h"

using namespace ui;

/******************** 태그 정보 ********************/
#define TAG_SCRIPT_BUTTON_NEXT			701
#define TAG_SPRITE_EVENT				702
#define TAG_SCRIPT_BUTTON_SKIP			703

class SceneCreator : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	bool FlagClickExit;

	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	//Resource
	const std::string background_resource = "Game_Resource/script_system/background/";
	const std::string character_resource = "Game_Resource/script_system/character/";
	const std::string bgmusic_resource = "Game_Resource/Sound/";

	//Dialogue
	Sprite* Sprite_Background;

	Sprite* Sprite_LPotrait;
	Sprite* Sprite_RPotrait;
	Sprite* Sprite_Event;

	TextField* TextField_Name;
	TextField* TextField_Dialogue;

	Button* Button_Next;
	Button* Button_Skip;

	//Quest Window
	Layer* LayerQuestWindow;
	Button* Button_Accept;
	Button* Button_Refuse;
	Label* Label_Chapter;
	Label* Label_Quest;
	Label* Label_QuestTitle;
	TextField* TextField_Inform;
	TextField* TextField_Goal;

	int cnt;
	int maxcnt;
	Json json;


public:

	SceneCreator();
	~SceneCreator();

	static Scene* createScene(int c, int s);
	virtual bool init();
	CREATE_FUNC(SceneCreator);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	void setResources();

	// 첫 실행 일 때
	void setResources(int i);

	void startBattle(int i);
	void openImage(string str);
	void setProgress(int _progress);
	void openQuestWindow(int _questNum);
	void openSE(string str);
	void setPot(string strL, string strR);
	void closeImage();
	void setPrologue();
	void startGame(int i);

	void moveScene();
};

#endif
