﻿#ifndef __SCENE_GAME_2_1_H__
#define __SCENE_GAME_2_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "checkProgress.h"

/*************** 태그 정보 ***************/
#define TAG_PROBLEM_1		701
#define TAG_PROBLEM_2		702
#define TAG_PROBLEM_3		703
#define TAG_PROBLEM_4		704
#define TAG_PROBLEM_5		705
#define TAG_PROBLEM_6		706
/****************************************/


class SceneGame_2_1 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;
	checkProgress* cP;
public:

	Button* Problems[6];

	int ProblemAchievement; //문제풀이 달성률
	//static일단 보류
	
	SceneGame_2_1();
	~SceneGame_2_1();
 
	static cocos2d::Scene* createScene(bool _booltmp = false);
    virtual bool init();
	CREATE_FUNC(SceneGame_2_1);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};


#endif 