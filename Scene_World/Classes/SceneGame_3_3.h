﻿#ifndef __SCENE_GAME_3_3_H__
#define __SCENE_GAME_3_3_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"


/*************** 태그 정보 ***************/
#define TAG_BUTTON_RECIPE		701
#define TAG_BUTTON_COOK			702
/****************************************/


class SceneGame_3_3 : public Layer
{

private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

public:

	Button* ButtonCook;
	Button* ButtonRecipe;

	SceneGame_3_3();
	~SceneGame_3_3();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_3_3);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};


#endif 