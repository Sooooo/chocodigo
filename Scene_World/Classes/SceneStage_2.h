﻿#ifndef __SCENE_STAGE_2_H__
#define __SCENE_STAGE_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "checkProgress.h"
#include "SceneCreator.h"

class SceneStage_2 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;
	checkProgress* cP;

	//Map
	Button* Map[4];
	Sprite* Notice[4];
	Button* ButtonPlayerInfo;
public:
	SceneStage_2();
	~SceneStage_2();

	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(SceneStage_2);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};

#endif
