﻿#ifndef __SCENE_GAME_3_1_1_H__
#define __SCENE_GAME_3_1_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "SceneCreator.h"

/*************** 태그 정보 ***************/
#define TAG_RABBIT				701
#define TAG_CAT					702
#define TAG_REJECT				703
#define TAG_PASS				704
#define TAG_BUTTON_REJECT		705
#define TAG_BUTTON_PASS			706
#define TAG_PROBLEM_WINDOW		710
/****************************************/


class SceneGame_3_1_1 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	int ProblemNum;	//문제 번호

	int Problem[7];	//문제
	bool ProblemAnswer;	//문제 답
	int UserAnswer;	//사용자 답
	TextField* ProblemState;
	TextField* ProblemWindow;

	Sprite* Rabbit;
	Sprite* Cat;
	Sprite* Reject;
	Sprite* Pass;

	Button* ButtonConfirm;
	Button* ButtonReject;
	Button* ButtonPass;

	Button* Button_O;
	Button* Button_X;

public:
	SceneGame_3_1_1();
	~SceneGame_3_1_1();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_3_1_1);

	void problemInit();	//문제 초기화
	void ProblemAnswerCheck();	//정답 계산
	void UserAnswerInit();	//정답 초기화
	void UserAnswerCheck();	//계산 결과 확인
	void play();	//실행하기

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
};


#endif 