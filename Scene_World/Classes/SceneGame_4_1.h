﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_GAME_4_1_H__
#define __SCENE_GAME_4_1_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

#include "CommonTagSet.h"
#include "CommonLib.h"

#include "LogManagement.h"
#include "SceneCreator.h"

USING_NS_CC;
using namespace ui;


/******************** 태그 정보 ********************/

//		TAG_BUTTON_CONFIRM					602

//		TAG_O								605
//		TAG_X								606

#define TAG_BUTTON_ANSWER					721
#define TAG_TEXTFIELD_ANSWER				711
#define TAG_TEXTFIELD_QUESTION				701

/**************************************************/

class SceneGame_4_1 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement *logManager;

	//Game
	Button* Button_O;
	Button* Button_X;
	Button* ButtonConfirm;

	Button* ButtonAnswer[4];
	TextField* TextQuestion[4];
	TextField* TextAnswer[4];

	int gameAnswer[4], selAnswer[4];

	int turn;

public:
	SceneGame_4_1();
	~SceneGame_4_1();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();

	CREATE_FUNC(SceneGame_4_1);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	void setRandomQuestion();
	void setRandomAnswer(int questionNumber);
	bool isCorrect();
	void answerButtonClicked(int buttonNum);	
};

#endif 