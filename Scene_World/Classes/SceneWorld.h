﻿#ifndef __SCENE_WORLD_H__
#define __SCENE_WORLD_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

#include "CommonTagSet.h"
#include "LogManagement.h"

USING_NS_CC;
using namespace ui;


/*************** 태그 정보 ***************/
#define TAG_STAGE			201
#define TAG_STAGE_ALERT		211
/****************************************/

class SceneWorld : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;

	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	Sprite* alertWorld[2];
	
	//Log Management
	LogManagement* logManager;

	//Scroll
	ScrollView* Scroll;
	Button* Stage[7];

	Button* ButtonPlayerInfo;

public:
		
	SceneWorld();
	~SceneWorld();

	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(SceneWorld);

	void onTouch(Ref* sender, Widget::TouchEventType type);		//터치 이벤트
	void appplicationExit();						//어플리케이션 종료
	void ScheduleAudio(float dt);
};

#endif
