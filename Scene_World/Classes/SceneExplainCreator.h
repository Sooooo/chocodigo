﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_EXPLAIN_CREATOR_H__
#define __SCENE_EXPLAIN_CREATOR_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui\CocosGUI.h"
#include "SceneWorld.h"

#include "SceneGame_1_1.h"
#include "SceneGame_1_2.h"

#include "SceneGame_2_1.h"
#include "SceneGame_2_2_1.h"
#include "SceneGame_2_2_2.h"

#include "SceneGame_3_1_1.h"
#include "SceneGame_3_1_2.h"
#include "SceneGame_3_2.h"
#include "SceneGame_3_3.h"

#include "SceneGame_4_1.h"
#include "SceneGame_4_2.h"

#include "SceneGame_5_1.h"
#include "SceneGame_5_2_1.h"
#include "SceneGame_5_2_2.h"
#include "SceneGame_5_3.h"

#include "SceneGame_6_1_1.h"
#include "SceneGame_6_2.h"
#include "SceneGame_6_3.h"

#include "SceneGame_7_1.h"

using namespace ui;

#define TAG_BUTTON_ALL_PAGE				701
#define TAG_BUTTON_PREV_PAGE			702
#define TAG_BUTTON_NEXT_PAGE			703

class SceneExplainCreator : public Layer
{
private:

	int currentpage = 1;

	int quest;

	int allpage[17];

	string str = "Game_Resource/QuestExplain/";

public:

	Button* Button_Next;
	Button* Button_Prev;
	Button* Button_All;

	SceneExplainCreator();
	~SceneExplainCreator();

	static Scene* createScene(int q, bool b = false);
	virtual bool init();
	CREATE_FUNC(SceneExplainCreator);

	void onTouch(Ref* sender, Widget::TouchEventType type);	
	void changePage(bool next);
};

#endif
