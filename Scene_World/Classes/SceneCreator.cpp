﻿#pragma execution_character_set("UTF-8")

//기본 헤더
#include "SceneCreator.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"

//화면 전환
#include "SceneWorld.h"
#include "PrologueCreator.h"

USING_NS_CC;
using namespace ui;

static int _ChapterNum;
static int _SceneNum;
int bufnum;

using std::string;
using rapidjson::Document;

SceneCreator::SceneCreator(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;
	FlagClickExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_";
}

SceneCreator::~SceneCreator(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneCreator::createScene(int c, int s)
{
	_ChapterNum = c;
	_SceneNum = s;

	auto scene = Scene::create();

	auto layer = SceneCreator::create();
	scene->addChild(layer);

	return scene;
}

// on "init" you need to initialize your instance
bool SceneCreator::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//Import csb file
	auto CSB_SceneCreator = CSLoader::createNode("Scene_Creator.csb");
	addChild(CSB_SceneCreator);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneCreator->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneCreator->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneCreator->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Schedule
	this->scheduleOnce(schedule_selector(SceneCreator::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//태그로 버튼 이미지 가져오기
	Sprite_Background = (Sprite*)CSB_SceneCreator->getChildByTag(TAG_BACKGROUND);
	Sprite_LPotrait = (Sprite*)CSB_SceneCreator->getChildByTag(TAG_LEFT_POTRAIT);
	Sprite_RPotrait = (Sprite*)CSB_SceneCreator->getChildByTag(TAG_RIGHT_POTRAIT);
	Sprite_Event = (Sprite*)CSB_SceneCreator->getChildByTag(TAG_SPRITE_EVENT);

	TextField_Dialogue = (TextField*)CSB_SceneCreator->getChildByTag(TAG_SCRIPT_DIALOGUE);
	TextField_Name = (TextField*)CSB_SceneCreator->getChildByTag(TAG_SCRIPT_NAME);

	Button_Next = (Button*)CSB_SceneCreator->getChildByTag(TAG_SCRIPT_BUTTON_NEXT);
	Button_Skip = (Button*)CSB_SceneCreator->getChildByTag(TAG_SCRIPT_BUTTON_SKIP);

	//버튼에 터치 이벤트 등록
	Button_Next->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));
	Button_Skip->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));
	
	LayerQuestWindow = (Layer*)CSB_SceneCreator->getChildByTag(TAG_LAYER_QUEST);
				
	LayerQuestWindow->setVisible(false);
	
	cnt = 1;
	
	json.openfile(_ChapterNum, _SceneNum, "script");

	setResources();

	return true;
}

void SceneCreator::setResources() {

	Sprite_Background->setTexture(json.getScriptData("BGIMG"));
	this->setPot(json.getScriptData("LPot"), json.getScriptData("RPot"));
	this->maxcnt = json.getArraySize() - 1;

	setResources(cnt);
}

void SceneCreator::setResources(int i) {

	string s = json.getScriptData(i, "event");
	bool flag = true;

	if (s.compare("null") == 0) { }
	else if (s.compare("startBattle") == 0) {
		this->startBattle(json.getIntParameter(i));
		flag = false;
	}
	else if (s.compare("openImage") == 0) {
		this->openImage(json.getScriptData(i, "parameter"));
	}
	else if (s.compare("setProgress") == 0){
		this->setProgress(json.getIntParameter(i));
	}
	else if (s.compare("openQuestWindow") == 0) {
		this->openQuestWindow(json.getIntParameter(i));
	}
//	else if (s.compare("openSE") == 0){
//		this->openSE(json.getScriptData(i, "parameter"));
//	}
	else if (s.compare("resetPot") == 0){
		this->setPot(json.getScriptData(i, "LPot"), json.getScriptData(i, "RPot"));
	}
	else if (s.compare("closeImage") == 0) {
		this->closeImage();
	}
	else if (s.compare("setPrologue") == 0) {
		this->setPrologue();
	}
	else if (s.compare("startGame") == 0){
		bufnum = json.getIntParameter(i);
		this->openQuestWindow(bufnum);
		//this->startGame(i);
		flag = false;
	}
	else if (s.compare("goToBoss") == 0){
		flag = false;
		auto scene = TransitionFade::create(0.5, PrologueCreator::createScene(7));
		Director::getInstance()->replaceScene(scene);

	}
		//Next가 true인 경우
	if (flag) {
		if (json.checkNext(i)) {
			TextField_Name->setString(json.getScriptData(i, "name"));
			TextField_Dialogue->setString(json.getScriptData(i, "dialogue"));
		}
		else {
			this->moveScene();
		}

		if (cnt < (this->maxcnt)) {
			cnt++;
		}

	}
	
	
	
}

void SceneCreator::startGame(int i) {
	auto scene = TransitionFade::create(0.5, SceneExplainCreator::createScene(i));
	Director::getInstance()->replaceScene(scene);
}
void SceneCreator::setPrologue() {
	logManager->setPrologue();
}

void SceneCreator::moveScene() {

	Scene* scene; 

	switch (_ChapterNum) {
	case 0:
		scene = TransitionFade::create(0.5, SceneWorld::createScene());
		Director::getInstance()->replaceScene(scene);
		break;
	case 1 : 
		scene = TransitionFade::create(0.5, SceneStage_1::createScene());
		Director::getInstance()->replaceScene(scene);
		break;
	case 2:
		scene = TransitionFade::create(0.5, SceneStage_2::createScene());
		Director::getInstance()->replaceScene(scene);
		break;
	case 3:
		scene = TransitionFade::create(0.5, SceneStage_3::createScene());
		Director::getInstance()->replaceScene(scene);
		break;
	case 4:
		scene = TransitionFade::create(0.5, SceneStage_4::createScene());
		Director::getInstance()->replaceScene(scene);
		break;
	case 5:
		scene = TransitionFade::create(0.5, SceneStage_5::createScene());
		Director::getInstance()->replaceScene(scene);
		break;
	case 6:
		scene = TransitionFade::create(0.5, SceneStage_6::createScene());
		Director::getInstance()->replaceScene(scene);
		break;
	default :
		break;
	}

}

void SceneCreator::setPot(string strL, string strR) {
	this->Sprite_LPotrait->setTexture(strL);
	this->Sprite_RPotrait->setTexture(strR);
}

void SceneCreator::startBattle(int i){

	auto scene = TransitionFade::create(0.5, BattleSystem::createScene(i));
	Director::getInstance()->replaceScene(scene);

};

void SceneCreator::openImage(string str) {

	this->Sprite_Event->setVisible(true);
	string _str = "Game_Resource/script_system/Image/";

	if (str == "themo.png") {
		_str.append("0_themo.png");
	}
	else {
		_str.append(commonLib::to_string(_ChapterNum));
		_str.append("_");
		_str.append(str);
	}
	
	this->Sprite_Event->setTexture(_str);
}; 

void SceneCreator::closeImage() {
	this->Sprite_Event->setVisible(false);
};

void SceneCreator::setProgress(int _progress){
	
	if (_progress == 100) {
		logManager->setChapterProgress(0);
	}
	else {
		logManager->setChapterProgress(_progress);
	}
	logManager->printAll();
};

void SceneCreator::openQuestWindow(int _questNum) {

	//Quest Window
	Button_Accept = (Button*)LayerQuestWindow->getChildByTag(TAG_BUTTON_ACCEPT);
	Button_Refuse = (Button*)LayerQuestWindow->getChildByTag(TAG_BUTTON_REFUSE);
	Button_Accept->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));
	Button_Refuse->addTouchEventListener(CC_CALLBACK_2(SceneCreator::onTouch, this));

	LayerQuestWindow->setVisible(true);
	Button_Next->setTouchEnabled(false);
	Json jjson;
	jjson.openfile(0, _questNum, "quest");

	Label* label[3];
	Label* textField[2];

	for (int i = 0; i < 3; i++){
		label[i] = Label::createWithTTF("", "Game_Resource/System/Font/08SeoulHangangL_0.ttf", 36);
		this->addChild(label[i]);
	}
	Vec2 ap;
	label[0]->setAnchorPoint(ap.ANCHOR_MIDDLE);
	label[0]->setPosition(669, 934);
	label[1]->setAnchorPoint(ap.ANCHOR_MIDDLE);
	label[1]->setPosition(881, 934);
	
	label[2]->setAnchorPoint(ap.ANCHOR_MIDDLE_LEFT);
	label[2]->setPosition(950, 934);
	
	textField[0] = Label::createWithTTF("", "Game_Resource/System/Font/08SeoulHangangL_0.ttf", 32, Size(860, 160));
	this->addChild(textField[0]);
	textField[1] = Label::createWithTTF("", "Game_Resource/System/Font/08SeoulHangangL_0.ttf", 32, Size(730, 190));
	this->addChild(textField[1]);
	

	Vec2 pos;
	pos.setPoint(537, 813);
	textField[0]->setAnchorPoint(ap.ANCHOR_TOP_LEFT);
	textField[0]->setPosition(pos);
	textField[1]->setAnchorPoint(ap.ANCHOR_TOP_LEFT);
	pos.setPoint(537, 552);
	textField[1]->setPosition(pos);

	label[0]->setString(commonLib::to_string(logManager->getChapter()));
	label[1]->setString(commonLib::to_string(_questNum));
	label[2]->setString(jjson.getQuestData(_questNum, "QTitle"));
	textField[0]->setString(jjson.getQuestData(_questNum, "inform"));
	textField[1]->setString(jjson.getQuestData(_questNum, "goal"));
		
};

void SceneCreator::openSE(string str) {
	//this->addChild();
};

void SceneCreator::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)
	{
		auto SelectButton = (Button*)sender;
		
		switch (SelectButton->getTag())
		{
			case TAG_SCRIPT_BUTTON_SKIP: {
				setResources(maxcnt);
				break;
			}
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_EXIT:
			{
				

				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);
				
				Button_Next->setTouchEnabled(false);

				break;
			}
			case TAG_BUTTON_BACK:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);

				moveScene();
				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);

				Button_Next->setTouchEnabled(true);

				break;
			}
			case TAG_EXIT:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();
				break;
			}
			case TAG_SCRIPT_BUTTON_NEXT:
			{
				setResources(cnt);
				break;
			}
			case TAG_BUTTON_ACCEPT:
			{
				this->setProgress(json.getProgressPara(maxcnt));
				startGame(bufnum);
				break;
			}
			case TAG_BUTTON_REFUSE:
			{
				this->moveScene();
				break;
			}
			default:
				break;
		}
	}
}

void SceneCreator::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneCreator::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{		
		BGM.append(commonLib::to_string(_ChapterNum));
		BGM.append(".mp3");
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}