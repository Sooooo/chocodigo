﻿//기본 헤더
#include "SceneGame_2_1_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"
//#include <string>
#include <ctime>
#include <cstdlib>
#include <math.h>


//화면전환
#include "SceneGame_2_1.h"

USING_NS_CC;
using namespace ui;

int tmp;

bool isReplay211;

SceneGame_2_1_1::SceneGame_2_1_1(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_2.mp3";

	ProblemNum = 0;
}


SceneGame_2_1_1::~SceneGame_2_1_1(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}


Scene* SceneGame_2_1_1::createScene(int i, bool _booltmp){

	isReplay211 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_2_1_1::create();
	scene->addChild(layer);

	tmp = i;
	return scene;
}

// on "init" you need to initialize your instance
bool SceneGame_2_1_1::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_2_1_1 = CSLoader::createNode("Scene_Game_2_1_1.csb");
	addChild(CSB_SceneGame_2_1_1);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_2_1_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_2_1_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_2_1_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_2_1_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//태그로 버튼 가져오기
	Button_O = (Button*)CSB_SceneGame_2_1_1->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_2_1_1->getChildByTag(TAG_X);
	ProblemTextField = (TextField*)CSB_SceneGame_2_1_1->getChildByTag(TAG_PROBLEM);
	ButtonStart = (Button*)CSB_SceneGame_2_1_1->getChildByTag(TAG_BUTTON_START);
	ButtonReset = (Button*)CSB_SceneGame_2_1_1->getChildByTag(TAG_BUTTON_RESET);

	for (int Index = 0; Index < 4; Index++){
		int Tag = Index + TAG_BINARY_1;
		Binaries[Index] = (Binary*)CSB_SceneGame_2_1_1->getChildByTag(Tag);
	}

	//버튼 터치이벤트
	ButtonStart->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));
	ButtonReset->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_1_1::onTouch, this));

	problemInit();

	return true;
}

void SceneGame_2_1_1::setProblemNum(int Index, bool Value)
{
	ProblemAnswer[Index] = Value;
}

void SceneGame_2_1_1::problemInit()
{

	srand((unsigned int)time(NULL));

	ProblemDecimal = rand() % 15 + 1;

	ProblemTextField->setString(commonLib::to_string(ProblemDecimal));

	//O, X 안보이게
	Button_O->setVisible(false);
	Button_X->setVisible(false);

	//답칸 0 초기화
	UserAnswerInit();

}

void SceneGame_2_1_1::UserAnswerInit()
{
	Button_X->setVisible(false);

	for (int Index = 0; Index < 4; Index++)
	{
		Binaries[Index]->setSelected(false);
	}
}

bool SceneGame_2_1_1::UserAnswerCheck()
{
	int result=0;

	for (int Index = 0; Index < 4; Index++)
	{
		UserAnswer[Index] = Binaries[Index]->isSelected();

		if (UserAnswer[Index] == 1)
		{
			result += pow(2, Index);
		}
		
	}
	
	if (ProblemDecimal == result){
		return true;
	}
	else {
		return false;
	}

}

void SceneGame_2_1_1::play(bool Ck)
{
	if (Ck == true)	//정답인 경우
	{
		Button_O->setVisible(true);
		ProblemNum++;
	}
	else if (Ck == false)	//오답인 경우
	{
		Button_X->setVisible(true);

	}

}



void SceneGame_2_1_1::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){
		

		auto SelectButton = (Button*)sender;
		auto action = ToggleVisibility::create();

		switch (SelectButton->getTag())
		{
			case TAG_BUTTON_START:{
				
				play(UserAnswerCheck());
				break;
			}
			case TAG_BUTTON_RESET:{
				
				UserAnswerInit();
				break; 
			}
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();

				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_BACK:{
				
				Scene* scene;

				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				if (isReplay211) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				else {					
					scene = TransitionFade::create(0.5, SceneGame_2_1::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				
				break;
			}
			case TAG_BUTTON_EXIT:
			{
				

				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);

				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);

				break;
			}
			case TAG_EXIT:
			{
				

				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();

				break;
			}
			case TAG_O:{

				Button_O->setVisible(false);
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				if (isReplay211) {
					auto scene = TransitionFade::create(0.5, QuestReplay::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				else {
					logManager->setQ2_2_Flag(tmp);
					logManager->setChapterProgress(logManager->getChapterProgress() + 1);
					logManager->printAll();
					if (logManager->getChapterProgress() == 26) {
						auto scene = TransitionFade::create(0.5, SceneCreator::createScene(2, 3));
						Director::getInstance()->replaceScene(scene);
					}
					else {
						auto scene = TransitionFade::create(0.5, SceneGame_2_1::createScene());
						Director::getInstance()->replaceScene(scene);
					}
				}
				break;
			}
			case TAG_X:{
				UserAnswerInit();
				break;
			}
			default:
				break;
		}
	}
}


void SceneGame_2_1_1::appplicationExit(){
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_2_1_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}