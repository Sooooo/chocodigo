﻿//기본 헤더
#include "SceneGame_2_2_2.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"
//#include <ctime>

//화면 전환
#include "SceneStage_2.h"

USING_NS_CC;
using namespace ui;

bool isReplay222;

SceneGame_2_2_2::SceneGame_2_2_2(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_2.mp3";
}

SceneGame_2_2_2::~SceneGame_2_2_2(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_2_2_2::createScene(bool _booltmp)
{
	isReplay222 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_2_2_2::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_2_2_2::init()
{
	if (!Layer::init())
	{
		return false;
	}


	//불러오기
	auto CSB_SceneGame_2_2_2 = CSLoader::createNode("Scene_Game_2_2_2.csb");
	addChild(CSB_SceneGame_2_2_2);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_2_2_2->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_2_2_2->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_2_2_2->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_2_2_2::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//숫자 텍스트 필드 및 버튼 가져오기
	for (int Index = 0; Index < 14; Index++){
		int Tag = Index + TAG_NUM_1;
		TextNum[Index] = (TextField*)CSB_SceneGame_2_2_2->getChildByTag(Tag);
	}

	for (int Index = 0; Index < 10; Index++){
		int Tag = Index + TAG_NUM_BUTTON_1;
		ButtonNum[Index] = (Button*)CSB_SceneGame_2_2_2->getChildByTag(Tag);
	}

	//태그로 버튼 및 텍스트 필드 가져오기
	ButtonConfirm = (Button*)CSB_SceneGame_2_2_2->getChildByTag(TAG_BUTTON_CONFIRM);
	Button_O = (Button*)CSB_SceneGame_2_2_2->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_2_2_2->getChildByTag(TAG_X);
	
	//터치 이벤트 등록
	ButtonConfirm->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));
	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));

	for (int Index = 0; Index < 10; Index++){
		ButtonNum[Index]->addTouchEventListener(CC_CALLBACK_2(SceneGame_2_2_2::onTouch, this));
	}

	problemInit();

	return true;
}

void SceneGame_2_2_2::setProblemNum(int Index, bool Value)
{
	ProblemAnswer[Index] = Value;
}

void SceneGame_2_2_2::problemInit()
{
	//play 보이게
	ButtonConfirm->setVisible(true);

	//O, X 안보이게
	Button_O->setVisible(false);
	Button_X->setVisible(false);

	//문제 초기화
	srand((unsigned int)time(NULL));

	for (int Index = 0; Index < 4; Index++)
	{
		bool NumFlag = rand() % 2;

		TextNum[Index]->setString(commonLib::to_string(NumFlag));
		Problem[Index] = NumFlag;
	}
	
	//답칸 초기화
	UserAnswerInit();

	//정답 계산
	problemCalculate();
}

void SceneGame_2_2_2::problemCalculate()
{
	ProblemAnswer[0] = Problem[0] && Problem[1];
	ProblemAnswer[1] = Problem[1] || Problem[2];
	ProblemAnswer[2] = Problem[2] && Problem[3];
	ProblemAnswer[3] = Problem[0] || ProblemAnswer[0];
	ProblemAnswer[4] = ProblemAnswer[1] && ProblemAnswer[2];
	ProblemAnswer[5] = ProblemAnswer[1] || Problem[3];
	ProblemAnswer[6] = ProblemAnswer[0] && ProblemAnswer[4];
	ProblemAnswer[7] = ProblemAnswer[3] || ProblemAnswer[5];
	ProblemAnswer[8] = ProblemAnswer[3] && ProblemAnswer[5];
	ProblemAnswer[9] = ProblemAnswer[4] || ProblemAnswer[2];
}

void SceneGame_2_2_2::touchButtonNum(int ButtonTag)
{
	int TextTag = ButtonTag - TAG_NUM_5 - 6;

	std::string s = TextNum[TextTag]->getString();
	
	if (s == "?")
	{
		TextNum[TextTag]->setString("0");
	}
	else if(s == "0")
	{
		TextNum[TextTag]->setString("1");
	}
	else if (s == "1")
	{
		TextNum[TextTag]->setString("0");
	}	
}

void SceneGame_2_2_2::UserAnswerInit()
{
	Button_X->setVisible(false);

	for (int Index = 0; Index < 10; Index++)
	{
		TextNum[Index + 4]->setString("?");
	}
}

bool SceneGame_2_2_2::UserAnswerCheck()
{
	for (int Index = 0; Index < 10; Index++)
	{
		std::string answer = TextNum[Index + 4]->getString();

		if (answer == "0")
		{
			UserAnswer[Index] = 0;
		}
		else if (answer == "1")
		{
			UserAnswer[Index] = 1;
		}
		else if (answer == "?")
		{
			return false;
		}
	}

	for (int Index = 0; Index < 10; Index++)
	{
		if (UserAnswer[Index] != ProblemAnswer[Index])
		{
			return false;
		}
	}

	return true;
}

void SceneGame_2_2_2::play()
{
	bool result = UserAnswerCheck();
	
	ButtonConfirm->setVisible(false);
	
	if (result == true)	//정답인 경우
	{
		Button_O->setVisible(true);
	}
	else if (result == false)	//오답인 경우
	{
		Button_X->setVisible(true);
	}
}

void SceneGame_2_2_2::onTouch(Ref* sender, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)	//터치를 뗄 때!
	{
		
		auto SelectButton = (Button*)sender;

		int Tag = SelectButton->getTag();
		switch (Tag)
		{
		case TAG_BUTTON_OPTION:
		{
			
			if (FlagOption == ON)
			{
				LayerOption->setVisible(false);
				FlagOption = OFF;
			}
			else if (FlagOption == OFF)
			{
				LayerOption->setVisible(true);
				FlagOption = ON;
			}

			break;
		}
		case TAG_AUDIO:
		{
			if (Audio->isSelected() == false)
			{
				logManager->setSoundOption(OFF);
				//mute
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (Audio->isSelected() == true)
			{
				logManager->setSoundOption(ON);
				//not mute
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
			}

			break;
		}
		case TAG_BUTTON_EXIT:
		{
			

			LayerExit->setVisible(true);

			ButtonOption->setTouchEnabled(false);
			Back->setTouchEnabled(false);
			Audio->setTouchEnabled(false);
			Exit->setTouchEnabled(false);

			for (int i = 0; i < 10; i++)
				ButtonNum[i]->setTouchEnabled(false);

			ButtonConfirm->setTouchEnabled(false);
			Button_O->setTouchEnabled(false);
			Button_X->setTouchEnabled(false);

			break;
		}
		case TAG_BUTTON_BACK:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();

			Scene* scene;

			if (isReplay222) {
				scene = TransitionFade::create(0.5, QuestReplay::createScene());
			}
			else {
				scene = TransitionFade::create(0.5, SceneStage_1::createScene());
			}
			Director::getInstance()->replaceScene(scene);
			break;
		}
		case TAG_CANCLE:
		{
			

			LayerExit->setVisible(false);

			ButtonOption->setTouchEnabled(true);
			Back->setTouchEnabled(true);
			Audio->setTouchEnabled(true);
			Exit->setTouchEnabled(true);

			for (int i = 0; i < 10; i++)
				ButtonNum[i]->setTouchEnabled(true);

			ButtonConfirm->setTouchEnabled(true);
			Button_O->setTouchEnabled(true);
			Button_X->setTouchEnabled(true);

			break;
		}
		case TAG_EXIT:
		{
			
			Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
			CocosDenshion::SimpleAudioEngine::getInstance()->end();

			appplicationExit();

			break;
		}
			case TAG_BUTTON_CONFIRM:
			{
				play();
				break;
			}
			case TAG_O:
			{
				


				Scene* scene;

				if (isReplay222) {
					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				else {

					logManager->setChapterProgress(70);
					logManager->setQuest(6);

					Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
					scene = TransitionFade::create(0.5, SceneCreator::createScene(2, 7));
					Director::getInstance()->replaceScene(scene);
				}
				break;
			}
			case TAG_X:
			{
				

				UserAnswerInit();
				ButtonConfirm->setVisible(true);
				break;
			}
			case TAG_NUM_BUTTON_1:
			case TAG_NUM_BUTTON_2:
			case TAG_NUM_BUTTON_3:
			case TAG_NUM_BUTTON_4:
			case TAG_NUM_BUTTON_5:
			case TAG_NUM_BUTTON_6:
			case TAG_NUM_BUTTON_7:
			case TAG_NUM_BUTTON_8:
			case TAG_NUM_BUTTON_9:
			case TAG_NUM_BUTTON_10:
			{
				touchButtonNum(Tag);
				break;
			}
			default:
				break;
		}

	}
}

void SceneGame_2_2_2::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_2_2_2::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);
	}
}