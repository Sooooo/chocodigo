﻿#ifndef __SCENE_GAME_2_1_2_H__
#define __SCENE_GAME_2_1_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;
using namespace std;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "SceneCreator.h"

/*************** 태그 정보 ***************/
#define TAG_PROB		 		701
#define TAG_DECIMAL_1			702
#define TAG_DECIMAL_10			703
#define TAG_BUTTON_UP_1			704
#define TAG_BUTTON_DOWN_1		705
#define TAG_BUTTON_UP_10		706
#define TAG_BUTTON_DOWN_10		707
/****************************************/


class SceneGame_2_1_2 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	//Game
	int ProblemDecimal;
	int NumTen;
	int NumOne;

	string ProblemToBinary;// 출력해줄 문제: 2진수값
	int ProblemNum;

	TextField* Problem;
	TextField* Decimal_1;
	TextField* Decimal_10;

	Button* ButtonStart;
	Button* ButtonReset;
	Button* Button_O;
	Button* Button_X;

	Button* ButtonUp_1;
	Button* ButtonUp_10;
	Button* ButtonDown_1;
	Button* ButtonDown_10;

public:


	SceneGame_2_1_2();
	~SceneGame_2_1_2();

	static cocos2d::Scene* createScene(int i, bool _booltmp = false);
	virtual bool init();
	CREATE_FUNC(SceneGame_2_1_2);

	void problemInit();	//문제 초기화
	void UserAnswerInit();	//정답 초기화
	bool UserAnswerCheck();	//계산 결과 확인
	void play(bool Ck);	//실행하기
	string decimalToBinary(int Decimal, string s);

	void NumUp_1();
	void NumUp_10();
	void NumDown_1();
	void NumDown_10();

	//터치 이벤트
	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);
	void CheckOX();
};

#endif 