﻿// 기본 헤더
#include "SceneGame_5_2_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "simpleAudioEngine.h"
#include "CommonLib.h"

//화면 전환
#include "SceneStage_5.h"
#include "SceneGame_5_2_2.h"


USING_NS_CC;
using namespace ui;
using namespace std; 

bool isReplay521;

SceneGame_5_2_1::SceneGame_5_2_1(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;
	hintVisible = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_5.mp3";

	char num[6] = { '1', '2', '3', '4', '5', '6' };

	for (int i = 0; i < 6; i++){
		Num[i] = num[i];
	}

	Commend[0] = "UP";
	Commend[1] = "DOWN";
	Commend[2] = "LEFT";
	Commend[3] = "RIGHT";
	Commend[4] = "PICK";
	Commend[5] = "↻";
	InputText = "";
	MazeText = "";
	loopCheck = 0;
	beforeState = "";
	LoopIn = "";
	FinalLoopIn = "";
	DeleteLoopIn = "";
}

SceneGame_5_2_1::~SceneGame_5_2_1(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_5_2_1::createScene(bool _booltmp)
{
	isReplay521 = _booltmp;

	auto scene = Scene::create();

	auto layer = SceneGame_5_2_1::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_5_2_1::init()
{

	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_5_2_1 = CSLoader::createNode("Scene_Game_5_2_1.csb");
	addChild(CSB_SceneGame_5_2_1);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_5_2_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_5_2_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_5_2_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//스크롤 가져오기
	Scroll = (ScrollView*)CSB_SceneGame_5_2_1->getChildByTag(TAG_SCROLL);
	
	//버튼 가져오기
	ButtonReset = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_RESET);
	//ButtonAdd = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_ADD);
	ButtonMinus = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_MINUS);
	ButtonConfirm = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_CONFIRM);
	ButtonMap = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_MAP);
	SpriteHint = (Sprite*)CSB_SceneGame_5_2_1->getChildByTag(TAG_SPRITE_HINT);

	B_UP = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_UP);
	B_DOWN = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_DOWN);
	B_LEFT = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_LEFT);
	B_RIGHT = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_RIGHT);
	B_PICK = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_PICK);
	B_LOOP = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_LOOP);
	
	B_1 = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_1);
	B_2 = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_2);
	B_3 = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_3);
	B_4 = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_4);
	B_5 = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_5);
	B_6 = (Button*)CSB_SceneGame_5_2_1->getChildByTag(TAG_BUTTON_6);

	//텍스트필드 가져오기
	TextInput = (TextField*)Scroll->getChildByTag(TAG_TEXTFIELD_INPUT);

	//터치이벤트 추가
	ButtonReset->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	//ButtonAdd->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	ButtonMinus->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	ButtonConfirm->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	ButtonMap->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_UP->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_DOWN->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_LEFT->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_RIGHT->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_PICK->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_LOOP->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_1->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_2->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_3->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_4->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_5->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));
	B_6->addTouchEventListener(CC_CALLBACK_2(SceneGame_5_2_1::onTouch, this));

	problemInit();

	return true;

}
void SceneGame_5_2_1::problemInit(){
	stateInit();
	TextInput->setString("");
	InputText = "";
	MazeText = "";
	B_LOOP->setTouchEnabled(false);
	beforeState = "";
	LoopIn = "";
	FinalLoopIn = "";
	loopNum = 0;
	loopCheck = 0;

	/*
	B_UP->setTouchEnabled(false);
	B_DOWN->setTouchEnabled(false);
	B_LEFT->setTouchEnabled(false);
	B_RIGHT->setTouchEnabled(false);
	B_PICK->setTouchEnabled(false);
	B_LOOP->setTouchEnabled(false);
	B_UP->setBright(false);
	B_DOWN->setBright(false);
	B_LEFT->setBright(false);
	B_RIGHT->setBright(false);
	B_PICK->setBright(false);
	B_LOOP->setBright(false);
	*/
	setInputButton();
	
}
void SceneGame_5_2_1::inputDelete(){
	int index = InputText.find_last_of(">");
	int index2 = MazeText.length()-2;
	int loopindex = InputText.find_last_of("{");
	int loopindexEnd = InputText.find_last_of("}");
	if (index == -1){
		InputText.erase(0);
		MazeText.erase(0);
	}
	else{
		//루프가 닫히기전 삭제
		if (loopCheck == 1){
			if (loopindex == 0){
				InputText.erase(0);
				MazeText.erase(0);
			}
			else{
				InputText.erase(loopindex - 3);
			}
			loopCheck = 0;
			LoopIn = "";
			FinalLoopIn = "";
			DeleteLoopIn = "";
		}
		else if (loopindexEnd == (InputText.length() - 1)){
			int MazeIndexLoop = MazeText.length() - DeleteLoopIn.length();
			if (loopindex == 0){
				InputText.erase(0);
				MazeText.erase(0);
			}
			else{
				InputText.erase(loopindex - 3);
				MazeText.erase(MazeIndexLoop - 1);
			}
			LoopIn = "";
			FinalLoopIn = "";
			DeleteLoopIn = "";
		
		}
		else {
			InputText.erase(index-1);
			MazeText.erase(index2);
		}
	}
	TextInput->setString(InputText);
}
void SceneGame_5_2_1::setInputButton(){
	B_UP->setTouchEnabled(true);
	B_DOWN->setTouchEnabled(true);
	B_LEFT->setTouchEnabled(true);
	B_RIGHT->setTouchEnabled(true);
	B_PICK->setTouchEnabled(true);
	B_LOOP->setTouchEnabled(true);
	B_UP->setBright(true);
	B_DOWN->setBright(true);
	B_LEFT->setBright(true);
	B_RIGHT->setBright(true);
	B_PICK->setBright(true);
	B_LOOP->setBright(true);
}
void SceneGame_5_2_1::setNumVisible(){
	B_1->setVisible(true);
	B_2->setVisible(true);
	B_3->setVisible(true);
	B_4->setVisible(true);
	B_5->setVisible(true);
	B_6->setVisible(true);
	B_UP->setTouchEnabled(false);
	B_DOWN->setTouchEnabled(false);
	B_LEFT->setTouchEnabled(false);
	B_RIGHT->setTouchEnabled(false);
	B_PICK->setTouchEnabled(false);
	B_UP->setBright(false);
	B_DOWN->setBright(false);
	B_LEFT->setBright(false);
	B_RIGHT->setBright(false);
	B_PICK->setBright(false);
	B_LOOP->setBright(false);
}
void SceneGame_5_2_1::stateInit(){
	B_1->setVisible(false);
	B_2->setVisible(false);
	B_3->setVisible(false);
	B_4->setVisible(false);
	B_5->setVisible(false);
	B_6->setVisible(false);
	B_UP->setTouchEnabled(true);
	B_DOWN->setTouchEnabled(true);
	B_LEFT->setTouchEnabled(true);
	B_RIGHT->setTouchEnabled(true);
	B_PICK->setTouchEnabled(true);
	B_LOOP->setTouchEnabled(true);
	B_UP->setBright(true);
	B_DOWN->setBright(true);
	B_LEFT->setBright(true);
	B_RIGHT->setBright(true);
	B_PICK->setBright(true);
	B_LOOP->setBright(true);
	/*if (loopCheck == 1){
		B_LOOP->setColor(ccc3(30, 144, 255));
	}
	else*/ 
	//B_LOOP->setTouchEnabled(false);

}

void SceneGame_5_2_1::loopTrans(){
	if (loopCheck == 0){
		InputText += '{';
		loopCheck = 1;
	} else if (loopCheck == 1){
		InputText += '}';
		loopCheck = 0;
		stateInit();
		TextInput->setString(InputText);
		//반복문실행
		loopString(LoopIn);
		MazeText += FinalLoopIn;
		LoopIn = "";
		DeleteLoopIn = FinalLoopIn;
		FinalLoopIn = "";

	}
}

void SceneGame_5_2_1::loopString(string loop){
		for (int i = 0; i < loopNum; i++){
			FinalLoopIn += LoopIn;
		}
}

void SceneGame_5_2_1::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED)
	{
		

		auto SelectButton = (Button*)sender;
		int Tag = SelectButton->getTag();

		switch (Tag)
		{
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_EXIT:
			{
				

				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);

				Button_O->setTouchEnabled(false);
				Button_X->setTouchEnabled(false);
				ButtonConfirm->setTouchEnabled(false);
				ButtonReset->setTouchEnabled(false);
				ButtonMap->setTouchEnabled(false);
				ButtonHint->setTouchEnabled(false);
				//ButtonAdd->setTouchEnabled(false);
				B_UP->setTouchEnabled(false);
				B_DOWN->setTouchEnabled(false);
				B_LEFT->setTouchEnabled(false);
				B_RIGHT->setTouchEnabled(false);
				B_PICK->setTouchEnabled(false);
				B_LOOP->setTouchEnabled(false);
				B_1->setTouchEnabled(false);
				B_2->setTouchEnabled(false);
				B_3->setTouchEnabled(false);
				B_4->setTouchEnabled(false);
				B_5->setTouchEnabled(false);
				B_6->setTouchEnabled(false);

				break;
			}
			case TAG_BUTTON_BACK:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				Scene* scene;

				if (isReplay521) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
				}
				else {
					scene = TransitionFade::create(0.5, SceneStage_5::createScene());
				}
				Director::getInstance()->replaceScene(scene);

				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);

				Button_O->setTouchEnabled(true);
				Button_X->setTouchEnabled(true);
				ButtonConfirm->setTouchEnabled(true);
				ButtonReset->setTouchEnabled(true);
				ButtonMap->setTouchEnabled(true);
				ButtonHint->setTouchEnabled(true);
				//ButtonAdd->setTouchEnabled(true);
				B_UP->setTouchEnabled(true);
				B_DOWN->setTouchEnabled(true);
				B_LEFT->setTouchEnabled(true);
				B_RIGHT->setTouchEnabled(true);
				B_PICK->setTouchEnabled(true);
				B_LOOP->setTouchEnabled(true);
				B_1->setTouchEnabled(true);
				B_2->setTouchEnabled(true);
				B_3->setTouchEnabled(true);
				B_4->setTouchEnabled(true);
				B_5->setTouchEnabled(true);
				B_6->setTouchEnabled(true);

				break;
			}
			case TAG_EXIT:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();

				break;
			}
			case TAG_BUTTON_MAP:
			{
				if (hintVisible == false)
				{
					SpriteHint->setVisible(true);
					hintVisible = true;
				}
				else if (hintVisible == true)
				{
					SpriteHint->setVisible(false);
					hintVisible = false;
				}
				break;
			}
			case TAG_BUTTON_RESET:{
				
				problemInit();
				break;
			}
			case TAG_BUTTON_MINUS:{
				
				inputDelete();
				break; 
			}
			case TAG_BUTTON_CONFIRM:{
				auto scene = TransitionFade::create(0.5, SceneGame_5_2_2::createScene(MazeText, isReplay521));
				Director::getInstance()->pushScene(scene);
				break;
			}
			default:
				break;
		}


		if (Tag >= 711 && Tag <= 716){

			//첫 입력 or Not
			if (InputText != "" && Tag != 716){ 
				//루프가 체크되어 있다면
				if (loopCheck == 1 && beforeState == Commend[5]){
					InputText += " : ";
					}
				else{
					InputText += " > ";}
			}

			//loop버튼 클릭 시
			switch (Tag)
			{
				case TAG_BUTTON_LOOP:{
					if (loopCheck == 0){
						if (InputText != ""){
							InputText += " > ";
						}
						setNumVisible();
						loopTrans();
						InputText.append(Commend[Tag - 711]);
						InputText += '/';
						break;
					}
					else{
						loopTrans();
						break;
					}

					break;
					}
				case TAG_BUTTON_PICK:{
					InputText.append(Commend[Tag - 711]);
					InputText += '/';
					InputText += Num[0];

					//루프안 이라면
					if (loopCheck == 1){
						LoopIn += commonLib::to_string(Pick);
						LoopIn += Num[0];
					}
					else{
						MazeText += commonLib::to_string(Pick);
						MazeText += Num[0];

						}
						stateInit();
						TextInput->setString(InputText);
						
					
					break;
				}
				default:{
					InputText.append(Commend[Tag - 711]);
					setNumVisible();
					InputText += '/';
					break; 
				}
			}
			
			int index = Tag - 710;

			switch (index)
			{
				case Up:{
					if (loopCheck == 1){
						LoopIn += commonLib::to_string(Up);
					}
					else if (loopCheck == 0) {
						MazeText += commonLib::to_string(Up);
					}
					break;
				}
				case Down:{
					if (loopCheck == 1){
						LoopIn += commonLib::to_string(Down);
					}
					else if(loopCheck == 0) {
						MazeText += commonLib::to_string(Down);
					}
					break;
				}
				case Left:{
					if (loopCheck == 1){
						LoopIn += commonLib::to_string(Left);
					}
					else if (loopCheck == 0) {
						MazeText += commonLib::to_string(Left);
					}
					break;
				}
				case Right:{
					if (loopCheck == 1){
						LoopIn += commonLib::to_string(Right);
					}
					else if (loopCheck == 0) {
						MazeText += commonLib::to_string(Right);
					}
					break;
				}

				default:
					break;
			}

			beforeState = Commend[Tag - 711];
		}

		//넘버패드 입력
		if (Tag >= 717 && Tag <= 722){
			stateInit();
			if (loopCheck == 1 && beforeState == Commend[5]){	//loop일때
				//루프횟수 저장
				loopNum = Tag - 716;
				TextInput->setString(InputText);
				InputText += Num[Tag - 717];
				TextInput->setString(InputText);
			}
			else if (loopCheck == 1){
				LoopIn += Num[Tag - 717];
				InputText += Num[Tag - 717];
				TextInput->setString(InputText);
			}
			else{
				InputText += Num[Tag - 717];
				MazeText += Num[Tag - 717];
				TextInput->setString(InputText);
			}
		}

	}
}

void SceneGame_5_2_1::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_5_2_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(BGM.c_str());
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);

		FlagAudio = false;
	}
}