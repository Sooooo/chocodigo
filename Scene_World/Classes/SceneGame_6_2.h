﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_GAME_6_2_H__
#define __SCENE_GAME_6_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"

/******************** 태그 정보 ********************/

//		TAG_BUTTON_START					601

//		TAG_O								605
//		TAG_X								606

#define	TAG_SPRITE_MOUSE					701
#define TAG_SPRITE_RIBBON					702
#define TAG_SPRITE_CHEESE					703
#define TAG_SPRITE_DOT						704
#define TAG_TEXTFIELD_GRAM					705

#define	TAG_BUTTON_HOUSE					710

/**************************************************/

class SceneGame_6_2 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	///Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement* logManager;

	Button* Button_O;
	Button* Button_X;
	Button* ButtonConfirm;

	Sprite* Sprite_Mouse;
	Sprite* Sprite_Ribbon;
	Sprite* Sprite_Cheese;
	Sprite* Sprite_Dot;

	TextField* TextField_Gram;

	Button* Button_House[6];
	bool flag[5];

	int gameAnswer;
	int selAnswer;

public:		
	SceneGame_6_2();
	~SceneGame_6_2();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();

	CREATE_FUNC(SceneGame_6_2);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	void setRandomQuestion();	

	bool isCorrect();

};

#endif 