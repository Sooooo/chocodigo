﻿#include "CommonLib.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

//to_string 사용하기 위함
#if( CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID )
std::string commonLib::to_string(int t) {
	std::ostringstream os;
	os << t;
	return os.str();
}
#endif

void commonLib::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}