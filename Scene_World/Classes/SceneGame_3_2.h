﻿#ifndef __SCENE_GAME_3_2_H__
#define __SCENE_GAME_3_2_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"
#include "PrologueCreator.h"

/******************** 태그 정보 ********************/

//		TAG_BUTTON_BACK						3
//		TAG_BUTTON_CONFIRM					602

//		TAG_O								605
//		TAG_X								606

#define TAG_SPRITE							701
#define TAG_BUTTON							711

/**************************************************/

class SceneGame_3_2 : public Layer
{
private :
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	Layer* LayerExit;
	bool FlagOption;
	bool FlagExit;
	bool FlagAudio;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	Button* PopExit;
	Button* PopCancle;

	//Log Management
	LogManagement *logManager;
public:

	Button* Button_O;
	Button* Button_X;
	Button* ButtonConfirm;

	Button* ButtonAnswer[12];

	//Button* InvisibleButton[8];
	//Sprite* InvisibleSprite[2];
	
	Sprite* SpriteQuestion[3];

	int gameAnswer, selAnswer;

	SceneGame_3_2();
	~SceneGame_3_2();

	static cocos2d::Scene* createScene(bool _booltmp = false);
	virtual bool init();

	CREATE_FUNC(SceneGame_3_2);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();
	void ScheduleAudio(float dt);

	//int setRandomAnswer(int QuestionNumber);
	bool isCorrect();

//	String String_Question = "./q"; //+1~3
	//String String_Ans = "./q";

	int randomizeNumber;

};



#endif 