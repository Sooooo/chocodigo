﻿//기본 헤더
#include "SceneGame_1_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
//#include <stdlib.h>
#include "simpleAudioEngine.h"

//화면 이동 라이브러리
#include "SceneStage_1.h"

USING_NS_CC;
using namespace ui;

bool isReplay11;

SceneGame_1_1::SceneGame_1_1(){
	FlagOption = OFF;
	FlagAudio = false;
	FlagExit = false;

	logManager = new LogManagement;
	BGM = "Game_Resource/Sound/BGM_1.mp3";
}

SceneGame_1_1::~SceneGame_1_1(){
	delete logManager;

	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}

Scene* SceneGame_1_1::createScene(bool _bool)
{
	isReplay11 = _bool;

	auto scene = Scene::create();

	auto layer = SceneGame_1_1::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_1_1::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//Import csb file
	auto CSB_SceneGame_1_1 = CSLoader::createNode("Scene_Game_1_1.csb");
	addChild(CSB_SceneGame_1_1);

	//Option Layer & Button
	LayerOption = (Layer*)CSB_SceneGame_1_1->getChildByTag(TAG_LAYER_OPTION);
	LayerExit = (Layer*)CSB_SceneGame_1_1->getChildByTag(TAG_LAYER_EXIT);
	ButtonOption = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_OPTION);

	Back = (Button*)LayerOption->getChildByTag(TAG_BUTTON_BACK);
	Audio = (CheckBox*)LayerOption->getChildByTag(TAG_AUDIO);
	Exit = (Button*)LayerOption->getChildByTag(TAG_BUTTON_EXIT);

	PopExit = (Button*)LayerExit->getChildByTag(TAG_EXIT);
	PopCancle = (Button*)LayerExit->getChildByTag(TAG_CANCLE);

	ButtonOption->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	Back->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	Audio->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	Exit->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	PopExit->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	PopCancle->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));

	LayerOption->setVisible(false);
	LayerExit->setVisible(false);

	if (logManager->getSystemSound() == OFF)
	{
		Audio->setSelected(true);
	}
	else if (logManager->getSystemSound() == ON)
	{
		Audio->setSelected(false);
	}

	//Scheduler
	this->scheduleOnce(schedule_selector(SceneGame_1_1::ScheduleAudio), 1.5f);

	//Sound
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		FlagAudio = true;
	}

	if (logManager->getSystemSound() == OFF)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
	}

	//Button
	ButtonRun = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_START);
	ButtonReset = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_RESET);

	ButtonSelect_1_1 = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_SELECT_VALUE_1_1);
	ButtonSelect_1_2 = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_SELECT_VALUE_1_2);

	ButtonSelect_2_1 = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_SELECT_VALUE_2_1);
	ButtonSelect_2_2 = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_SELECT_VALUE_2_2);
	ButtonSelect_2_3 = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_SELECT_VALUE_2_3);

	ButtonSelect_3_1 = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_SELECT_VALUE_3_1);
	ButtonSelect_3_2 = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_SELECT_VALUE_3_2);
	ButtonSelect_3_3 = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_BUTTON_SELECT_VALUE_3_3);

	Button_O = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_O);
	Button_X = (Button*)CSB_SceneGame_1_1->getChildByTag(TAG_X);

	ButtonRun->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	ButtonReset->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));

	ButtonSelect_1_1->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	ButtonSelect_1_2->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));

	ButtonSelect_2_1->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	ButtonSelect_2_2->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	ButtonSelect_2_3->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));

	ButtonSelect_3_1->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	ButtonSelect_3_2->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	ButtonSelect_3_3->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));

	Button_O->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));
	Button_X->addTouchEventListener(CC_CALLBACK_2(SceneGame_1_1::onTouch, this));

	//Sprite
	SpriteClothes = (Sprite*)CSB_SceneGame_1_1->getChildByTag(TAG_SPRITE_CLOTHES);
	SpritePatternStar = (Sprite*)CSB_SceneGame_1_1->getChildByTag(TAG_SPRITE_PATTERN_STAR);
	SpritePatternSprite = (Sprite*)CSB_SceneGame_1_1->getChildByTag(TAG_SPRITE_PATTERN_SPRITE);
	SpritePatternSnow = (Sprite*)CSB_SceneGame_1_1->getChildByTag(TAG_SPRITE_PATTERN_SNOW);
	
	//이미지 랜덤으로 출력
	// TAG_IMG_COMPLETED 의 이미지를 랜덤으로 색 지정 (빨강 파랑 흰색)
	srand((unsigned int)time(NULL));
	category = 701;
	pattern = setRandomPattern();
	color = setRandomColor();
	return true;
}

int SceneGame_1_1::setRandomPattern() {
	// 랜덤으로 패턴 visible

	int buf = rand() % 3;
	switch (buf) {
	case 0:
		SpritePatternSprite->setVisible(false);
		SpritePatternStar->setVisible(true);
		SpritePatternSnow->setVisible(false);
		break;
	case 1:
		SpritePatternSprite->setVisible(true);
		SpritePatternSnow->setVisible(false);
		SpritePatternStar->setVisible(false);
		break;
	case 2:
		SpritePatternSprite->setVisible(false);
		SpritePatternSnow->setVisible(true);
		SpritePatternStar->setVisible(false);
		break;
	}
	return (buf + 706);
}

int SceneGame_1_1::setRandomColor() {
	// 랜덤으로 패턴 visible

	auto action = TintTo::create(0, 255, 0, 0);;
	int buf = rand() % 3;
	switch (buf) {
	case 0:
		action = TintTo::create(0, 255, 0, 0);
		break;
	case 1:
		action = TintTo::create(0, 0, 0, 255);
		break;
	case 2:
		action = TintTo::create(0, 255, 242, 0);
		break;
	}
	SpriteClothes->runAction(action);
	return (buf + 703);
}

bool SceneGame_1_1::isCorrect() {
	if ((color == selColor) && (category == selCategory) && (pattern == selPattern))
		return true;
	else return false;
}

int SceneGame_1_1::buttonTouch(int disable, int able1, int able2) {

	//able의 태그를 가진 버튼 제외하곤 모두 disable
	switch (able1) {
	case TAG_BUTTON_SELECT_VALUE_1_1:
		ButtonSelect_1_1->setTouchEnabled(true);
		ButtonSelect_1_1->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_1_2:
		ButtonSelect_1_2->setTouchEnabled(true);
		ButtonSelect_1_2->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_2_1:
		ButtonSelect_2_1->setTouchEnabled(true);
		ButtonSelect_2_1->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_2_2:
		ButtonSelect_2_2->setTouchEnabled(true);
		ButtonSelect_2_2->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_2_3:
		ButtonSelect_2_3->setTouchEnabled(true);
		ButtonSelect_2_3->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_3_1:
		ButtonSelect_3_1->setTouchEnabled(true);
		ButtonSelect_3_1->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_3_2:
		ButtonSelect_3_2->setTouchEnabled(true);
		ButtonSelect_3_2->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_3_3:
		ButtonSelect_3_3->setTouchEnabled(true);
		ButtonSelect_3_3->setBright(true);
		break;
	}

	switch (able2) {
	case TAG_BUTTON_SELECT_VALUE_1_1:
		ButtonSelect_1_1->setTouchEnabled(true);
		ButtonSelect_1_1->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_1_2:
		ButtonSelect_1_2->setTouchEnabled(true);
		ButtonSelect_1_2->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_2_1:
		ButtonSelect_2_1->setTouchEnabled(true);
		ButtonSelect_2_1->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_2_2:
		ButtonSelect_2_2->setTouchEnabled(true);
		ButtonSelect_2_2->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_2_3:
		ButtonSelect_2_3->setTouchEnabled(true);
		ButtonSelect_2_3->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_3_1:
		ButtonSelect_3_1->setTouchEnabled(true);
		ButtonSelect_3_1->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_3_2:
		ButtonSelect_3_2->setTouchEnabled(true);
		ButtonSelect_3_2->setBright(true);
		break;
	case TAG_BUTTON_SELECT_VALUE_3_3:
		ButtonSelect_3_3->setTouchEnabled(true);
		ButtonSelect_3_3->setBright(true);
		break;
	}

	switch (disable) {
	case TAG_BUTTON_SELECT_VALUE_1_1:
		ButtonSelect_1_1->setTouchEnabled(false);
		ButtonSelect_1_1->setBright(false);
		break;
	case TAG_BUTTON_SELECT_VALUE_1_2:
		ButtonSelect_1_2->setTouchEnabled(false);
		ButtonSelect_1_2->setBright(false);
		break;
	case TAG_BUTTON_SELECT_VALUE_2_1:
		ButtonSelect_2_1->setTouchEnabled(false);
		ButtonSelect_2_1->setBright(false);
		break;
	case TAG_BUTTON_SELECT_VALUE_2_2:
		ButtonSelect_2_2->setTouchEnabled(false);
		ButtonSelect_2_2->setBright(false);
		break;
	case TAG_BUTTON_SELECT_VALUE_2_3:
		ButtonSelect_2_3->setTouchEnabled(false);
		ButtonSelect_2_3->setBright(false);
		break;
	case TAG_BUTTON_SELECT_VALUE_3_1:
		ButtonSelect_3_1->setTouchEnabled(false);
		ButtonSelect_3_1->setBright(false);
		break;
	case TAG_BUTTON_SELECT_VALUE_3_2:
		ButtonSelect_3_2->setTouchEnabled(false);
		ButtonSelect_3_2->setBright(false);
		break;
	case TAG_BUTTON_SELECT_VALUE_3_3:
		ButtonSelect_3_3->setTouchEnabled(false);
		ButtonSelect_3_3->setBright(false);
		break;
	}

	return disable;
}

void SceneGame_1_1::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){

		auto SelectButton = (Button*)sender;
		auto action = ToggleVisibility::create();

		switch (SelectButton->getTag())
		{
			case TAG_BUTTON_OPTION:
			{
				
				if (FlagOption == ON)
				{
					LayerOption->setVisible(false);
					FlagOption = OFF;
				}
				else if (FlagOption == OFF)
				{
					LayerOption->setVisible(true);
					FlagOption = ON;
				}

				break;
			}
			case TAG_AUDIO:
			{
				if (Audio->isSelected() == false)
				{
					logManager->setSoundOption(OFF);
					//mute
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
				}
				else if (Audio->isSelected() == true)
				{
					logManager->setSoundOption(ON);
					//not mute
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
					CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				}

				break;
			}
			case TAG_BUTTON_EXIT:
			{
				

				LayerExit->setVisible(true);

				ButtonOption->setTouchEnabled(false);
				Back->setTouchEnabled(false);
				Audio->setTouchEnabled(false);
				Exit->setTouchEnabled(false);
				
				ButtonSelect_1_1->setTouchEnabled(false);
				ButtonSelect_1_2->setTouchEnabled(false);
				ButtonSelect_2_1->setTouchEnabled(false);
				ButtonSelect_2_2->setTouchEnabled(false);
				ButtonSelect_2_3->setTouchEnabled(false);
				ButtonSelect_3_1->setTouchEnabled(false);
				ButtonSelect_3_2->setTouchEnabled(false);
				ButtonSelect_3_3->setTouchEnabled(false);
				ButtonRun->setTouchEnabled(false);
				ButtonReset->setTouchEnabled(false);

				break;
			}
			case TAG_BUTTON_BACK:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();

				Scene* scene;

				if (isReplay11) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
				}
				else {
					scene = TransitionFade::create(0.5, SceneStage_1::createScene());
				}
								
				Director::getInstance()->replaceScene(scene);
				break;
			}
			case TAG_CANCLE:
			{
				

				LayerExit->setVisible(false);

				ButtonOption->setTouchEnabled(true);
				Back->setTouchEnabled(true);
				Audio->setTouchEnabled(true);
				Exit->setTouchEnabled(true);

				ButtonSelect_1_1->setTouchEnabled(true);
				ButtonSelect_1_2->setTouchEnabled(true);
				ButtonSelect_2_1->setTouchEnabled(true);
				ButtonSelect_2_2->setTouchEnabled(true);
				ButtonSelect_2_3->setTouchEnabled(true);
				ButtonSelect_3_1->setTouchEnabled(true);
				ButtonSelect_3_2->setTouchEnabled(true);
				ButtonSelect_3_3->setTouchEnabled(true);
				ButtonRun->setTouchEnabled(true);
				ButtonReset->setTouchEnabled(true);

				break;
			}
			case TAG_EXIT:
			{
				
				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
				CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
				CocosDenshion::SimpleAudioEngine::getInstance()->end();

				appplicationExit();

				break;
			}
			case TAG_BUTTON_START:
			{
				if (isCorrect()) {
					Button_O->setVisible(true);
				}
				else {
					Button_X->setVisible(true);
				}
				break;
			}
			case TAG_O:
			{
				

				Director::getInstance()->getEventDispatcher()->removeAllEventListeners();

				Scene* scene;

				Button_O->setVisible(false);

				if (isReplay11) {
					scene = TransitionFade::create(0.5, QuestReplay::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				else {
					logManager->setChapterProgress(80);
					scene = TransitionFade::create(0.5, SceneStage_1::createScene());
					Director::getInstance()->replaceScene(scene);
				}
				
			}
			case TAG_X:
			{
				SceneGame_1_1::init();
			}
			case TAG_BUTTON_RESET:
			{
				

				ButtonSelect_1_1->setTouchEnabled(true);
				ButtonSelect_1_1->setBright(true);
				ButtonSelect_1_2->setTouchEnabled(true);
				ButtonSelect_1_2->setBright(true);

				ButtonSelect_2_1->setTouchEnabled(true);
				ButtonSelect_2_1->setBright(true);
				ButtonSelect_2_2->setTouchEnabled(true);
				ButtonSelect_2_2->setBright(true);
				ButtonSelect_2_3->setTouchEnabled(true);
				ButtonSelect_2_3->setBright(true);

				ButtonSelect_3_1->setTouchEnabled(true);
				ButtonSelect_3_1->setBright(true);
				ButtonSelect_3_2->setTouchEnabled(true);
				ButtonSelect_3_2->setBright(true);
				ButtonSelect_3_3->setTouchEnabled(true);
				ButtonSelect_3_3->setBright(true);
				selCategory = selColor = selPattern = 0;
				break;
			}
			case TAG_BUTTON_SELECT_VALUE_1_1:{
				selCategory = buttonTouch(TAG_BUTTON_SELECT_VALUE_1_1, TAG_BUTTON_SELECT_VALUE_1_2, 0);
				break;
			}
			case TAG_BUTTON_SELECT_VALUE_1_2:{
				selCategory = buttonTouch(TAG_BUTTON_SELECT_VALUE_1_2, TAG_BUTTON_SELECT_VALUE_1_1, 0);
				break;
			}
			case TAG_BUTTON_SELECT_VALUE_2_1:{
				selColor = buttonTouch(TAG_BUTTON_SELECT_VALUE_2_1, TAG_BUTTON_SELECT_VALUE_2_2, TAG_BUTTON_SELECT_VALUE_2_3);
				break;
			}
			case TAG_BUTTON_SELECT_VALUE_2_2:{
				selColor = buttonTouch(TAG_BUTTON_SELECT_VALUE_2_2, TAG_BUTTON_SELECT_VALUE_2_1, TAG_BUTTON_SELECT_VALUE_2_3);
				break;
			}
			case TAG_BUTTON_SELECT_VALUE_2_3:{
				selColor = buttonTouch(TAG_BUTTON_SELECT_VALUE_2_3, TAG_BUTTON_SELECT_VALUE_2_2, TAG_BUTTON_SELECT_VALUE_2_1);
				break;
			}
			case TAG_BUTTON_SELECT_VALUE_3_1:{
				selPattern = buttonTouch(TAG_BUTTON_SELECT_VALUE_3_1, TAG_BUTTON_SELECT_VALUE_3_2, TAG_BUTTON_SELECT_VALUE_3_3);
				break;
			}
			case TAG_BUTTON_SELECT_VALUE_3_2:{
				selPattern = buttonTouch(TAG_BUTTON_SELECT_VALUE_3_2, TAG_BUTTON_SELECT_VALUE_3_1, TAG_BUTTON_SELECT_VALUE_3_3);
				break;
			}
			case TAG_BUTTON_SELECT_VALUE_3_3:{
				selPattern = buttonTouch(TAG_BUTTON_SELECT_VALUE_3_3, TAG_BUTTON_SELECT_VALUE_3_2, TAG_BUTTON_SELECT_VALUE_3_1);
				break;
			}
			default:
				break;
		}
	}

}

void SceneGame_1_1::appplicationExit(){
	Director::getInstance()->end();


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ios)
	EXIT(0);
#endif
}

void SceneGame_1_1::ScheduleAudio(float dt)
{
	if (FlagAudio == true && logManager->getSystemSound() == ON)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(BGM.c_str(), true);
	}
}