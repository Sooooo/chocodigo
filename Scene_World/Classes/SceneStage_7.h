﻿#pragma execution_character_set("UTF-8")

#ifndef __SCENE_STAGE_7_H__
#define __SCENE_STAGE_7_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

#include "CommonTagSet.h"
#include "LogManagement.h"

/*************** 태그 정보 ***************/
#define TAG_MAP_1				201
#define TAG_MAP_2				202
#define TAG_MAP_3				203
/****************************************/


class SceneStage_7 : public Layer
{
private:
	enum
	{
		OFF = 0,
		ON = 1
	};

	std::string BGM;

	//Option Layer
	Layer* LayerOption;
	bool FlagOption;
	Button* ButtonOption;
	Button* Back;
	CheckBox* Audio;	//Check : OFF, nonCheck : ON
	Button* Exit;

	//Log Management
	LogManagement* logManager;

	//Scroll
	ScrollView* Scroll;

	//Map
	Button* Map_1;
	Button* Map_2;
	Button* Map_3;

public:
	SceneStage_7();
	~SceneStage_7();

	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(SceneStage_7);

	void onTouch(Ref* sender, Widget::TouchEventType type);
	void appplicationExit();

};

#endif