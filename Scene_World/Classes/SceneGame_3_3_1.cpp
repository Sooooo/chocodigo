﻿//기본 헤더
#include "SceneGame_3_3_1.h"

//사용 라이브러리
#include "cocostudio/CocoStudio.h"
#include "ui\CocosGUI.h"

//화면 전환
#include "SceneGame_3_3.h"

USING_NS_CC;
using namespace ui;

SceneGame_3_3_1::SceneGame_3_3_1(){

}

SceneGame_3_3_1::~SceneGame_3_3_1(){
	this->removeAllChildren();
	Director::getInstance()->purgeCachedData();
}


Scene* SceneGame_3_3_1::createScene()
{
	auto scene = Scene::create();

	auto layer = SceneGame_3_3_1::create();
	scene->addChild(layer);

	return scene;
}

bool SceneGame_3_3_1::init()
{

	if (!Layer::init())
	{
		return false;
	}

	auto CSB_SceneGame_3_3_1 = CSLoader::createNode("Scene_Game_3_3_1.csb");
	addChild(CSB_SceneGame_3_3_1);

	//스프라이트,버튼 불러오기
	ButtonBack = (Button*)CSB_SceneGame_3_3_1->getChildByTag(TAG_BUTTON_BACK);
	ButtonNext = (Button*)CSB_SceneGame_3_3_1->getChildByTag(TAG_BUTTON_NEXT);
	ButtonFront = (Button*)CSB_SceneGame_3_3_1->getChildByTag(TAG_BUTTON_FRONT);
	
	Recipe1 = (TextField*)CSB_SceneGame_3_3_1->getChildByTag(TAG_TEXT_PAGE1);
	Recipe2 = (TextField*)CSB_SceneGame_3_3_1->getChildByTag(TAG_TEXT_PAGE2);
	Recipe3 = (TextField*)CSB_SceneGame_3_3_1->getChildByTag(TAG_TEXT_PAGE3);
	Recipe4 = (TextField*)CSB_SceneGame_3_3_1->getChildByTag(TAG_TEXT_PAGE4);

	//터치이벤트 추가
	ButtonBack->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_1::onTouch, this));
	ButtonNext->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_1::onTouch, this));
	ButtonFront->addTouchEventListener(CC_CALLBACK_2(SceneGame_3_3_1::onTouch, this));

	return true;
}
void SceneGame_3_3_1::onTouch(Ref* sender, Widget::TouchEventType type){
	if (type == Widget::TouchEventType::ENDED){
		

		auto SelectButton = (Button*)sender;

		switch (SelectButton->getTag())
		{
		case TAG_BUTTON_BACK:{
			
			Director::getInstance()->popScene();
			break;
		}
		case TAG_BUTTON_NEXT:{
			Recipe1->setVisible(false);
			Recipe2->setVisible(false);
			Recipe3->setVisible(true);
			Recipe4->setVisible(true);
			ButtonNext->setVisible(false);
			ButtonFront->setVisible(true);
			break;
		}
		case TAG_BUTTON_FRONT:{
			Recipe1->setVisible(true);
			Recipe2->setVisible(true);
			Recipe3->setVisible(false);
			Recipe4->setVisible(false);
			ButtonNext->setVisible(true);
			ButtonFront->setVisible(false);


			break;
		}

		default:
			break;
		}
	}
}
