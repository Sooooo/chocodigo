<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_2_2_1" ID="11e67f49-387a-4c9c-8f63-df9b4e8c855b" Version="2.2.5.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="SceneGame_2_2_1" Tag="29" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Background" ActionTag="-757598471" IconVisible="False" LeftMargin="320.2326" RightMargin="319.7673" TopMargin="55.7731" BottomMargin="54.2269" ctype="SpriteObjectData">
            <Size X="1280.0000" Y="970.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="960.2326" Y="539.2269" />
            <Scale ScaleX="1.5078" ScaleY="1.1134" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5001" Y="0.4993" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/board.jpg" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q_Title" ActionTag="1599204561" IconVisible="False" LeftMargin="51.1724" RightMargin="1337.8276" TopMargin="91.9375" BottomMargin="805.0625" ctype="SpriteObjectData">
            <Size X="531.0000" Y="183.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="316.6724" Y="896.5625" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1649" Y="0.8302" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/2_SANTA/Q1/Q1_Title.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="TotalNum" ActionTag="824800773" IconVisible="False" LeftMargin="858.2568" RightMargin="961.7432" TopMargin="166.5037" BottomMargin="886.4963" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="/ 3" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="908.2568" Y="899.9963" />
            <Scale ScaleX="3.3200" ScaleY="3.6667" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.4731" Y="0.8333" />
            <PreSize />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CurrentNum" ActionTag="-1713687683" Tag="701" IconVisible="False" LeftMargin="758.2574" RightMargin="1061.7427" TopMargin="166.5037" BottomMargin="886.4963" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="1" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="808.2574" Y="899.9963" />
            <Scale ScaleX="3.3200" ScaleY="3.6667" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.4210" Y="0.8333" />
            <PreSize />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="description_1" ActionTag="1068623692" IconVisible="False" LeftMargin="1310.7356" RightMargin="468.2644" TopMargin="211.2910" BottomMargin="848.7090" FontSize="20" LabelText="      = 1 ,    = 0" ctype="TextObjectData">
            <Size X="141.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1381.2356" Y="858.7090" />
            <Scale ScaleX="3.6876" ScaleY="3.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7194" Y="0.7951" />
            <PreSize />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanM_0.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="description_2" ActionTag="-332712117" IconVisible="False" LeftMargin="1341.7419" RightMargin="294.2581" TopMargin="74.2587" BottomMargin="722.7413" ctype="SpriteObjectData">
            <Size X="284.0000" Y="283.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1483.7419" Y="864.2413" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7728" Y="0.8002" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="description_3" ActionTag="-44248391" IconVisible="False" LeftMargin="1077.2402" RightMargin="558.7598" TopMargin="77.2703" BottomMargin="726.7297" ctype="SpriteObjectData">
            <Size X="284.0000" Y="276.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1219.2402" Y="864.7297" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6350" Y="0.8007" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Operation" ActionTag="2075494118" Tag="711" IconVisible="False" LeftMargin="946.6552" RightMargin="873.3447" TopMargin="466.5000" BottomMargin="586.5000" TouchEnable="True" FontSize="48" IsCustomSize="True" LabelText="" PlaceHolderText="and" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="996.6552" Y="600.0000" />
            <Scale ScaleX="2.9600" ScaleY="3.3704" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5191" Y="0.5556" />
            <PreSize />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Equal" ActionTag="-2139458817" IconVisible="False" LeftMargin="694.9854" RightMargin="1125.0146" TopMargin="726.5000" BottomMargin="326.5000" TouchEnable="True" FontSize="48" IsCustomSize="True" LabelText="" PlaceHolderText="=" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="744.9854" Y="340.0000" />
            <Scale ScaleX="2.9600" ScaleY="3.3704" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3880" Y="0.3148" />
            <PreSize />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Star_1" ActionTag="413362388" Tag="702" IconVisible="False" LeftMargin="290.0000" RightMargin="1590.0000" TopMargin="460.0000" BottomMargin="580.0000" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="310.0000" Y="600.0000" />
            <Scale ScaleX="4.7000" ScaleY="4.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1615" Y="0.5556" />
            <PreSize />
            <NormalBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Star_2" ActionTag="-354336287" Tag="703" IconVisible="False" LeftMargin="490.0000" RightMargin="1390.0000" TopMargin="460.0000" BottomMargin="580.0000" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="510.0000" Y="600.0000" />
            <Scale ScaleX="4.7000" ScaleY="4.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2656" Y="0.5556" />
            <PreSize />
            <NormalBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Star_3" ActionTag="1866744584" Tag="704" IconVisible="False" LeftMargin="690.0000" RightMargin="1190.0000" TopMargin="460.0000" BottomMargin="580.0000" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="710.0000" Y="600.0000" />
            <Scale ScaleX="4.7000" ScaleY="4.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3698" Y="0.5556" />
            <PreSize />
            <NormalBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Star_4" ActionTag="-1794512892" Tag="705" IconVisible="False" LeftMargin="1190.0000" RightMargin="690.0000" TopMargin="460.0000" BottomMargin="580.0000" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1210.0000" Y="600.0000" />
            <Scale ScaleX="4.7000" ScaleY="4.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6302" Y="0.5556" />
            <PreSize />
            <NormalBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Star_5" ActionTag="827456517" Tag="706" IconVisible="False" LeftMargin="1390.0000" RightMargin="490.0000" TopMargin="460.0000" BottomMargin="580.0000" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1410.0000" Y="600.0000" />
            <Scale ScaleX="4.7000" ScaleY="4.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7344" Y="0.5556" />
            <PreSize />
            <NormalBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Star_6" ActionTag="-1433750957" Tag="707" IconVisible="False" LeftMargin="1590.0000" RightMargin="290.0000" TopMargin="460.0000" BottomMargin="580.0000" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1610.0000" Y="600.0000" />
            <Scale ScaleX="4.7000" ScaleY="4.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8385" Y="0.5556" />
            <PreSize />
            <NormalBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Star_7" ActionTag="-1907569505" Tag="710" IconVisible="False" LeftMargin="1188.9833" RightMargin="691.0167" TopMargin="720.0000" BottomMargin="320.0000" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1208.9833" Y="340.0000" />
            <Scale ScaleX="4.7000" ScaleY="4.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6297" Y="0.3148" />
            <PreSize />
            <NormalBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Star_8" ActionTag="50571370" Tag="709" IconVisible="False" LeftMargin="984.9836" RightMargin="895.0164" TopMargin="720.0000" BottomMargin="320.0000" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1004.9836" Y="340.0000" />
            <Scale ScaleX="4.7000" ScaleY="4.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5234" Y="0.3148" />
            <PreSize />
            <NormalBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Star_9" ActionTag="-952041265" Tag="708" IconVisible="False" LeftMargin="780.9923" RightMargin="1099.0077" TopMargin="720.0000" BottomMargin="320.0000" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="800.9923" Y="340.0000" />
            <Scale ScaleX="4.7000" ScaleY="4.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4172" Y="0.3148" />
            <PreSize />
            <NormalBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Empty.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Star_Full.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" ActionTag="678761947" Tag="602" IconVisible="False" LeftMargin="935.9903" RightMargin="938.0097" TopMargin="521.7764" BottomMargin="522.2236" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" OutlineSize="1" ShadowOffsetX="2" ShadowOffsetY="-2" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="958.9903" Y="540.2236" />
            <Scale ScaleX="42.0374" ScaleY="29.8335" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4995" Y="0.5002" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" ActionTag="2107864042" Tag="603" IconVisible="False" LeftMargin="936.3342" RightMargin="937.6658" TopMargin="521.5344" BottomMargin="522.4656" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" OutlineSize="1" ShadowOffsetX="2" ShadowOffsetY="-2" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="959.3342" Y="540.4656" />
            <Scale ScaleX="42.0374" ScaleY="29.8335" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4997" Y="0.5004" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Play" ActionTag="283361948" Tag="601" IconVisible="False" LeftMargin="1540.2000" RightMargin="333.8000" TopMargin="961.2540" BottomMargin="82.7460" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="616" Scale9Height="151" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1563.2000" Y="100.7460" />
            <Scale ScaleX="13.3913" ScaleY="4.0555" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8142" Y="0.0933" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Q1/Button_start.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>