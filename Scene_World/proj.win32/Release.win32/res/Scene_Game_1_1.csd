<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_1_1" ID="1a3c4eb1-c61f-48ff-8068-936ae073df73" Version="2.2.5.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="SceneGame_1_1" Tag="28" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="bg_2" ActionTag="-1942487473" IconVisible="False" LeftMargin="-0.6680" RightMargin="0.6680" TopMargin="-0.6667" BottomMargin="0.6667" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="959.3320" Y="540.6667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4997" Y="0.5006" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Q1/bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1" ActionTag="197141347" Tag="601" IconVisible="False" LeftMargin="1444.3645" RightMargin="429.6355" TopMargin="769.3270" BottomMargin="274.6730" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="616" Scale9Height="151" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1467.3645" Y="292.6730" />
            <Scale ScaleX="13.3189" ScaleY="4.1481" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7643" Y="0.2710" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Q1/Button_start.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_0" ActionTag="1074742410" Tag="602" IconVisible="False" LeftMargin="1445.3445" RightMargin="428.6555" TopMargin="940.3328" BottomMargin="103.6672" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="616" Scale9Height="151" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1468.3445" Y="121.6672" />
            <Scale ScaleX="13.3189" ScaleY="4.1481" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7648" Y="0.1127" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Q1/reset.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_3" ActionTag="-1635698382" Tag="701" IconVisible="False" LeftMargin="251.9987" RightMargin="1622.0013" TopMargin="210.3240" BottomMargin="833.6760" TouchEnable="True" FontSize="14" ButtonText="+" Scale9Enable="True" Scale9Width="201" Scale9Height="147" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="274.9987" Y="851.6760" />
            <Scale ScaleX="4.3334" ScaleY="3.9630" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1432" Y="0.7886" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Q1/Button_SelectAttribute.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>