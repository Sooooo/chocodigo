<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Pop_Game_2" ID="4eb199c4-ad7d-4f3e-89c4-459d661558e9" Version="2.2.5.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="ScenePopGame_2" Tag="63" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="PopBackgourndGift" ActionTag="676265212" IconVisible="False" LeftMargin="282.9149" RightMargin="357.0851" TopMargin="154.1852" BottomMargin="20.8148" ctype="SpriteObjectData">
            <Size X="1280.0000" Y="905.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="922.9149" Y="473.3148" />
            <Scale ScaleX="2.0769" ScaleY="1.6884" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4807" Y="0.4383" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/Pop_Backgournd_Gift.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Success" ActionTag="-1228569655" IconVisible="False" LeftMargin="705.1604" RightMargin="583.8396" TopMargin="408.5344" BottomMargin="347.4656" ctype="SpriteObjectData">
            <Size X="631.0000" Y="324.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1020.6604" Y="509.4656" />
            <Scale ScaleX="0.9440" ScaleY="0.9508" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5316" Y="0.4717" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/System/Success.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ButtonBack" ActionTag="-467693621" Tag="601" IconVisible="False" LeftMargin="1400.7041" RightMargin="473.2959" TopMargin="860.0321" BottomMargin="183.9679" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" OutlineSize="1" ShadowOffsetX="2" ShadowOffsetY="-2" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1423.7041" Y="201.9679" />
            <Scale ScaleX="9.4268" ScaleY="4.1405" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7415" Y="0.1870" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>