<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Stage_2" ID="b60b05d0-ddd2-4ea3-8ec1-7b8a5a71c392" Version="2.2.5.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="SceneStage_2" Tag="95" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="ScrollView" ActionTag="25465589" Tag="1" Rotation="269.9916" RotationSkewX="269.9916" RotationSkewY="270.0000" IconVisible="False" LeftMargin="3479.4512" RightMargin="-1759.4512" TopMargin="877.1152" BottomMargin="2.8848" TouchEnable="True" BackColorAlpha="0" ComboBoxIndex="2" ColorAngle="112.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Backgrond_1_3" ActionTag="149751674" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="-859.4053" RightMargin="-860.5947" TopMargin="-484.6891" BottomMargin="-295.3109" ctype="SpriteObjectData">
                <Size X="1920.0000" Y="1080.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.5947" Y="244.6891" />
                <Scale ScaleX="0.0576" ScaleY="0.1854" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5030" Y="0.8156" />
                <PreSize />
                <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/stage_1_backgrond.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Backgrond_2_5" ActionTag="1616628805" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="-859.4145" RightMargin="-860.5854" TopMargin="-374.2554" BottomMargin="-405.7446" FlipX="True" ctype="SpriteObjectData">
                <Size X="1920.0000" Y="1080.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.5855" Y="134.2554" />
                <Scale ScaleX="0.0576" ScaleY="0.1854" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5029" Y="0.4475" />
                <PreSize />
                <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/stage_1_backgrond.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_2" ActionTag="207315865" Tag="201" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="76.6822" RightMargin="77.3178" TopMargin="5.9297" BottomMargin="258.0703" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="600" Scale9Height="559" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="99.6822" Y="276.0703" />
                <Scale ScaleX="0.6829" ScaleY="2.1151" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4984" Y="0.9202" />
                <PreSize />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/magician_house.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_0_4" ActionTag="-287198909" Tag="202" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="76.7020" RightMargin="77.2980" TopMargin="64.2066" BottomMargin="199.7934" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="588" Scale9Height="417" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="99.7020" Y="217.7934" />
                <Scale ScaleX="0.6829" ScaleY="2.1151" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4985" Y="0.7260" />
                <PreSize />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/store.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_6" ActionTag="-722007235" Tag="203" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="80.4281" RightMargin="73.5719" TopMargin="103.1535" BottomMargin="160.8465" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="600" Scale9Height="559" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="103.4281" Y="178.8465" />
                <Scale ScaleX="0.6829" ScaleY="2.1151" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5171" Y="0.5962" />
                <PreSize />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/magician_house.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_2_8" ActionTag="43587086" Tag="204" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="85.9846" RightMargin="68.0154" TopMargin="179.9940" BottomMargin="84.0060" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="588" Scale9Height="417" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="108.9846" Y="102.0060" />
                <Scale ScaleX="0.6829" ScaleY="2.1151" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5449" Y="0.3400" />
                <PreSize />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/store.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="3479.4512" Y="2.8848" />
            <Scale ScaleX="5.4097" ScaleY="17.4168" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.8122" Y="0.0027" />
            <PreSize X="0.1042" Y="0.1852" />
            <SingleColor A="255" R="70" G="180" B="203" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleX="-0.3746" ScaleY="0.9272" />
            <InnerNodeSize Width="200" Height="300" />
          </AbstractNodeData>
          <AbstractNodeData Name="ButtonBack" ActionTag="-741904564" Tag="2" IconVisible="False" LeftMargin="252.0023" RightMargin="1621.9977" TopMargin="100.3294" BottomMargin="943.6706" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="357" Scale9Height="118" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="275.0023" Y="961.6706" />
            <Scale ScaleX="9.4058" ScaleY="4.5185" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1432" Y="0.8904" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Back.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BGM" ActionTag="-1524565621" Tag="3" IconVisible="True" LeftMargin="104.9949" RightMargin="1815.0051" TopMargin="1008.3323" BottomMargin="71.6677" Volume="1.0000" Loop="True" ctype="SimpleAudioObjectData">
            <Size />
            <AnchorPoint />
            <Position X="104.9949" Y="71.6677" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0547" Y="0.0664" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/Sound/BGM_vilage.mp3" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>