<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_2_2_2" ID="a0440df5-69bf-4910-a906-2a1b86ff52c6" Version="2.2.5.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="SceneGame_2_2" Tag="39" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="q2_bg_1" ActionTag="1143043718" Tag="26" IconVisible="False" LeftMargin="-1.3714" RightMargin="1.3713" TopMargin="1.3621" BottomMargin="-1.3621" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="958.6286" Y="538.6379" />
            <Scale ScaleX="1.0000" ScaleY="1.0093" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4993" Y="0.4987" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/2_SANTA/Q2/q2_bg.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_0" ActionTag="-1209342359" Tag="101" IconVisible="False" LeftMargin="1545.6448" RightMargin="328.3552" TopMargin="977.0000" BottomMargin="67.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="616" Scale9Height="151" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1568.6448" Y="85.0000" />
            <Scale ScaleX="13.3189" ScaleY="4.1481" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8170" Y="0.0787" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Q1/Button_start.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_0_0" ActionTag="545411803" Tag="102" IconVisible="False" LeftMargin="849.5504" RightMargin="1024.4496" TopMargin="977.0000" BottomMargin="67.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="616" Scale9Height="151" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="872.5504" Y="85.0000" />
            <Scale ScaleX="13.3189" ScaleY="4.1481" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4545" Y="0.0787" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Q1/reset.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>