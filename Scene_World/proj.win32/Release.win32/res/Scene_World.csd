<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_World" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="2.2.5.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="SceneWorld" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="ScrollView" ActionTag="1665350098" Tag="1" Rotation="269.9916" RotationSkewX="269.9916" RotationSkewY="270.0000" IconVisible="False" LeftMargin="3843.4458" RightMargin="-2123.4458" TopMargin="881.1327" BottomMargin="-1.1327" TouchEnable="True" BackColorAlpha="0" ComboBoxIndex="2" ColorAngle="112.0000" Scale9Width="1920" Scale9Height="1080" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="World_Backgrond_1" ActionTag="-56604961" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="-859.7750" RightMargin="-860.2250" TopMargin="-489.8636" BottomMargin="-290.1364" ctype="SpriteObjectData">
                <Size X="1920.0000" Y="1080.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.2250" Y="249.8636" />
                <Scale ScaleX="0.0524" ScaleY="0.1857" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5011" Y="0.8329" />
                <PreSize />
                <FileData Type="Normal" Path="Game_Resource/Scene_World/World.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="World_Backgrond_2" ActionTag="2021466520" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="-859.7749" RightMargin="-860.2251" TopMargin="-390.0711" BottomMargin="-389.9289" FlipX="True" ctype="SpriteObjectData">
                <Size X="1920.0000" Y="1080.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.2251" Y="150.0711" />
                <Scale ScaleX="0.0524" ScaleY="0.1852" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5011" Y="0.5002" />
                <PreSize />
                <FileData Type="Normal" Path="Game_Resource/Scene_World/World.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_Stage_0" ActionTag="1169355760" Tag="201" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="39.2786" RightMargin="114.7214" TopMargin="-3.5914" BottomMargin="267.5914" TouchEnable="True" FontSize="12" ButtonText="Disabled" Scale9Enable="True" Scale9Width="494" Scale9Height="421" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="62.2786" Y="285.5914" />
                <Scale ScaleX="0.5217" ScaleY="1.7637" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3114" Y="0.9520" />
                <PreSize X="0.0240" Y="0.0333" />
                <TextColor A="255" R="207" G="39" B="37" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/Scene_World/prologue.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_Stage_1" ActionTag="-669354511" Tag="202" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="97.3843" RightMargin="56.6157" TopMargin="36.4615" BottomMargin="227.5385" TouchEnable="True" FontSize="12" Scale9Enable="True" Scale9Width="494" Scale9Height="470" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="120.3843" Y="245.5385" />
                <Scale ScaleX="0.5217" ScaleY="1.7637" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6019" Y="0.8185" />
                <PreSize X="0.0240" Y="0.0333" />
                <TextColor A="255" R="207" G="39" B="37" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/Scene_World/chapter1.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_Stage_2" ActionTag="-587909216" Tag="203" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="44.5822" RightMargin="109.4178" TopMargin="67.2052" BottomMargin="196.7948" TouchEnable="True" FontSize="12" Scale9Enable="True" Scale9Width="520" Scale9Height="322" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="67.5822" Y="214.7948" />
                <Scale ScaleX="0.5217" ScaleY="1.7637" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3379" Y="0.7160" />
                <PreSize X="0.0240" Y="0.0333" />
                <TextColor A="255" R="207" G="39" B="37" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/Scene_World/chapter2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_Stage_3" ActionTag="-1691350585" Tag="204" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="43.0784" RightMargin="110.9216" TopMargin="96.0596" BottomMargin="167.9404" TouchEnable="True" FontSize="12" ButtonText="Disabled" Scale9Enable="True" Scale9Width="494" Scale9Height="421" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="66.0784" Y="185.9404" />
                <Scale ScaleX="0.5217" ScaleY="1.7637" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3304" Y="0.6198" />
                <PreSize X="0.0240" Y="0.0333" />
                <TextColor A="255" R="207" G="39" B="37" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/Scene_World/prologue.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_Stage_4" ActionTag="-1838958696" Tag="205" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="97.3375" RightMargin="56.6625" TopMargin="127.9920" BottomMargin="136.0080" TouchEnable="True" FontSize="12" ButtonText="Disabled" Scale9Enable="True" Scale9Width="494" Scale9Height="421" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="120.3375" Y="154.0080" />
                <Scale ScaleX="0.5217" ScaleY="1.7637" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6017" Y="0.5134" />
                <PreSize X="0.0240" Y="0.0333" />
                <TextColor A="255" R="207" G="39" B="37" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/Scene_World/prologue.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_Stage_5" ActionTag="394912659" Tag="206" Rotation="90.0000" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="41.9129" RightMargin="112.0871" TopMargin="168.5927" BottomMargin="95.4073" TouchEnable="True" FontSize="12" ButtonText="Disabled" Scale9Enable="True" Scale9Width="494" Scale9Height="421" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="64.9129" Y="113.4073" />
                <Scale ScaleX="0.5217" ScaleY="1.7637" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3246" Y="0.3780" />
                <PreSize X="0.0240" Y="0.0333" />
                <TextColor A="255" R="207" G="39" B="37" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="Game_Resource/Scene_World/prologue.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="3843.4458" Y="-1.1327" />
            <Scale ScaleX="5.4347" ScaleY="19.1919" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="2.0018" Y="-0.0010" />
            <PreSize X="0.1042" Y="0.1852" />
            <SingleColor A="255" R="70" G="180" B="203" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleX="-0.3746" ScaleY="0.9272" />
            <InnerNodeSize Width="200" Height="300" />
          </AbstractNodeData>
          <AbstractNodeData Name="BGM_1" ActionTag="-847861192" Tag="101" IconVisible="True" LeftMargin="98.6976" RightMargin="1821.3024" TopMargin="1006.0005" BottomMargin="73.9995" Volume="1.0000" Loop="True" ctype="SimpleAudioObjectData">
            <Size />
            <AnchorPoint />
            <Position X="98.6976" Y="73.9995" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0514" Y="0.0685" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/Sound/BGM_World.mp3" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Back" ActionTag="-108356260" Tag="2" IconVisible="False" LeftMargin="273.4158" RightMargin="1600.5842" TopMargin="142.9976" BottomMargin="901.0024" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="357" Scale9Height="118" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="296.4158" Y="919.0024" />
            <Scale ScaleX="9.4058" ScaleY="4.5185" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1544" Y="0.8509" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Back.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>