<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Prologue_1" ID="956494f5-d472-4ad9-b6fd-154c74700640" Version="2.2.5.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="ScenePrologue_1" Visible="False" Tag="31" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Backgorund" ActionTag="-1006827384" Tag="1" IconVisible="False" LeftMargin="937.9691" RightMargin="936.0309" TopMargin="521.3430" BottomMargin="522.6570" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1920" Scale9Height="1080" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="960.9691" Y="540.6570" />
            <Scale ScaleX="41.7247" ScaleY="30.0741" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5005" Y="0.5006" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Stage_1_prologue2.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Stage_1_prologue2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Audio_1" ActionTag="-1015135408" Tag="101" IconVisible="True" LeftMargin="71.6658" RightMargin="1848.3342" TopMargin="991.6669" BottomMargin="88.3331" Volume="1.0000" Loop="True" ctype="SimpleAudioObjectData">
            <Size />
            <AnchorPoint />
            <Position X="71.6658" Y="88.3331" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0373" Y="0.0818" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/Sound/BGM_Stage_1.mp3" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>