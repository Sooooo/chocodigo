<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Prologue_2" ID="e5253d1e-0f8f-4802-af98-de956b61ab40" Version="2.2.5.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="ScenePrologue_2" Visible="False" Tag="92" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Button_1" ActionTag="-208263570" Tag="1" IconVisible="False" LeftMargin="937.6470" RightMargin="936.3530" TopMargin="517.0032" BottomMargin="526.9968" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1920" Scale9Height="1280" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="960.6470" Y="544.9968" />
            <Scale ScaleX="41.7248" ScaleY="30.2592" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5003" Y="0.5046" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/2_SANTA/christmas.jpg" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/2_SANTA/christmas.jpg" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_1" ActionTag="-1543794648" IconVisible="False" LeftMargin="251.7306" RightMargin="1568.2694" TopMargin="129.2969" BottomMargin="923.7031" FontSize="24" IsCustomSize="True" LabelText="산타 마을" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="301.7306" Y="937.2031" />
            <Scale ScaleX="4.5800" ScaleY="5.7407" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.1572" Y="0.8678" />
            <PreSize />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanM_0.ttf" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>