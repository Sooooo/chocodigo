<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Title" ID="7dfb9f8b-11dd-4e0e-a0b4-e70c1fa6d9c0" Version="2.2.5.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="SceneTitle" Tag="14" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Title_Background" ActionTag="-309356081" IconVisible="False" LeftMargin="-0.6868" RightMargin="0.6868" TopMargin="1.5167" BottomMargin="-1.5167" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position X="1919.3132" Y="1078.4833" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9996" Y="0.9986" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/Scene_Title/Scene_Titlle.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="System_Logo" ActionTag="-1333214022" IconVisible="False" LeftMargin="601.8328" RightMargin="569.1672" TopMargin="104.5005" BottomMargin="587.4995" ctype="SpriteObjectData">
            <Size X="749.0000" Y="388.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="976.3328" Y="781.4995" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5085" Y="0.7236" />
            <PreSize />
            <FileData Type="Normal" Path="Game_Resource/Scene_Title/System_logo.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Title_Start" ActionTag="1294757987" Tag="1" IconVisible="False" LeftMargin="962.1511" RightMargin="911.8489" TopMargin="647.6729" BottomMargin="396.3271" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="616" Scale9Height="151" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="985.1511" Y="414.3271" />
            <Scale ScaleX="13.3912" ScaleY="4.1944" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5131" Y="0.3836" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/Scene_Title/Button_Title_Start.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Title_Quit" ActionTag="82313343" Tag="2" IconVisible="False" LeftMargin="961.6506" RightMargin="912.3494" TopMargin="849.6745" BottomMargin="194.3255" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="616" Scale9Height="151" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="984.6506" Y="212.3255" />
            <Scale ScaleX="13.3912" ScaleY="4.1944" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5128" Y="0.1966" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/Scene_Title/Button_Quit.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="290875474" Tag="3" IconVisible="False" LeftMargin="1759.1572" RightMargin="114.8428" TopMargin="919.6744" BottomMargin="124.3256" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="217" Scale9Height="217" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1782.1572" Y="142.3256" />
            <Scale ScaleX="4.6956" ScaleY="6.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9282" Y="0.1318" />
            <PreSize />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/Scene_Title/Button_Option.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Audio_2" ActionTag="230201740" Tag="101" IconVisible="True" LeftMargin="74.9990" RightMargin="1845.0010" TopMargin="990.0007" BottomMargin="89.9993" Volume="1.0000" Loop="True" ctype="SimpleAudioObjectData">
            <Size />
            <AnchorPoint />
            <Position X="74.9990" Y="89.9993" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0391" Y="0.0833" />
            <PreSize />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>