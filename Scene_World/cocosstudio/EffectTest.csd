<GameProjectFile>
  <PropertyGroup Type="Scene" Name="EffectTest" ID="9a7c81ba-b990-40a8-9f59-5a108c1323e0" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="15" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Particle_Player" ActionTag="-1520576948" Tag="50" IconVisible="True" LeftMargin="340.5026" RightMargin="1579.4974" TopMargin="813.8029" BottomMargin="266.1971" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="340.5026" Y="266.1971" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1773" Y="0.2465" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/defaultParticle.plist" Plist="" />
            <BlendFunc Src="775" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_Monster" ActionTag="214484291" Tag="51" IconVisible="True" LeftMargin="971.6610" RightMargin="948.3390" TopMargin="386.4573" BottomMargin="693.5427" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="971.6610" Y="693.5427" />
            <Scale ScaleX="0.9235" ScaleY="1.0647" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5061" Y="0.6422" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/defaultParticle.plist" Plist="" />
            <BlendFunc Src="775" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1" ActionTag="-1702960132" Tag="701" IconVisible="False" LeftMargin="1525.3682" RightMargin="348.6318" TopMargin="646.0228" BottomMargin="397.9772" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1548.3682" Y="415.9772" />
            <Scale ScaleX="12.3038" ScaleY="4.5307" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8064" Y="0.3852" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_0" ActionTag="-1443059823" Tag="702" IconVisible="False" LeftMargin="1529.5892" RightMargin="344.4108" TopMargin="846.6799" BottomMargin="197.3201" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1552.5892" Y="215.3201" />
            <Scale ScaleX="12.3038" ScaleY="4.5307" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8086" Y="0.1994" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>