<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_6_2" ID="71bafba4-92c3-416d-9a4d-1f2541751c8a" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="580500846" Tag="1" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/background.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_start" ActionTag="875388492" Tag="601" IconVisible="False" LeftMargin="1518.5000" RightMargin="38.5000" TopMargin="822.5000" BottomMargin="142.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1700.0000" Y="200.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8854" Y="0.1852" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Start.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Mouse" ActionTag="1259058974" Tag="701" IconVisible="False" LeftMargin="1648.2854" RightMargin="20.7146" TopMargin="20.1445" BottomMargin="748.8554" ctype="SpriteObjectData">
            <Size X="251.0000" Y="311.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1773.7854" Y="904.3554" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9238" Y="0.8374" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/mouse.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_dot" ActionTag="851594457" Tag="704" IconVisible="False" LeftMargin="1648.2900" RightMargin="20.7100" TopMargin="20.1400" BottomMargin="748.8600" ctype="SpriteObjectData">
            <Size X="251.0000" Y="311.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1773.7900" Y="904.3600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9238" Y="0.8374" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/dot.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Ribbon" ActionTag="-2147312739" Tag="702" IconVisible="False" LeftMargin="1648.2900" RightMargin="20.7100" TopMargin="20.1400" BottomMargin="748.8600" ctype="SpriteObjectData">
            <Size X="251.0000" Y="311.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1773.7900" Y="904.3600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9238" Y="0.8374" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/ribbon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Cheese" ActionTag="1276992995" Tag="703" IconVisible="False" LeftMargin="1648.2900" RightMargin="20.7100" TopMargin="20.1400" BottomMargin="748.8600" ctype="SpriteObjectData">
            <Size X="251.0000" Y="311.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1773.7900" Y="904.3600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9238" Y="0.8374" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/cheese.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_gram" ActionTag="1638623175" Tag="705" IconVisible="False" LeftMargin="1727.6635" RightMargin="92.3365" TopMargin="43.7917" BottomMargin="986.2083" TouchEnable="True" FontSize="48" IsCustomSize="True" LabelText="100g" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1777.6635" Y="1011.2083" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9259" Y="0.9363" />
            <PreSize X="0.0521" Y="0.0463" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__0" ActionTag="-394565693" Tag="710" IconVisible="False" LeftMargin="10.0000" RightMargin="1710.0000" TopMargin="865.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="110.0000" Y="115.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0573" Y="0.1065" />
            <PreSize X="0.1042" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house_checked.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__1" ActionTag="1973853514" Tag="711" IconVisible="False" LeftMargin="267.0468" RightMargin="1452.9531" TopMargin="865.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="367.0468" Y="115.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1912" Y="0.1065" />
            <PreSize X="0.1042" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house_checked.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__2" ActionTag="-1516648279" Tag="712" IconVisible="False" LeftMargin="520.2335" RightMargin="1199.7665" TopMargin="865.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="620.2335" Y="115.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3230" Y="0.1065" />
            <PreSize X="0.1042" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house_checked.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__3" ActionTag="-1241750352" Tag="713" IconVisible="False" LeftMargin="763.0492" RightMargin="956.9508" TopMargin="865.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="863.0492" Y="115.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4495" Y="0.1065" />
            <PreSize X="0.1042" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house_checked.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__4" ActionTag="2075509814" Tag="714" IconVisible="False" LeftMargin="1017.1394" RightMargin="702.8606" TopMargin="865.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1117.1394" Y="115.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5818" Y="0.1065" />
            <PreSize X="0.1042" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house_checked.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__5" ActionTag="519563918" Tag="715" IconVisible="False" LeftMargin="1289.6235" RightMargin="430.3765" TopMargin="865.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1389.6235" Y="115.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7238" Y="0.1065" />
            <PreSize X="0.1042" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house_checked.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_2/house.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" ActionTag="-128399776" VisibleForFrame="False" Tag="606" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" ActionTag="434718648" VisibleForFrame="False" Tag="605" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="-1806565623" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="1673529095" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2" ActionTag="1699281508" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>