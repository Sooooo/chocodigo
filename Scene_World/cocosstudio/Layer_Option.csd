<GameProjectFile>
  <PropertyGroup Type="Layer" Name="Layer_Option" ID="2c02f00f-61ae-4eff-b268-83e128422ea8" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="87" ctype="GameLayerObjectData">
        <Size X="558.0000" Y="147.0000" />
        <Children>
          <AbstractNodeData Name="Option_BG" ActionTag="1622567258" IconVisible="False" ctype="SpriteObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_SelectValue.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option_0_1" ActionTag="-922345910" Tag="3" IconVisible="False" LeftMargin="168.4678" RightMargin="269.5322" TopMargin="11.8516" BottomMargin="15.1484" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="90" Scale9Height="98" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="228.4678" Y="75.1484" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4094" Y="0.5112" />
            <PreSize X="0.2151" Y="0.8163" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Option_Back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="CheckBox_1" ActionTag="2099113190" Tag="7" IconVisible="False" LeftMargin="300.1719" RightMargin="137.8281" TopMargin="16.4432" BottomMargin="10.5568" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="360.1719" Y="70.5568" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6455" Y="0.4800" />
            <PreSize X="0.2151" Y="0.8163" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/System/Button_Audio.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/System/Option_X.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option_0_2" ActionTag="-392595350" Tag="6" IconVisible="False" LeftMargin="431.3226" RightMargin="6.6774" TopMargin="12.9995" BottomMargin="14.0005" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="90" Scale9Height="98" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="491.3226" Y="74.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8805" Y="0.5034" />
            <PreSize X="0.2151" Y="0.8163" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Option_Exit.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>