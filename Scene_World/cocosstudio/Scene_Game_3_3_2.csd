<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_3_3_2" ID="01d55356-14a3-4e82-85ec-0f25adc560ea" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="38" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Q332_back_1" ActionTag="894666011" Tag="1" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <Children>
              <AbstractNodeData Name="Quest_10" ActionTag="817473335" Tag="53" IconVisible="False" LeftMargin="1195.8689" RightMargin="193.1311" TopMargin="656.0522" BottomMargin="240.9478" ctype="SpriteObjectData">
                <Size X="531.0000" Y="183.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1461.3689" Y="332.4478" />
                <Scale ScaleX="0.5380" ScaleY="0.5380" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7611" Y="0.3078" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="Game_Resource/System/Quest.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/Q332_back.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Flour" ActionTag="-1135691193" Tag="701" IconVisible="False" LeftMargin="1385.5000" RightMargin="155.5000" TopMargin="130.0000" BottomMargin="570.0000" ctype="SpriteObjectData">
            <Size X="379.0000" Y="380.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1575.0000" Y="760.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8203" Y="0.7037" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/flour.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Egg" ActionTag="943080650" VisibleForFrame="False" Tag="702" IconVisible="False" LeftMargin="1385.5000" RightMargin="155.5000" TopMargin="130.0000" BottomMargin="570.0000" ctype="SpriteObjectData">
            <Size X="379.0000" Y="380.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1575.0000" Y="760.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8203" Y="0.7037" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/egg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Milk" ActionTag="2084716213" VisibleForFrame="False" Tag="703" IconVisible="False" LeftMargin="1385.5000" RightMargin="155.5000" TopMargin="130.0000" BottomMargin="570.0000" ctype="SpriteObjectData">
            <Size X="379.0000" Y="380.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1575.0000" Y="760.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8203" Y="0.7037" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/milk.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Num_10" ActionTag="-1278241255" Tag="711" IconVisible="False" LeftMargin="920.0000" RightMargin="842.0000" TopMargin="826.0000" BottomMargin="96.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="128" Scale9Height="136" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="158.0000" Y="158.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="999.0000" Y="175.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5203" Y="0.1620" />
            <PreSize X="0.0823" Y="0.1463" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/Q332.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Num_1" ActionTag="1739566" Tag="710" IconVisible="False" LeftMargin="1120.0004" RightMargin="641.9996" TopMargin="826.0000" BottomMargin="96.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="128" Scale9Height="136" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="158.0000" Y="158.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1199.0004" Y="175.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6245" Y="0.1620" />
            <PreSize X="0.0823" Y="0.1463" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/Q332.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Num_10" ActionTag="-2096053456" Tag="709" IconVisible="False" LeftMargin="972.3984" RightMargin="789.6016" TopMargin="853.3368" BottomMargin="68.6632" FontSize="100" IsCustomSize="True" LabelText="" PlaceHolderText="0" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="158.0000" Y="158.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1051.3984" Y="147.6632" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.5476" Y="0.1367" />
            <PreSize X="0.0823" Y="0.1463" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Num_1" ActionTag="-318626924" Tag="708" IconVisible="False" LeftMargin="1169.7617" RightMargin="592.2383" TopMargin="854.6402" BottomMargin="67.3598" FontSize="100" IsCustomSize="True" LabelText="" PlaceHolderText="0" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="158.0000" Y="158.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1248.7617" Y="146.3598" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.6504" Y="0.1355" />
            <PreSize X="0.0823" Y="0.1463" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Quest_Count" ActionTag="1513685497" Tag="604" IconVisible="False" LeftMargin="1634.0581" RightMargin="185.9419" TopMargin="698.5116" BottomMargin="281.4884" FontSize="100" IsCustomSize="True" LabelText="" PlaceHolderText="1" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1684.0581" Y="331.4884" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.8771" Y="0.3069" />
            <PreSize X="0.0521" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q3" ActionTag="-145311753" Tag="55" IconVisible="False" LeftMargin="1719.2307" RightMargin="60.7693" TopMargin="695.6002" BottomMargin="284.3998" FontSize="100" IsCustomSize="True" LabelText="" PlaceHolderText="/ 3" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="140.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1789.2307" Y="334.3998" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.9319" Y="0.3096" />
            <PreSize X="0.0729" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" ActionTag="1081858836" VisibleForFrame="False" Tag="605" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="960.0000" Y="540.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" ActionTag="686317963" VisibleForFrame="False" Tag="606" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="960.0000" Y="540.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Input_Num_1" ActionTag="-754264885" Tag="704" IconVisible="False" LeftMargin="214.1370" RightMargin="1585.8630" TopMargin="53.1431" BottomMargin="926.8569" FontSize="100" IsCustomSize="True" LabelText="1" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="120.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="274.1370" Y="976.8569" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.1428" Y="0.9045" />
            <PreSize X="0.0625" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Input_Num_2" ActionTag="1375182508" Tag="705" IconVisible="False" LeftMargin="353.2526" RightMargin="1446.7473" TopMargin="52.3816" BottomMargin="927.6183" FontSize="100" IsCustomSize="True" LabelText="2" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="120.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="413.2526" Y="977.6183" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.2152" Y="0.9052" />
            <PreSize X="0.0625" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Input_Num_3" ActionTag="-529904558" Tag="706" IconVisible="False" LeftMargin="494.5176" RightMargin="1305.4824" TopMargin="53.7747" BottomMargin="926.2254" FontSize="100" IsCustomSize="True" LabelText="3" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="120.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="554.5176" Y="976.2254" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.2888" Y="0.9039" />
            <PreSize X="0.0625" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Start" ActionTag="1122686041" Tag="5" IconVisible="False" LeftMargin="1418.5000" RightMargin="138.5000" TopMargin="922.5000" BottomMargin="42.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1600.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8333" Y="0.0926" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Start.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q_Num_1" ActionTag="-1338599320" Tag="712" IconVisible="False" LeftMargin="299.5363" RightMargin="1500.4637" TopMargin="456.7280" BottomMargin="523.2720" FontSize="100" IsCustomSize="True" LabelText="1" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="120.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="359.5363" Y="573.2720" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.1873" Y="0.5308" />
            <PreSize X="0.0625" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q_Num_2" ActionTag="1706865592" Tag="713" IconVisible="False" LeftMargin="655.2553" RightMargin="1144.7446" TopMargin="457.8152" BottomMargin="522.1848" FontSize="100" IsCustomSize="True" LabelText="2" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="120.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="715.2553" Y="572.1848" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.3725" Y="0.5298" />
            <PreSize X="0.0625" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q_Num_3" ActionTag="-1167389529" Tag="714" IconVisible="False" LeftMargin="990.8723" RightMargin="809.1277" TopMargin="455.5125" BottomMargin="524.4875" FontSize="100" IsCustomSize="True" LabelText="3" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="120.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1050.8723" Y="574.4875" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.5473" Y="0.5319" />
            <PreSize X="0.0625" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q1_square" ActionTag="872537094" Tag="715" IconVisible="False" LeftMargin="472.3533" RightMargin="1366.6467" TopMargin="466.8998" BottomMargin="531.1002" ctype="SpriteObjectData">
            <Size X="81.0000" Y="82.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="512.8533" Y="572.1002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2671" Y="0.5297" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/square.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q2_square" ActionTag="927142773" Tag="716" IconVisible="False" LeftMargin="472.3549" RightMargin="1366.6450" TopMargin="467.8978" BottomMargin="530.1022" ctype="SpriteObjectData">
            <Size X="81.0000" Y="82.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="512.8549" Y="571.1022" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2671" Y="0.5288" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/square.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q2_circle_2" ActionTag="-2062077740" Tag="717" IconVisible="False" LeftMargin="829.2102" RightMargin="1015.7898" TopMargin="469.8922" BottomMargin="532.1078" ctype="SpriteObjectData">
            <Size X="75.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="866.7102" Y="571.1078" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4514" Y="0.5288" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/circle_2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q3_circle_1" ActionTag="322417691" Tag="718" IconVisible="False" LeftMargin="469.1035" RightMargin="1365.8965" TopMargin="464.3655" BottomMargin="530.6345" ctype="SpriteObjectData">
            <Size X="85.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="511.6035" Y="573.1345" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2665" Y="0.5307" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/circle_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q3_diamond" ActionTag="-888137944" Tag="719" IconVisible="False" LeftMargin="828.4733" RightMargin="1013.5267" TopMargin="469.6519" BottomMargin="530.3481" ctype="SpriteObjectData">
            <Size X="78.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="867.4733" Y="570.3481" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4518" Y="0.5281" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/diamond.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="-946262120" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="687093523" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2_0" ActionTag="-902566165" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>