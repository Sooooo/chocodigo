<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_3_2" ID="c37498c9-324f-44b4-b09a-c4a78b390ae0" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="41" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="1979811282" IconVisible="False" LeftMargin="-0.3452" RightMargin="0.3452" TopMargin="2.0370" BottomMargin="-2.0370" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="959.6548" Y="537.9630" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4998" Y="0.4981" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/S3_q2_baclground.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_START" ActionTag="-1497141927" Tag="601" IconVisible="False" LeftMargin="1318.5000" RightMargin="238.5000" TopMargin="930.0000" BottomMargin="50.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1500.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7813" Y="0.0926" />
            <PreSize X="0.1891" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Play.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Question_1" ActionTag="-26181689" Tag="701" IconVisible="False" LeftMargin="235.5000" RightMargin="1013.5000" TopMargin="212.5000" BottomMargin="248.5000" ctype="SpriteObjectData">
            <Size X="671.0000" Y="619.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="571.0000" Y="558.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2974" Y="0.5167" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_0_0" ActionTag="686755365" Tag="711" IconVisible="False" LeftMargin="1215.0000" RightMargin="455.0000" TopMargin="285.0000" BottomMargin="545.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="220" Scale9Height="228" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1340.0000" Y="670.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6979" Y="0.6204" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1a12.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1a12.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1a1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_0_1" ActionTag="1639551623" Tag="712" IconVisible="False" LeftMargin="1520.0000" RightMargin="150.0000" TopMargin="285.0000" BottomMargin="545.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="250" Scale9Height="250" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1645.0000" Y="670.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8568" Y="0.6204" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1answer2.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1answer2.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1answer.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_0_2" ActionTag="793267713" Tag="714" IconVisible="False" LeftMargin="1519.9998" RightMargin="150.0002" TopMargin="587.9547" BottomMargin="242.0453" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="250" Scale9Height="250" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1644.9998" Y="367.0453" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8568" Y="0.3399" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1a22.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1a22.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1a2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_0_3" ActionTag="1292673754" Tag="713" IconVisible="False" LeftMargin="1212.0457" RightMargin="457.9543" TopMargin="585.0001" BottomMargin="244.9999" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="250" Scale9Height="250" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1337.0457" Y="369.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6964" Y="0.3426" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1a32.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1a32.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q1a3.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Question_2" ActionTag="-2114341427" VisibleForFrame="False" Tag="702" IconVisible="False" LeftMargin="235.5000" RightMargin="1013.5000" TopMargin="212.5000" BottomMargin="248.5000" ctype="SpriteObjectData">
            <Size X="671.0000" Y="619.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="571.0000" Y="558.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2974" Y="0.5167" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Question_3" ActionTag="-1956465928" VisibleForFrame="False" Tag="703" IconVisible="False" LeftMargin="235.5000" RightMargin="1013.5000" TopMargin="212.5000" BottomMargin="248.5000" ctype="SpriteObjectData">
            <Size X="671.0000" Y="619.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="571.0000" Y="558.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2974" Y="0.5167" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_2_0" ActionTag="126581642" VisibleForFrame="False" Tag="715" IconVisible="False" LeftMargin="1215.0000" RightMargin="455.0000" TopMargin="285.0000" BottomMargin="545.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="220" Scale9Height="228" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1340.0000" Y="670.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6979" Y="0.6204" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2answer2.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2answer2.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2answer.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_2_1" ActionTag="971440407" VisibleForFrame="False" Tag="716" IconVisible="False" LeftMargin="1520.0000" RightMargin="150.0000" TopMargin="285.0000" BottomMargin="545.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="250" Scale9Height="250" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1645.0000" Y="670.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8568" Y="0.6204" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2a12.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2a12.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2a1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_2_2" ActionTag="-1013065854" VisibleForFrame="False" Tag="718" IconVisible="False" LeftMargin="1520.0000" RightMargin="150.0000" TopMargin="587.9500" BottomMargin="242.0500" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="250" Scale9Height="250" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1645.0000" Y="367.0500" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8568" Y="0.3399" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2a32.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2a32.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2a3.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_2_3" ActionTag="-257239661" VisibleForFrame="False" Tag="717" IconVisible="False" LeftMargin="1212.0000" RightMargin="458.0000" TopMargin="585.0000" BottomMargin="245.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="250" Scale9Height="250" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1337.0000" Y="370.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6964" Y="0.3426" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2a22.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2a22.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q2a2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_3_0" ActionTag="1665484837" VisibleForFrame="False" Tag="719" IconVisible="False" LeftMargin="1215.0000" RightMargin="455.0000" TopMargin="285.0000" BottomMargin="545.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="220" Scale9Height="228" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1340.0000" Y="670.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6979" Y="0.6204" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3a12.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3a12.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3a1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_3_1" ActionTag="860123585" VisibleForFrame="False" Tag="720" IconVisible="False" LeftMargin="1520.0000" RightMargin="150.0000" TopMargin="285.0000" BottomMargin="545.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="250" Scale9Height="250" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1645.0000" Y="670.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8568" Y="0.6204" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3a22.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3a22.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3a2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_3_2" ActionTag="753549784" VisibleForFrame="False" Tag="722" IconVisible="False" LeftMargin="1520.0000" RightMargin="150.0000" TopMargin="587.9500" BottomMargin="242.0500" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="250" Scale9Height="250" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1645.0000" Y="367.0500" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8568" Y="0.3399" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3answer2.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3answer2.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3answer.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Ans_3_3" ActionTag="1113048639" VisibleForFrame="False" Tag="721" IconVisible="False" LeftMargin="1212.0000" RightMargin="458.0000" TopMargin="585.0000" BottomMargin="245.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="250" Scale9Height="250" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1337.0000" Y="370.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6964" Y="0.3426" />
            <PreSize X="0.1302" Y="0.2315" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3a32.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3a32.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_2/q3a3.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" ActionTag="1201220316" VisibleForFrame="False" Tag="606" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" ActionTag="-31649878" VisibleForFrame="False" Tag="605" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="1725133659" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="1071784439" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2_0" ActionTag="-1404907097" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>