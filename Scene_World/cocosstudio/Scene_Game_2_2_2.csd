<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_2_2_2" ID="a0440df5-69bf-4910-a906-2a1b86ff52c6" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="SceneGame_2_2" Tag="39" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="q2_bg_1" ActionTag="1143043718" IconVisible="False" LeftMargin="-1.3714" RightMargin="1.3713" TopMargin="1.3621" BottomMargin="-1.3621" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="958.6286" Y="538.6379" />
            <Scale ScaleX="1.0000" ScaleY="1.0093" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4993" Y="0.4987" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/2_SANTA/Game_2/q2_bg.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_0" ActionTag="-1209342359" Tag="602" IconVisible="False" LeftMargin="1418.5000" RightMargin="138.5000" TopMargin="922.5000" BottomMargin="42.5000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1600.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8333" Y="0.0926" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Play.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15" ActionTag="-803110623" Tag="701" IconVisible="False" LeftMargin="316.0646" RightMargin="1503.9354" TopMargin="133.8436" BottomMargin="919.1564" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="0" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="366.0646" Y="932.6564" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.1907" Y="0.8636" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_0" ActionTag="-1393310379" Tag="702" IconVisible="False" LeftMargin="316.9461" RightMargin="1503.0540" TopMargin="343.9438" BottomMargin="709.0562" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="0" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="366.9461" Y="722.5562" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.1911" Y="0.6690" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_1" ActionTag="2141698507" Tag="703" IconVisible="False" LeftMargin="315.0536" RightMargin="1504.9464" TopMargin="553.8198" BottomMargin="499.1802" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="0" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="365.0536" Y="512.6802" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.1901" Y="0.4747" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_2" ActionTag="-1861212393" Tag="704" IconVisible="False" LeftMargin="315.0527" RightMargin="1504.9473" TopMargin="753.4532" BottomMargin="299.5468" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="0" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="365.0527" Y="313.0468" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.1901" Y="0.2899" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_3" ActionTag="760076808" Tag="705" IconVisible="False" LeftMargin="768.0249" RightMargin="1051.9751" TopMargin="226.6566" BottomMargin="826.3434" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="818.0249" Y="839.8434" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.4261" Y="0.7776" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_4" ActionTag="269335682" Tag="706" IconVisible="False" LeftMargin="767.0250" RightMargin="1052.9750" TopMargin="433.7479" BottomMargin="619.2521" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="817.0250" Y="632.7521" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.4255" Y="0.5859" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_5" ActionTag="-1972892128" Tag="707" IconVisible="False" LeftMargin="767.1364" RightMargin="1052.8636" TopMargin="636.0565" BottomMargin="416.9435" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="817.1364" Y="430.4435" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.4256" Y="0.3986" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_6" ActionTag="1540507930" Tag="708" IconVisible="False" LeftMargin="1239.0690" RightMargin="580.9310" TopMargin="228.5477" BottomMargin="824.4523" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1289.0690" Y="837.9523" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.6714" Y="0.7759" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_7" ActionTag="1740975211" Tag="709" IconVisible="False" LeftMargin="1240.9622" RightMargin="579.0378" TopMargin="436.6376" BottomMargin="616.3624" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1290.9622" Y="629.8624" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.6724" Y="0.5832" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_8" ActionTag="-1848921294" Tag="710" IconVisible="False" LeftMargin="1239.0723" RightMargin="580.9277" TopMargin="637.1600" BottomMargin="415.8400" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1289.0723" Y="429.3400" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.6714" Y="0.3975" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_9" ActionTag="-953164790" Tag="711" IconVisible="False" LeftMargin="1685.5150" RightMargin="134.4850" TopMargin="132.0693" BottomMargin="920.9307" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1735.5150" Y="934.4307" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.9039" Y="0.8652" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_10" ActionTag="52903606" Tag="712" IconVisible="False" LeftMargin="1683.6261" RightMargin="136.3739" TopMargin="342.0508" BottomMargin="710.9492" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1733.6261" Y="724.4492" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.9029" Y="0.6708" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_11" ActionTag="68972544" Tag="713" IconVisible="False" LeftMargin="1682.6246" RightMargin="137.3754" TopMargin="547.2535" BottomMargin="505.7465" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1732.6246" Y="519.2465" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.9024" Y="0.4808" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_15_12" ActionTag="-386038415" Tag="714" IconVisible="False" LeftMargin="1681.7308" RightMargin="138.2692" TopMargin="747.8859" BottomMargin="305.1141" FontSize="65" IsCustomSize="True" LabelText="" PlaceHolderText="?" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1731.7308" Y="318.6141" />
            <Scale ScaleX="2.3621" ScaleY="2.1210" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.9019" Y="0.2950" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_3" ActionTag="-1372015013" Tag="715" IconVisible="False" LeftMargin="716.6170" RightMargin="1157.3831" TopMargin="259.9894" BottomMargin="784.0106" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="739.6170" Y="802.0106" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3852" Y="0.7426" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_4" ActionTag="20081547" Tag="716" IconVisible="False" LeftMargin="715.7281" RightMargin="1158.2720" TopMargin="469.0798" BottomMargin="574.9202" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="738.7281" Y="592.9202" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3848" Y="0.5490" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_5" ActionTag="-667835873" Tag="717" IconVisible="False" LeftMargin="718.5091" RightMargin="1155.4910" TopMargin="672.3900" BottomMargin="371.6100" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="741.5091" Y="389.6100" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3862" Y="0.3607" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_6" ActionTag="-1770524034" Tag="718" IconVisible="False" LeftMargin="1188.6621" RightMargin="685.3379" TopMargin="263.7721" BottomMargin="780.2279" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1211.6621" Y="798.2279" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6311" Y="0.7391" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_7" ActionTag="1440722942" Tag="719" IconVisible="False" LeftMargin="1189.5533" RightMargin="684.4467" TopMargin="469.9733" BottomMargin="574.0267" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1212.5533" Y="592.0267" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6315" Y="0.5482" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_8" ActionTag="-1247911676" Tag="720" IconVisible="False" LeftMargin="1188.5543" RightMargin="685.4457" TopMargin="672.3912" BottomMargin="371.6088" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1211.5543" Y="389.6088" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6310" Y="0.3607" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_9" ActionTag="1302117052" Tag="721" IconVisible="False" LeftMargin="1631.2156" RightMargin="242.7844" TopMargin="164.4002" BottomMargin="879.5998" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1654.2156" Y="897.5998" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8616" Y="0.8311" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_10" ActionTag="2117326462" Tag="722" IconVisible="False" LeftMargin="1634.1119" RightMargin="239.8881" TopMargin="373.4940" BottomMargin="670.5060" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1657.1119" Y="688.5060" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8631" Y="0.6375" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_10_0" ActionTag="1212017944" Tag="723" IconVisible="False" LeftMargin="1634.1107" RightMargin="239.8893" TopMargin="581.5864" BottomMargin="462.4136" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1657.1107" Y="480.4136" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8631" Y="0.4448" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_11_10_1" ActionTag="-784790978" Tag="724" IconVisible="False" LeftMargin="1632.2172" RightMargin="241.7828" TopMargin="782.1112" BottomMargin="261.8888" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1655.2172" Y="279.8888" />
            <Scale ScaleX="2.9740" ScaleY="3.7325" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8621" Y="0.2592" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" Visible="False" ActionTag="-1468770867" Tag="605" IconVisible="False" LeftMargin="935.9903" RightMargin="938.0097" TopMargin="521.7764" BottomMargin="522.2236" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="958.9903" Y="540.2236" />
            <Scale ScaleX="42.0374" ScaleY="29.8335" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4995" Y="0.5002" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" Visible="False" ActionTag="18391202" Tag="606" IconVisible="False" LeftMargin="936.3342" RightMargin="937.6658" TopMargin="521.5344" BottomMargin="522.4656" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="959.3342" Y="540.4656" />
            <Scale ScaleX="42.0374" ScaleY="29.8335" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4997" Y="0.5004" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2_0" ActionTag="1627355971" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="-1554266512" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="-812717100" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>