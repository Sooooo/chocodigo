<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_3_3_1" ID="a3f3da6e-f9d8-4a62-8d42-617ca49e7433" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="77" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="-578452870" Tag="701" IconVisible="False" LeftMargin="-2.1519" RightMargin="2.1519" BottomMargin="0.0000" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position X="-2.1519" Y="0.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0011" Y="0.0000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/q3_recipe.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_next" ActionTag="1530847557" Tag="703" IconVisible="False" LeftMargin="1297.1385" RightMargin="406.8615" TopMargin="835.0000" BottomMargin="115.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="186" Scale9Height="108" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="216.0000" Y="130.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1405.1385" Y="180.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7318" Y="0.1667" />
            <PreSize X="0.1125" Y="0.1204" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/button_next.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_back" ActionTag="706211086" Tag="3" IconVisible="False" LeftMargin="1518.5980" RightMargin="-45.5980" TopMargin="-21.6736" BottomMargin="683.6735" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="417" Scale9Height="396" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="447.0000" Y="418.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1742.0980" Y="892.6735" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9073" Y="0.8265" />
            <PreSize X="0.2328" Y="0.3870" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/button_recipe.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_front" ActionTag="313027312" VisibleForFrame="False" Tag="704" IconVisible="False" LeftMargin="286.8816" RightMargin="1417.1184" TopMargin="835.0000" BottomMargin="115.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="186" Scale9Height="108" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="216.0000" Y="130.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="394.8816" Y="180.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2057" Y="0.1667" />
            <PreSize X="0.1125" Y="0.1204" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_3/button_next_2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_11" ActionTag="1915716531" Tag="705" IconVisible="False" LeftMargin="348.0000" RightMargin="1122.0000" TopMargin="165.0000" BottomMargin="215.0000" FontSize="72" IsCustomSize="True" LabelText="레시피: ●&#xA;&#xA;1 ● 3 = 4&#xA;&#xA;2 ● 2 = 4&#xA;&#xA;15 ● 5 = 20&#xA;&#xA;8 ● 1 = 9" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="450.0000" Y="700.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="573.0000" Y="565.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.2984" Y="0.5231" />
            <PreSize X="0.2344" Y="0.6481" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_22" ActionTag="729352417" Tag="706" IconVisible="False" LeftMargin="991.0000" RightMargin="479.0000" TopMargin="165.0000" BottomMargin="215.0000" FontSize="72" IsCustomSize="True" LabelText="레시피: □&#xA;&#xA;4 □ 4 = 16&#xA;&#xA;7 □ 2 = 14&#xA;&#xA;5 □ 6 = 30&#xA;&#xA;2 □ 4 = 8" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="450.0000" Y="700.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1216.0000" Y="565.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.6333" Y="0.5231" />
            <PreSize X="0.2344" Y="0.6481" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_33" ActionTag="1278991694" VisibleForFrame="False" Tag="707" IconVisible="False" LeftMargin="348.0000" RightMargin="1122.0000" TopMargin="165.0000" BottomMargin="215.0000" FontSize="72" IsCustomSize="True" LabelText="레시피: ◆&#xA;&#xA;10 ◆ 2 = 5&#xA;&#xA;3 ◆ 2 = 1&#xA;&#xA;40 ◆ 5 = 8&#xA;&#xA;8 ◆ 3 = 2" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="450.0000" Y="700.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="573.0000" Y="565.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.2984" Y="0.5231" />
            <PreSize X="0.2344" Y="0.6481" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_44" ActionTag="-1264647253" VisibleForFrame="False" Tag="708" IconVisible="False" LeftMargin="991.0000" RightMargin="479.0000" TopMargin="165.0000" BottomMargin="215.0000" FontSize="72" IsCustomSize="True" LabelText="레시피: ◎&#xA;&#xA;2 ◎ 3 = 8&#xA;&#xA;3 ◎ 4 = 81&#xA;&#xA;4 ◎ 0 = 1&#xA;&#xA;10 ◎ 2 = 100" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="450.0000" Y="700.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1216.0000" Y="565.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.6333" Y="0.5231" />
            <PreSize X="0.2344" Y="0.6481" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>