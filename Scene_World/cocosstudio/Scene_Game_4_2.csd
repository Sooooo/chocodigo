<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_4_2" ID="42b13807-8e98-4dae-9b92-7a3383ca8379" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="40" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Background" ActionTag="1065078048" IconVisible="False" LeftMargin="-0.5461" RightMargin="0.5461" TopMargin="-1.7593" BottomMargin="1.7592" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="959.4539" Y="541.7592" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4997" Y="0.5016" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/background.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Chat" ActionTag="-1464551944" IconVisible="False" LeftMargin="139.7645" RightMargin="145.2355" TopMargin="595.0997" BottomMargin="183.9003" ctype="SpriteObjectData">
            <Size X="1635.0000" Y="301.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="957.2645" Y="334.4003" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4986" Y="0.3096" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/chat.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_1" ActionTag="-193963291" Tag="705" IconVisible="False" LeftMargin="144.0000" RightMargin="1384.0000" TopMargin="173.4998" BottomMargin="503.5002" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="362" Scale9Height="381" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="392.0000" Y="403.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="340.0000" Y="705.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1771" Y="0.6528" />
            <PreSize X="0.2042" Y="0.3731" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/people1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_2" ActionTag="1016595597" Tag="706" IconVisible="False" LeftMargin="551.0000" RightMargin="975.0000" TopMargin="173.4998" BottomMargin="503.5002" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="364" Scale9Height="381" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="394.0000" Y="403.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="748.0000" Y="705.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3896" Y="0.6528" />
            <PreSize X="0.2052" Y="0.3731" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/people2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_3" ActionTag="-1228982306" Tag="707" IconVisible="False" LeftMargin="964.2611" RightMargin="557.7389" TopMargin="178.9999" BottomMargin="505.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="368" Scale9Height="374" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="398.0000" Y="396.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1163.2611" Y="703.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6059" Y="0.6509" />
            <PreSize X="0.2073" Y="0.3667" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/people3.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_4" ActionTag="2068203084" Tag="708" IconVisible="False" LeftMargin="1373.8602" RightMargin="150.1398" TopMargin="171.9997" BottomMargin="504.0003" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="366" Scale9Height="382" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="396.0000" Y="404.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1571.8602" Y="706.0003" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8187" Y="0.6537" />
            <PreSize X="0.2062" Y="0.3741" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/people4.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Confirm" ActionTag="-1711359760" Tag="602" IconVisible="False" LeftMargin="1418.5000" RightMargin="138.5000" TopMargin="922.5000" BottomMargin="42.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1600.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8333" Y="0.0926" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Play.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Chat" ActionTag="248750342" Tag="709" IconVisible="False" LeftMargin="199.2303" RightMargin="220.7698" TopMargin="668.2187" BottomMargin="211.7813" TouchEnable="True" FontSize="50" IsCustomSize="True" LabelText="" PlaceHolderText="사건과 연관있는 사람이 누구인지 알기 위해 질문을 하였다. &#xA;그 중에서 세 사람은 거짓말을 하고, 한 사람만 참말을 하였다.&#xA;사람들의 말을 잘 들어보고 범인을 찾아라.&#xA;" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="1500.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="949.2303" Y="311.7813" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.4944" Y="0.2887" />
            <PreSize X="0.7813" Y="0.1852" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" Visible="False" ActionTag="2111390513" VisibleForFrame="False" Tag="606" IconVisible="False" LeftMargin="941.3339" RightMargin="932.6661" TopMargin="524.5339" BottomMargin="519.4661" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="964.3339" Y="537.4661" />
            <Scale ScaleX="42.0374" ScaleY="29.8335" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5023" Y="0.4977" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" Visible="False" ActionTag="1467202240" VisibleForFrame="False" Tag="605" IconVisible="False" LeftMargin="941.9897" RightMargin="932.0103" TopMargin="525.7748" BottomMargin="518.2252" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="964.9897" Y="536.2252" />
            <Scale ScaleX="42.0374" ScaleY="29.8335" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5026" Y="0.4965" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Check_1" ActionTag="-472509285" Tag="701" IconVisible="False" LeftMargin="303.5786" RightMargin="1493.4214" TopMargin="36.0714" BottomMargin="935.9286" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="123.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="365.0786" Y="989.9286" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1901" Y="0.9166" />
            <PreSize X="0.0641" Y="0.1000" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button_2.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Check_2" ActionTag="-315685095" Tag="702" IconVisible="False" LeftMargin="707.9476" RightMargin="1089.0525" TopMargin="37.2449" BottomMargin="934.7552" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="123.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="769.4476" Y="988.7552" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4008" Y="0.9155" />
            <PreSize X="0.0641" Y="0.1000" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button_2.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Check_3" ActionTag="-490975733" Tag="703" IconVisible="False" LeftMargin="1121.6730" RightMargin="675.3270" TopMargin="37.2582" BottomMargin="934.7418" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="123.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1183.1730" Y="988.7418" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6162" Y="0.9155" />
            <PreSize X="0.0641" Y="0.1000" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button_2.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Check_4" ActionTag="-1971082075" Tag="704" IconVisible="False" LeftMargin="1532.3595" RightMargin="264.6405" TopMargin="34.3662" BottomMargin="937.6337" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="123.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1593.8595" Y="991.6337" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8301" Y="0.9182" />
            <PreSize X="0.0641" Y="0.1000" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_2/button_2.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="140527430" Tag="37" IconVisible="False" LeftMargin="296.0585" RightMargin="1535.9415" TopMargin="511.7270" BottomMargin="518.2730" FontSize="50" LabelText="가영" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="88.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="340.0585" Y="543.2730" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.1771" Y="0.5030" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="255" B="255" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_0" ActionTag="-88813313" Tag="38" IconVisible="False" LeftMargin="709.3812" RightMargin="1122.6188" TopMargin="512.5879" BottomMargin="517.4121" FontSize="50" LabelText="나영" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="88.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="753.3812" Y="542.4121" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.3924" Y="0.5022" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="255" B="255" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_1" ActionTag="-2057127531" Tag="39" IconVisible="False" LeftMargin="1120.4233" RightMargin="711.5767" TopMargin="511.7270" BottomMargin="518.2730" FontSize="50" LabelText="다영" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="88.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1164.4233" Y="543.2730" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.6065" Y="0.5030" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="255" B="255" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_2" ActionTag="582872227" Tag="40" IconVisible="False" LeftMargin="1533.1906" RightMargin="298.8094" TopMargin="515.5882" BottomMargin="514.4118" FontSize="50" LabelText="라영" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="88.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1577.1906" Y="539.4118" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.8215" Y="0.4995" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="255" B="255" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="454291728" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="-2013553319" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2_0" ActionTag="-159474381" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>