<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_5_3" ID="def08bd5-268e-43a8-b955-40e6e0cf93da" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="580500846" Tag="1" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/background.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_start" ActionTag="875388492" Tag="601" IconVisible="False" LeftMargin="1418.5000" RightMargin="138.5000" TopMargin="922.5000" BottomMargin="42.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1600.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8333" Y="0.0926" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Play.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_hint" ActionTag="-1387445196" Tag="701" IconVisible="False" LeftMargin="1572.7424" RightMargin="-7.7424" TopMargin="3.5251" BottomMargin="835.4749" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="325" Scale9Height="219" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="355.0000" Y="241.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1750.2424" Y="955.9749" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9116" Y="0.8852" />
            <PreSize X="0.1849" Y="0.2231" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/hintbutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_HELLOWORLD" ActionTag="-1149010560" Tag="704" IconVisible="False" LeftMargin="65.3982" RightMargin="1204.6018" TopMargin="355.5528" BottomMargin="604.4472" TouchEnable="True" FontSize="100" IsCustomSize="True" LabelText="HELLOWORLD" PlaceHolderText="" MaxLengthEnable="True" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="650.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="390.3982" Y="664.4472" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2033" Y="0.6152" />
            <PreSize X="0.3385" Y="0.1111" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_A" ActionTag="-2114898458" Tag="710" IconVisible="False" LeftMargin="838.7508" RightMargin="951.2492" TopMargin="297.6516" BottomMargin="663.3484" TouchEnable="True" FontSize="68" ButtonText="A" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="903.7508" Y="722.8484" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4707" Y="0.6693" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_B" ActionTag="-726164178" Tag="711" IconVisible="False" LeftMargin="988.7483" RightMargin="801.2517" TopMargin="297.6506" BottomMargin="663.3494" TouchEnable="True" FontSize="68" ButtonText="B" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1053.7483" Y="722.8494" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5488" Y="0.6693" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_C" ActionTag="-1226703111" Tag="712" IconVisible="False" LeftMargin="1138.7529" RightMargin="651.2471" TopMargin="297.6536" BottomMargin="663.3464" TouchEnable="True" FontSize="68" ButtonText="C" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1203.7529" Y="722.8464" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6270" Y="0.6693" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_D" ActionTag="-1302465593" Tag="713" IconVisible="False" LeftMargin="1288.7471" RightMargin="501.2529" TopMargin="297.6556" BottomMargin="663.3444" TouchEnable="True" FontSize="68" ButtonText="D" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1353.7471" Y="722.8444" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7051" Y="0.6693" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_E" ActionTag="-1433174696" Tag="714" IconVisible="False" LeftMargin="1438.7407" RightMargin="351.2593" TopMargin="297.6576" BottomMargin="663.3424" TouchEnable="True" FontSize="68" ButtonText="E" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1503.7407" Y="722.8424" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7832" Y="0.6693" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_F" ActionTag="-1615579727" Tag="715" IconVisible="False" LeftMargin="1588.7419" RightMargin="201.2581" TopMargin="297.6595" BottomMargin="663.3405" TouchEnable="True" FontSize="68" ButtonText="F" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1653.7419" Y="722.8405" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8613" Y="0.6693" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_G" ActionTag="-228293072" Tag="716" IconVisible="False" LeftMargin="1738.7305" RightMargin="51.2695" TopMargin="297.6606" BottomMargin="663.3394" TouchEnable="True" FontSize="68" ButtonText="G" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1803.7305" Y="722.8394" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9394" Y="0.6693" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_H" ActionTag="2017484724" Tag="717" IconVisible="False" LeftMargin="838.4665" RightMargin="951.5335" TopMargin="437.0070" BottomMargin="523.9930" TouchEnable="True" FontSize="68" ButtonText="H" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="903.4665" Y="583.4930" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4706" Y="0.5403" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_I" ActionTag="1934165926" Tag="718" IconVisible="False" LeftMargin="988.4666" RightMargin="801.5334" TopMargin="437.0070" BottomMargin="523.9930" TouchEnable="True" FontSize="68" ButtonText="I" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1053.4666" Y="583.4930" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5487" Y="0.5403" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_J" ActionTag="-965029043" Tag="719" IconVisible="False" LeftMargin="1138.7485" RightMargin="651.2515" TopMargin="436.6650" BottomMargin="524.3350" TouchEnable="True" FontSize="68" ButtonText="J" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1203.7485" Y="583.8350" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6270" Y="0.5406" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_K" ActionTag="-365297269" Tag="720" IconVisible="False" LeftMargin="1288.7427" RightMargin="501.2573" TopMargin="436.6689" BottomMargin="524.3311" TouchEnable="True" FontSize="68" ButtonText="K" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1353.7427" Y="583.8311" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7051" Y="0.5406" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_L" ActionTag="-954301844" Tag="721" IconVisible="False" LeftMargin="1438.7368" RightMargin="351.2632" TopMargin="436.6699" BottomMargin="524.3301" TouchEnable="True" FontSize="68" ButtonText="L" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1503.7368" Y="583.8301" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7832" Y="0.5406" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_M" ActionTag="1369499847" Tag="722" IconVisible="False" LeftMargin="1588.7380" RightMargin="201.2620" TopMargin="436.6729" BottomMargin="524.3271" TouchEnable="True" FontSize="68" ButtonText="M" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1653.7380" Y="583.8271" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8613" Y="0.5406" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_N" ActionTag="32780651" Tag="723" IconVisible="False" LeftMargin="1738.7261" RightMargin="51.2739" TopMargin="436.6729" BottomMargin="524.3271" TouchEnable="True" FontSize="68" ButtonText="N" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1803.7261" Y="583.8271" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9394" Y="0.5406" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O_" ActionTag="-652368674" Tag="724" IconVisible="False" LeftMargin="838.4665" RightMargin="951.5335" TopMargin="576.0070" BottomMargin="384.9930" TouchEnable="True" FontSize="68" ButtonText="O" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="903.4665" Y="444.4930" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4706" Y="0.4116" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_P" ActionTag="1373463445" Tag="725" IconVisible="False" LeftMargin="988.4624" RightMargin="801.5376" TopMargin="576.0070" BottomMargin="384.9930" TouchEnable="True" FontSize="68" ButtonText="P" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1053.4624" Y="444.4930" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5487" Y="0.4116" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Q" ActionTag="-335831285" Tag="726" IconVisible="False" LeftMargin="1138.7485" RightMargin="651.2515" TopMargin="575.6647" BottomMargin="385.3353" TouchEnable="True" FontSize="68" ButtonText="Q" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1203.7485" Y="444.8353" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6270" Y="0.4119" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_R" ActionTag="1116424894" Tag="727" IconVisible="False" LeftMargin="1288.7427" RightMargin="501.2573" TopMargin="575.6669" BottomMargin="385.3331" TouchEnable="True" FontSize="68" ButtonText="R" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1353.7427" Y="444.8331" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7051" Y="0.4119" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_S" ActionTag="-1159996170" Tag="728" IconVisible="False" LeftMargin="1438.7368" RightMargin="351.2632" TopMargin="575.6678" BottomMargin="385.3322" TouchEnable="True" FontSize="68" ButtonText="S" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1503.7368" Y="444.8322" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7832" Y="0.4119" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_T" ActionTag="15319401" Tag="729" IconVisible="False" LeftMargin="1588.7380" RightMargin="201.2620" TopMargin="575.6697" BottomMargin="385.3303" TouchEnable="True" FontSize="68" ButtonText="T" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1653.7380" Y="444.8303" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8613" Y="0.4119" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_U" ActionTag="-1517209550" Tag="730" IconVisible="False" LeftMargin="1738.7261" RightMargin="51.2739" TopMargin="575.6708" BottomMargin="385.3292" TouchEnable="True" FontSize="68" ButtonText="U" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1803.7261" Y="444.8292" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9394" Y="0.4119" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_V" ActionTag="-1043146439" Tag="731" IconVisible="False" LeftMargin="838.4704" RightMargin="951.5296" TopMargin="715.0041" BottomMargin="245.9959" TouchEnable="True" FontSize="68" ButtonText="V" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="903.4704" Y="305.4959" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4706" Y="0.2829" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_W" ActionTag="1895994870" Tag="732" IconVisible="False" LeftMargin="988.4666" RightMargin="801.5334" TopMargin="715.0041" BottomMargin="245.9959" TouchEnable="True" FontSize="68" ButtonText="W" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1053.4666" Y="305.4959" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5487" Y="0.2829" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X_" ActionTag="902485866" Tag="733" IconVisible="False" LeftMargin="1138.7485" RightMargin="651.2515" TopMargin="714.6611" BottomMargin="246.3389" TouchEnable="True" FontSize="68" ButtonText="X" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1203.7485" Y="305.8389" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6270" Y="0.2832" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Y" ActionTag="-1170879346" Tag="734" IconVisible="False" LeftMargin="1288.7446" RightMargin="501.2554" TopMargin="714.6628" BottomMargin="246.3372" TouchEnable="True" FontSize="68" ButtonText="Y" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1353.7446" Y="305.8372" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7051" Y="0.2832" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Z" ActionTag="-1813702794" Tag="735" IconVisible="False" LeftMargin="1438.7368" RightMargin="351.2632" TopMargin="714.6649" BottomMargin="246.3351" TouchEnable="True" FontSize="68" ButtonText="Z" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1503.7368" Y="305.8351" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7832" Y="0.2832" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_del" ActionTag="-679314306" Tag="736" IconVisible="False" LeftMargin="1588.7400" RightMargin="201.2600" TopMargin="714.6700" BottomMargin="246.3300" TouchEnable="True" FontSize="68" ButtonText="←" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="100" Scale9Height="97" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="130.0000" Y="119.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1653.7400" Y="305.8300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8613" Y="0.2832" />
            <PreSize X="0.0677" Y="0.1102" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/keybutton.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Answer" ActionTag="-622427220" Tag="705" IconVisible="False" LeftMargin="65.3983" RightMargin="1204.6017" TopMargin="659.5536" BottomMargin="300.4464" TouchEnable="True" FontSize="100" IsCustomSize="True" LabelText="HELLOWORLD" PlaceHolderText="" MaxLengthEnable="True" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="650.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="390.3983" Y="360.4464" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2033" Y="0.3337" />
            <PreSize X="0.3385" Y="0.1111" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_hint" ActionTag="-1868712855" VisibleForFrame="False" Tag="702" IconVisible="False" LeftMargin="477.2258" RightMargin="475.7742" TopMargin="262.5889" BottomMargin="219.4111" ctype="SpriteObjectData">
            <Size X="967.0000" Y="598.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="960.7258" Y="518.4111" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5004" Y="0.4800" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/5_SEA/Game_3/hint.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_hint" ActionTag="-1368081065" VisibleForFrame="False" Tag="703" IconVisible="False" LeftMargin="903.5513" RightMargin="916.4487" TopMargin="565.4020" BottomMargin="414.5980" TouchEnable="True" FontSize="100" IsCustomSize="True" LabelText="0" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="953.5513" Y="464.5980" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.4966" Y="0.4302" />
            <PreSize X="0.0521" Y="0.0926" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_start_0" ActionTag="-2121360620" Tag="603" IconVisible="False" LeftMargin="1018.5000" RightMargin="538.5000" TopMargin="922.5000" BottomMargin="42.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1200.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6250" Y="0.0926" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Reset.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" ActionTag="434718648" VisibleForFrame="False" Tag="605" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" ActionTag="-128399776" VisibleForFrame="False" Tag="606" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="508418391" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="287245670" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2_0" ActionTag="-2013458093" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>