<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_6_1_2" ID="02416f80-02c5-4f2f-a8f1-3673e381ee14" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="62" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Backgorund" ActionTag="449072849" Tag="1" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_background.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_play" ActionTag="1575717947" Tag="601" IconVisible="False" LeftMargin="1418.5000" RightMargin="138.5000" TopMargin="922.5000" BottomMargin="42.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1600.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8333" Y="0.0926" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Start.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="A1" ActionTag="1830831083" Tag="701" IconVisible="False" LeftMargin="538.7100" RightMargin="963.2900" TopMargin="229.4693" BottomMargin="432.5307" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="418.0000" Y="418.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="747.7100" Y="641.5307" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3894" Y="0.5940" />
            <PreSize X="0.2177" Y="0.3870" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a1.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a1.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a1_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="A2" ActionTag="1139768312" Tag="702" IconVisible="False" LeftMargin="991.8174" RightMargin="510.1826" TopMargin="97.4424" BottomMargin="564.5576" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="418.0000" Y="418.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1200.8174" Y="773.5576" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6254" Y="0.7163" />
            <PreSize X="0.2177" Y="0.3870" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a2.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a2.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a2.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a2_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="A3" ActionTag="1830364003" Tag="703" IconVisible="False" LeftMargin="877.1947" RightMargin="624.8053" TopMargin="565.7971" BottomMargin="96.2029" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="418.0000" Y="418.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1086.1947" Y="305.2029" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5657" Y="0.2826" />
            <PreSize X="0.2177" Y="0.3870" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a3.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a3.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a3.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a3_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="A4" ActionTag="-305193623" Tag="704" IconVisible="False" LeftMargin="1336.4457" RightMargin="165.5543" TopMargin="423.4182" BottomMargin="238.5818" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="418.0000" Y="418.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1545.4457" Y="447.5818" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8049" Y="0.4144" />
            <PreSize X="0.2177" Y="0.3870" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a4.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a4.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a4.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_1/2_a4_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="X" ActionTag="368701693" VisibleForFrame="False" Tag="606" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="O" ActionTag="-1278817649" VisibleForFrame="False" Tag="605" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="-270909648" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="938686856" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2" ActionTag="718002668" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>