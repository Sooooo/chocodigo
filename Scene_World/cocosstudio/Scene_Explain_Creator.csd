<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Explain_Creator" ID="79284792-69ce-4bd3-8b30-bda9a127dc7e" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="23" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Button_4" ActionTag="-683249182" Tag="701" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="14" RightEage="14" TopEage="4" BottomEage="4" Scale9OriginX="-14" Scale9OriginY="-4" Scale9Width="28" Scale9Height="8" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1" ActionTag="1439580483" Tag="702" IconVisible="False" LeftMargin="1600.0000" RightMargin="229.0000" TopMargin="12.0000" BottomMargin="965.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="61" Scale9Height="81" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="91.0000" Y="103.0000" />
            <AnchorPoint />
            <Position X="1600.0000" Y="965.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8333" Y="0.8935" />
            <PreSize X="0.0474" Y="0.0954" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/QuestExplain/button2.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/QuestExplain/button.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_0" ActionTag="56528472" Tag="703" RotationSkewY="180.0000" IconVisible="False" LeftMargin="1830.0000" RightMargin="-1.0000" TopMargin="12.0000" BottomMargin="965.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="61" Scale9Height="81" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="91.0000" Y="103.0000" />
            <AnchorPoint />
            <Position X="1830.0000" Y="965.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9531" Y="0.8935" />
            <PreSize X="0.0474" Y="0.0954" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/QuestExplain/button2.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/QuestExplain/button.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>