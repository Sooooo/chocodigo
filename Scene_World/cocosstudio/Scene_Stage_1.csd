<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Stage_1" ID="945da6c1-f677-4956-b22b-9e3ed6e31aa2" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="SceneStage_1" Tag="19" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_Background" ActionTag="820413973" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/stage_1_backgrond.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BGM" ActionTag="-1256033556" Tag="100" IconVisible="True" LeftMargin="104.9949" RightMargin="1815.0051" TopMargin="1008.3323" BottomMargin="71.6677" Volume="1.0000" Loop="True" ctype="SimpleAudioObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="104.9949" Y="71.6677" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0547" Y="0.0664" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__1" ActionTag="-1153700649" Tag="201" IconVisible="False" LeftMargin="169.4713" RightMargin="1238.5287" TopMargin="65.9417" BottomMargin="335.0583" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="482" Scale9Height="657" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="512.0000" Y="679.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="425.4713" Y="674.5583" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2216" Y="0.6246" />
            <PreSize X="0.2667" Y="0.6287" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/stage1_button0.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__2" ActionTag="1943763673" Tag="202" IconVisible="False" LeftMargin="891.2245" RightMargin="680.7755" TopMargin="590.1146" BottomMargin="137.8854" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="318" Scale9Height="330" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="348.0000" Y="352.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1065.2245" Y="313.8854" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5548" Y="0.2906" />
            <PreSize X="0.1813" Y="0.3259" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/stage1_button1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__3" ActionTag="-104528893" Tag="203" IconVisible="False" LeftMargin="1127.9388" RightMargin="455.0612" TopMargin="327.3323" BottomMargin="473.6677" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="307" Scale9Height="257" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="337.0000" Y="279.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1296.4388" Y="613.1677" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6752" Y="0.5677" />
            <PreSize X="0.1755" Y="0.2583" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/stage1_button2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button__4" ActionTag="92037341" Tag="204" IconVisible="False" LeftMargin="1546.3014" RightMargin="90.6986" TopMargin="184.3992" BottomMargin="545.6008" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="253" Scale9Height="328" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="283.0000" Y="350.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1687.8014" Y="720.6008" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8791" Y="0.6672" />
            <PreSize X="0.1474" Y="0.3241" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/stage1_button3.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1" ActionTag="-75161552" VisibleForFrame="False" Tag="251" IconVisible="False" LeftMargin="439.1544" RightMargin="1166.8457" TopMargin="133.5935" BottomMargin="748.4065" ctype="SpriteObjectData">
            <Size X="314.0000" Y="198.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="596.1544" Y="847.4065" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3105" Y="0.7846" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/System/notice.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1_0" ActionTag="193421984" VisibleForFrame="False" Tag="252" IconVisible="False" LeftMargin="860.2999" RightMargin="745.7002" TopMargin="515.9037" BottomMargin="366.0963" ctype="SpriteObjectData">
            <Size X="314.0000" Y="198.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1017.2999" Y="465.0963" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5298" Y="0.4306" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/System/notice.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1_0_0" ActionTag="-1734382568" VisibleForFrame="False" Tag="253" IconVisible="False" LeftMargin="1203.7817" RightMargin="402.2183" TopMargin="244.9568" BottomMargin="637.0432" ctype="SpriteObjectData">
            <Size X="314.0000" Y="198.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1360.7817" Y="736.0432" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7087" Y="0.6815" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/System/notice.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1_0_1" ActionTag="1568262071" VisibleForFrame="False" Tag="254" IconVisible="False" LeftMargin="1571.5249" RightMargin="34.4751" TopMargin="135.3195" BottomMargin="746.6805" ctype="SpriteObjectData">
            <Size X="314.0000" Y="198.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1728.5249" Y="845.6805" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9003" Y="0.7830" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/System/notice.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="-1087296121" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="-549805776" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2" Visible="False" ActionTag="-645850319" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1" ActionTag="915842955" Tag="10" IconVisible="False" LeftMargin="1737.5000" RightMargin="57.5000" TopMargin="42.5000" BottomMargin="922.5000" TouchEnable="True" FontSize="50" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="95" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="125.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1800.0000" Y="980.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9375" Y="0.9074" />
            <PreSize X="0.0651" Y="0.1065" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanM_0.ttf" Plist="" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/button_info.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>