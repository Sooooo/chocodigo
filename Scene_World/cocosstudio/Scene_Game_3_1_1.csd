<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_3_1_1" ID="3f93de43-b558-4048-ad35-985cd64e3fed" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="120" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="q3_bg_1" ActionTag="-1621499660" IconVisible="False" LeftMargin="7.7886" RightMargin="-7.7886" TopMargin="-95.6597" BottomMargin="-104.3403" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1280.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="967.7886" Y="535.6597" />
            <Scale ScaleX="1.0077" ScaleY="0.8572" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5041" Y="0.4960" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_1/Background.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Rabbit" ActionTag="759458506" Tag="701" IconVisible="False" LeftMargin="187.7356" RightMargin="1527.2644" TopMargin="127.8231" BottomMargin="726.1769" ctype="SpriteObjectData">
            <Size X="205.0000" Y="226.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="290.2356" Y="839.1769" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1512" Y="0.7770" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_1/Rabbit.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Cat" ActionTag="1346819878" Tag="702" IconVisible="False" LeftMargin="187.7356" RightMargin="1527.2644" TopMargin="143.3221" BottomMargin="741.6779" ctype="SpriteObjectData">
            <Size X="205.0000" Y="195.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="290.2356" Y="839.1779" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1512" Y="0.7770" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_1/Cat.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Reject" ActionTag="963624670" Tag="703" IconVisible="False" LeftMargin="1459.2659" RightMargin="4.7341" TopMargin="132.2727" BottomMargin="528.7273" ctype="SpriteObjectData">
            <Size X="456.0000" Y="419.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1687.2659" Y="738.2273" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8788" Y="0.6835" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_1/Reject.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Pass" ActionTag="1495156388" Tag="704" IconVisible="False" LeftMargin="1463.2693" RightMargin="8.7307" TopMargin="113.7708" BottomMargin="510.2292" ctype="SpriteObjectData">
            <Size X="448.0000" Y="456.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1687.2693" Y="738.2292" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8788" Y="0.6835" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/3_TOY/Game_1/Pass.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Reject" ActionTag="-1293675262" Tag="705" IconVisible="False" LeftMargin="1131.5740" RightMargin="583.4260" TopMargin="780.8323" BottomMargin="84.1677" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="175" Scale9Height="193" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="205.0000" Y="215.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1234.0740" Y="191.6677" />
            <Scale ScaleX="1.0065" ScaleY="0.8549" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6427" Y="0.1775" />
            <PreSize X="0.1068" Y="0.1991" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_1/Button_Reject.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_1/Button_Reject.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Pass" ActionTag="-743502467" Tag="706" IconVisible="False" LeftMargin="1513.2168" RightMargin="202.7832" TopMargin="785.6632" BottomMargin="79.3368" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="193" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="204.0000" Y="215.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1615.2168" Y="186.8368" />
            <Scale ScaleX="1.0065" ScaleY="0.8549" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8413" Y="0.1730" />
            <PreSize X="0.1063" Y="0.1991" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="Game_Resource/3_TOY/Game_1/Button_Pass.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/3_TOY/Game_1/Button_Pass.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField" ActionTag="1841650228" Tag="710" IconVisible="False" LeftMargin="815.9899" RightMargin="754.0100" TopMargin="548.9009" BottomMargin="431.0991" FontSize="100" IsCustomSize="True" LabelText="010101010" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="350.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="990.9899" Y="481.0991" />
            <Scale ScaleX="2.2300" ScaleY="1.8875" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.5161" Y="0.4455" />
            <PreSize X="0.1823" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanM_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" Visible="False" ActionTag="-293690205" Tag="605" IconVisible="False" LeftMargin="941.9897" RightMargin="932.0103" TopMargin="525.7748" BottomMargin="518.2252" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="964.9897" Y="536.2252" />
            <Scale ScaleX="42.0374" ScaleY="29.8335" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5026" Y="0.4965" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" Visible="False" ActionTag="-1155529513" Tag="606" IconVisible="False" LeftMargin="941.3339" RightMargin="932.6661" TopMargin="524.5339" BottomMargin="519.4661" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="964.3339" Y="537.4661" />
            <Scale ScaleX="42.0374" ScaleY="29.8335" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5023" Y="0.4977" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Q_Title" ActionTag="-1625654337" IconVisible="False" LeftMargin="24.6909" RightMargin="1364.3091" TopMargin="892.1381" BottomMargin="4.8619" ctype="SpriteObjectData">
            <Size X="531.0000" Y="183.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="290.1909" Y="96.3619" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1511" Y="0.0892" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/System/Quest.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="TotalNum" ActionTag="-467719951" IconVisible="False" LeftMargin="831.7762" RightMargin="988.2238" TopMargin="966.7029" BottomMargin="86.2971" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="/ 3" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="881.7762" Y="99.7971" />
            <Scale ScaleX="3.3200" ScaleY="3.6667" />
            <CColor A="255" R="77" G="77" B="77" />
            <PrePosition X="0.4593" Y="0.0924" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CurrentNum" ActionTag="-528194565" Tag="604" IconVisible="False" LeftMargin="731.7775" RightMargin="1088.2224" TopMargin="966.7029" BottomMargin="86.2971" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="1" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="781.7775" Y="99.7971" />
            <Scale ScaleX="3.3200" ScaleY="3.6667" />
            <CColor A="255" R="77" G="77" B="77" />
            <PrePosition X="0.4072" Y="0.0924" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Confirm" ActionTag="-2074041684" Tag="602" IconVisible="False" LeftMargin="1730.4125" RightMargin="-11.4125" TopMargin="836.9962" BottomMargin="96.0038" TouchEnable="True" FontSize="100" ButtonText="확인" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="171" Scale9Height="125" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="201.0000" Y="147.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1830.9125" Y="169.5038" />
            <Scale ScaleX="0.7560" ScaleY="0.9187" />
            <CColor A="255" R="230" G="230" B="250" />
            <PrePosition X="0.9536" Y="0.1569" />
            <PreSize X="0.1047" Y="0.1361" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_SelectAttribute.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="1850929302" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="821416337" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2_0" ActionTag="456933791" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>