<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_4_1" ID="67e86382-a168-4576-b965-8f2140b2feb4" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="27" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="157267951" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/4_FAIRYTAIL/Game_1/background.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_START" ActionTag="-1065170826" Tag="601" IconVisible="False" LeftMargin="1373.5000" RightMargin="183.5000" TopMargin="922.5000" BottomMargin="42.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1555.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8099" Y="0.0926" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Play.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Green" ActionTag="502823059" Tag="701" RotationSkewX="-6.3126" RotationSkewY="-6.3103" IconVisible="False" LeftMargin="152.7194" RightMargin="1387.2806" TopMargin="154.3342" BottomMargin="675.6658" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="test" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="380.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="342.7194" Y="800.6658" />
            <Scale ScaleX="0.9312" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.1785" Y="0.7414" />
            <PreSize X="0.1979" Y="0.2315" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Yellow" ActionTag="1276234009" Tag="702" RotationSkewX="-3.4151" RotationSkewY="-3.4179" IconVisible="False" LeftMargin="715.2083" RightMargin="824.7917" TopMargin="229.3259" BottomMargin="600.6741" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="test" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="380.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="905.2083" Y="725.6741" />
            <Scale ScaleX="0.9296" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.4715" Y="0.6719" />
            <PreSize X="0.1979" Y="0.2315" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Red" ActionTag="-2140079357" Tag="703" RotationSkewX="0.0629" RotationSkewY="0.0655" IconVisible="False" LeftMargin="104.7644" RightMargin="1435.2356" TopMargin="611.5895" BottomMargin="218.4106" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="test" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="380.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="294.7644" Y="343.4106" />
            <Scale ScaleX="0.9078" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.1535" Y="0.3180" />
            <PreSize X="0.1979" Y="0.2315" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Blue" ActionTag="-384439055" Tag="704" RotationSkewX="0.0629" RotationSkewY="0.0657" IconVisible="False" LeftMargin="652.4795" RightMargin="887.5205" TopMargin="689.5428" BottomMargin="140.4572" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="test" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="380.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="842.4795" Y="265.4572" />
            <Scale ScaleX="0.9308" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.4388" Y="0.2458" />
            <PreSize X="0.1979" Y="0.2315" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanEB_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Green_Answer" ActionTag="2063762681" Tag="711" IconVisible="False" LeftMargin="1536.6017" RightMargin="283.3983" TopMargin="220.4711" BottomMargin="759.5289" FontSize="72" IsCustomSize="True" LabelText="" PlaceHolderText="1" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1586.6017" Y="809.5289" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.8264" Y="0.7496" />
            <PreSize X="0.0521" Y="0.0926" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Yellow_Answer" ActionTag="-1983456028" Tag="712" IconVisible="False" LeftMargin="1536.5997" RightMargin="283.4003" TopMargin="359.4224" BottomMargin="620.5776" FontSize="72" IsCustomSize="True" LabelText="" PlaceHolderText="1" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1586.5997" Y="670.5776" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.8264" Y="0.6209" />
            <PreSize X="0.0521" Y="0.0926" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Red_Answer" ActionTag="-54545668" Tag="713" IconVisible="False" LeftMargin="1536.5997" RightMargin="283.4003" TopMargin="498.3276" BottomMargin="481.6724" FontSize="72" IsCustomSize="True" LabelText="" PlaceHolderText="1" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1586.5997" Y="531.6724" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.8264" Y="0.4923" />
            <PreSize X="0.0521" Y="0.0926" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_Blue_Answer" ActionTag="-2106199910" Tag="714" IconVisible="False" LeftMargin="1536.5997" RightMargin="283.4003" TopMargin="636.9628" BottomMargin="343.0372" FontSize="72" IsCustomSize="True" LabelText="" PlaceHolderText="1" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1586.5997" Y="393.0372" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.8264" Y="0.3639" />
            <PreSize X="0.0521" Y="0.0926" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Green" ActionTag="-609371017" Tag="721" IconVisible="False" LeftMargin="1510.0000" RightMargin="310.0000" TopMargin="202.7442" BottomMargin="777.2558" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="14" RightEage="14" TopEage="4" BottomEage="4" Scale9OriginX="-14" Scale9OriginY="-4" Scale9Width="28" Scale9Height="8" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1560.0000" Y="827.2558" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8125" Y="0.7660" />
            <PreSize X="0.0521" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Yellow" ActionTag="-2135486237" Tag="722" IconVisible="False" LeftMargin="1510.0000" RightMargin="310.0000" TopMargin="342.2399" BottomMargin="637.7601" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="14" RightEage="14" TopEage="4" BottomEage="4" Scale9OriginX="-14" Scale9OriginY="-4" Scale9Width="28" Scale9Height="8" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1560.0000" Y="687.7601" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8125" Y="0.6368" />
            <PreSize X="0.0521" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Red" ActionTag="-1695103258" Tag="723" IconVisible="False" LeftMargin="1510.0000" RightMargin="310.0000" TopMargin="485.1000" BottomMargin="494.9000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="14" RightEage="14" TopEage="4" BottomEage="4" Scale9OriginX="-14" Scale9OriginY="-4" Scale9Width="28" Scale9Height="8" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1560.0000" Y="544.9000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8125" Y="0.5045" />
            <PreSize X="0.0521" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Blue" ActionTag="1517043817" Tag="724" IconVisible="False" LeftMargin="1511.1355" RightMargin="308.8645" TopMargin="622.0519" BottomMargin="357.9481" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="14" RightEage="14" TopEage="4" BottomEage="4" Scale9OriginX="-14" Scale9OriginY="-4" Scale9Width="28" Scale9Height="8" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1561.1355" Y="407.9481" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8131" Y="0.3777" />
            <PreSize X="0.0521" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" ActionTag="1405658099" VisibleForFrame="False" Tag="605" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" ActionTag="-725685491" VisibleForFrame="False" Tag="606" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="-685690176" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="1497696396" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2_0" ActionTag="580272019" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>