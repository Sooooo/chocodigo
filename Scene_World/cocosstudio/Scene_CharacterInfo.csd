<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_CharacterInfo" ID="410d5d88-a3a3-4def-af02-e8d91b61010a" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="275" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="info_back_1" ActionTag="1286382706" Tag="97" IconVisible="False" LeftMargin="-0.0002" RightMargin="0.0002" TopMargin="1.2878" BottomMargin="-1.2879" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position X="-0.0002" Y="-1.2879" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0000" Y="-0.0012" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/CharacterInfo/info_back.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="game_replay" ActionTag="-1087262541" Tag="701" IconVisible="False" LeftMargin="1396.9137" RightMargin="124.0863" TopMargin="760.7599" BottomMargin="214.2401" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="369" Scale9Height="83" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="399.0000" Y="105.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1596.4137" Y="266.7401" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8315" Y="0.2470" />
            <PreSize X="0.2078" Y="0.0972" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_QusetReplay.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_close" ActionTag="697021528" Tag="3" IconVisible="False" LeftMargin="1726.0863" RightMargin="73.9137" TopMargin="61.9030" BottomMargin="897.0970" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="90" Scale9Height="99" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="120.0000" Y="121.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1786.0863" Y="957.5970" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9303" Y="0.8867" />
            <PreSize X="0.0625" Y="0.1120" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/CharacterInfo/button_close.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="r1_2" ActionTag="-283600578" Tag="703" IconVisible="False" LeftMargin="801.5759" RightMargin="885.4241" TopMargin="384.1135" BottomMargin="425.8865" ctype="SpriteObjectData">
            <Size X="233.0000" Y="270.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="918.0759" Y="560.8865" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4782" Y="0.5193" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/CharacterInfo/r1.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="r2_3" ActionTag="926728811" Tag="704" IconVisible="False" LeftMargin="1036.9845" RightMargin="650.0155" TopMargin="383.3097" BottomMargin="427.6903" ctype="SpriteObjectData">
            <Size X="233.0000" Y="269.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1153.4845" Y="562.1903" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6008" Y="0.5205" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/CharacterInfo/r2.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="r3_4" ActionTag="470606833" Tag="705" IconVisible="False" LeftMargin="1037.3342" RightMargin="648.6658" TopMargin="521.3804" BottomMargin="288.6196" ctype="SpriteObjectData">
            <Size X="234.0000" Y="270.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1154.3342" Y="423.6196" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6012" Y="0.3922" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/CharacterInfo/r3.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="r4_5" ActionTag="1971186246" Tag="706" IconVisible="False" LeftMargin="1037.1385" RightMargin="649.8615" TopMargin="658.1025" BottomMargin="151.8975" ctype="SpriteObjectData">
            <Size X="233.0000" Y="270.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1153.6385" Y="286.8975" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6009" Y="0.2656" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/CharacterInfo/r4.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="r5_6" ActionTag="1817556239" Tag="707" IconVisible="False" LeftMargin="800.4547" RightMargin="885.5454" TopMargin="658.7258" BottomMargin="151.2742" ctype="SpriteObjectData">
            <Size X="234.0000" Y="270.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="917.4547" Y="286.2742" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4778" Y="0.2651" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/CharacterInfo/r5.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="r6_7" ActionTag="1252074558" Tag="708" IconVisible="False" LeftMargin="799.6218" RightMargin="886.3782" TopMargin="521.3794" BottomMargin="288.6206" ctype="SpriteObjectData">
            <Size X="234.0000" Y="270.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="916.6218" Y="423.6206" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4774" Y="0.3922" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/CharacterInfo/r6.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_1" ActionTag="1703109124" Tag="702" IconVisible="False" LeftMargin="1684.8850" RightMargin="-164.8850" TopMargin="464.8378" BottomMargin="515.1622" FontSize="80" IsCustomSize="True" LabelText="" PlaceHolderText="0" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="400.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1884.8850" Y="565.1622" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.9817" Y="0.5233" />
            <PreSize X="0.2083" Y="0.0926" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulHangangM_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_1_0" ActionTag="734278956" Tag="107" IconVisible="False" LeftMargin="1444.4141" RightMargin="225.5859" TopMargin="476.6639" BottomMargin="403.3360" FontSize="60" IsCustomSize="True" LabelText="" PlaceHolderText="Chapter" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="250.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1569.4141" Y="503.3360" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.8174" Y="0.4661" />
            <PreSize X="0.1302" Y="0.1852" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulHangangM_0.ttf" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TextField_1_0_0" ActionTag="-126976978" Tag="108" IconVisible="False" LeftMargin="1508.4377" RightMargin="211.5623" TopMargin="559.2303" BottomMargin="320.7696" FontSize="60" IsCustomSize="True" LabelText="" PlaceHolderText="진행중" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1608.4377" Y="420.7696" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.8377" Y="0.3896" />
            <PreSize X="0.1042" Y="0.1852" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulHangangM_0.ttf" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>