<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_1_1" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="BackGround" ActionTag="953446860" IconVisible="False" LeftMargin="1.9881" RightMargin="-1.9882" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position X="1.9881" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0010" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/bg.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Start" ActionTag="-376201240" Tag="601" IconVisible="False" LeftMargin="1288.5000" RightMargin="268.5000" TopMargin="722.5001" BottomMargin="242.4999" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="363" Scale9Height="115" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1470.0000" Y="299.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7656" Y="0.2778" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Start.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Reset" ActionTag="-1205153796" Tag="603" IconVisible="False" LeftMargin="1288.4951" RightMargin="268.5049" TopMargin="872.5031" BottomMargin="92.4969" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="363" Scale9Height="115" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1469.9951" Y="149.9969" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7656" Y="0.1389" />
            <PreSize X="0.1891" Y="0.1065" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Reset.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Clothes" ActionTag="1086250893" Tag="711" IconVisible="False" LeftMargin="1153.0000" RightMargin="127.0000" TopMargin="81.5000" BottomMargin="418.5000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="580.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1473.0000" Y="708.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7672" Y="0.6560" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/IMG_T_SHIRTS.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Pattern_Sprite" ActionTag="1481761777" VisibleForFrame="False" Tag="712" IconVisible="False" LeftMargin="1153.0000" RightMargin="127.0000" TopMargin="81.5000" BottomMargin="418.5000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="580.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1473.0000" Y="708.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7672" Y="0.6560" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/IMG_PATTERN_SPRITE.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Pattern_Star" ActionTag="1860005549" VisibleForFrame="False" Tag="713" IconVisible="False" LeftMargin="1153.0000" RightMargin="127.0000" TopMargin="81.5000" BottomMargin="418.5000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="580.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1473.0000" Y="708.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7672" Y="0.6560" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/IMG_PATTERN_STAR.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_Pattern_Snow" ActionTag="-1244101309" VisibleForFrame="False" Tag="714" IconVisible="False" LeftMargin="1153.0000" RightMargin="127.0000" TopMargin="81.5000" BottomMargin="418.5000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="580.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1473.0000" Y="708.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7672" Y="0.6560" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/IMG_PATTERN_SNOW.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_SelectValue_1_1" ActionTag="226217148" Tag="701" IconVisible="False" LeftMargin="435.5987" RightMargin="1224.4014" TopMargin="213.0000" BottomMargin="727.0000" TouchEnable="True" FontSize="72" ButtonText="상의" Scale9Enable="True" LeftEage="9" RightEage="9" TopEage="4" BottomEage="4" Scale9OriginX="9" Scale9OriginY="4" Scale9Width="110" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="260.0000" Y="140.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="565.5987" Y="797.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2946" Y="0.7380" />
            <PreSize X="0.1354" Y="0.1296" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_Selected_1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_SelectValue_1_2" ActionTag="1469474277" Tag="702" IconVisible="False" LeftMargin="716.7533" RightMargin="943.2467" TopMargin="213.0000" BottomMargin="727.0000" TouchEnable="True" FontSize="72" ButtonText="하의" Scale9Enable="True" LeftEage="9" RightEage="9" TopEage="4" BottomEage="4" Scale9OriginX="9" Scale9OriginY="4" Scale9Width="110" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="260.0000" Y="140.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="846.7533" Y="797.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4410" Y="0.7380" />
            <PreSize X="0.1354" Y="0.1296" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_Selected_1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_SelectValue_2_1" ActionTag="-620689184" Tag="703" IconVisible="False" LeftMargin="438.0421" RightMargin="1311.9579" TopMargin="489.4371" BottomMargin="450.5629" TouchEnable="True" FontSize="72" ButtonText="빨강" Scale9Enable="True" LeftEage="9" RightEage="9" TopEage="4" BottomEage="4" Scale9OriginX="9" Scale9OriginY="4" Scale9Width="113" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="170.0000" Y="140.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="523.0421" Y="520.5629" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2724" Y="0.4820" />
            <PreSize X="0.0885" Y="0.1296" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_Selected_2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_SelectValue_2_2" ActionTag="-1527805389" Tag="704" IconVisible="False" LeftMargin="630.3979" RightMargin="1119.6021" TopMargin="489.4401" BottomMargin="450.5599" TouchEnable="True" FontSize="72" ButtonText="파랑" Scale9Enable="True" LeftEage="9" RightEage="9" TopEage="4" BottomEage="4" Scale9OriginX="9" Scale9OriginY="4" Scale9Width="112" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="170.0000" Y="140.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="715.3979" Y="520.5599" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3726" Y="0.4820" />
            <PreSize X="0.0885" Y="0.1296" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_Selected_2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_SelectValue_2_3" ActionTag="-1152558516" Tag="705" IconVisible="False" LeftMargin="813.1375" RightMargin="936.8625" TopMargin="489.4400" BottomMargin="450.5600" TouchEnable="True" FontSize="72" ButtonText="노랑" Scale9Enable="True" LeftEage="9" RightEage="9" TopEage="4" BottomEage="4" Scale9OriginX="9" Scale9OriginY="4" Scale9Width="112" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="170.0000" Y="140.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="898.1375" Y="520.5600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4678" Y="0.4820" />
            <PreSize X="0.0885" Y="0.1296" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_Selected_2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_SelectValue_3_1" ActionTag="-444353892" Tag="706" IconVisible="False" LeftMargin="438.0399" RightMargin="1311.9601" TopMargin="764.4400" BottomMargin="175.5600" TouchEnable="True" FontSize="72" ButtonText="별" Scale9Enable="True" LeftEage="9" RightEage="9" TopEage="4" BottomEage="4" Scale9OriginX="9" Scale9OriginY="4" Scale9Width="43" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="170.0000" Y="140.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="523.0399" Y="245.5600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2724" Y="0.2274" />
            <PreSize X="0.0885" Y="0.1296" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_Selected_2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_SelectValue_3_2" ActionTag="-1860134315" Tag="707" IconVisible="False" LeftMargin="630.4000" RightMargin="1119.6000" TopMargin="761.7460" BottomMargin="178.2540" TouchEnable="True" FontSize="72" ButtonText="줄" Scale9Enable="True" LeftEage="9" RightEage="9" TopEage="4" BottomEage="4" Scale9OriginX="9" Scale9OriginY="4" Scale9Width="47" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="170.0000" Y="140.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="715.4000" Y="248.2540" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3726" Y="0.2299" />
            <PreSize X="0.0885" Y="0.1296" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_Selected_2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_SelectValue_3_3" ActionTag="-952050062" Tag="708" IconVisible="False" LeftMargin="813.1399" RightMargin="936.8601" TopMargin="764.4400" BottomMargin="175.5600" TouchEnable="True" FontSize="72" ButtonText="눈" Scale9Enable="True" LeftEage="9" RightEage="9" TopEage="4" BottomEage="4" Scale9OriginX="9" Scale9OriginY="4" Scale9Width="47" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="170.0000" Y="140.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="898.1399" Y="245.5600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4678" Y="0.2274" />
            <PreSize X="0.0885" Y="0.1296" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulNamsanB_0.ttf" Plist="" />
            <TextColor A="255" R="26" G="26" B="26" />
            <DisabledFileData Type="Normal" Path="Game_Resource/1_MAGICIAN/Game_1/Button_Selected_2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Correct" ActionTag="118980397" VisibleForFrame="False" Tag="605" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Wrong" ActionTag="14613671" VisibleForFrame="False" Tag="606" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="-362914791" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="244224578" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2_0" ActionTag="1430136552" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>