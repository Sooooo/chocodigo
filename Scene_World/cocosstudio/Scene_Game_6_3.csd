<GameProjectFile>
  <PropertyGroup Type="Scene" Name="Scene_Game_6_3" ID="2e3375d0-09b3-488f-912b-de7240a86627" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="145" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="background_26" ActionTag="1000650999" IconVisible="False" LeftMargin="-0.5852" RightMargin="0.5852" TopMargin="0.3318" BottomMargin="-0.3318" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="959.4148" Y="539.6682" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4997" Y="0.4997" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/Background.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_1" ActionTag="1354748215" Tag="701" IconVisible="False" LeftMargin="61.5000" RightMargin="1681.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="150.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0781" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p1.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p1.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p1_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_10" ActionTag="473767899" Tag="710" IconVisible="False" LeftMargin="1654.5000" RightMargin="88.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1743.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9078" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p10.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p10.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p10_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_2" ActionTag="1733835272" Tag="702" IconVisible="False" LeftMargin="238.5000" RightMargin="1504.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="327.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1703" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p2.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p2.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p2_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_3" ActionTag="-61652616" Tag="703" IconVisible="False" LeftMargin="415.5000" RightMargin="1327.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="504.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2625" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p3.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p3.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p3_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_4" ActionTag="-1651779781" Tag="704" IconVisible="False" LeftMargin="592.5000" RightMargin="1150.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="681.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3547" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p4.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p4.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p4_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_5" ActionTag="1550063764" Tag="705" IconVisible="False" LeftMargin="769.5000" RightMargin="973.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="858.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4469" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p5.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p5.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p5_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_6" ActionTag="50242634" Tag="706" IconVisible="False" LeftMargin="946.5000" RightMargin="796.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1035.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5391" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p6.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p6.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p6_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_7" ActionTag="1124806909" Tag="707" IconVisible="False" LeftMargin="1123.5000" RightMargin="619.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1212.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6313" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p7.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p7.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p7_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_8" ActionTag="1489318106" Tag="708" IconVisible="False" LeftMargin="1300.5000" RightMargin="442.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1389.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7234" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p8.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p8.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p8_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="People_9" ActionTag="-260032987" Tag="709" IconVisible="False" LeftMargin="1477.5000" RightMargin="265.5000" TopMargin="250.0000" BottomMargin="370.0000" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="177.0000" Y="460.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1566.0000" Y="600.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8156" Y="0.5556" />
            <PreSize X="0.0922" Y="0.4259" />
            <NormalBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p9.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p9.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="Game_Resource/6_PSEUDO/Game_3/p9_c.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_27" ActionTag="-594868283" Tag="711" IconVisible="False" LeftMargin="1107.6531" RightMargin="449.3469" TopMargin="902.5000" BottomMargin="62.5000" TouchEnable="True" FontSize="70" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1289.1531" Y="120.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6714" Y="0.1111" />
            <PreSize X="0.1891" Y="0.1065" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulHangangM_0.ttf" Plist="" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Change.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_27_0" ActionTag="588849785" Tag="712" IconVisible="False" LeftMargin="1496.8730" RightMargin="60.1270" TopMargin="902.5000" BottomMargin="62.5000" TouchEnable="True" FontSize="70" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="333" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="363.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1678.3730" Y="120.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8742" Y="0.1111" />
            <PreSize X="0.1891" Y="0.1065" />
            <FontResource Type="Normal" Path="Game_Resource/System/Font/08SeoulHangangM_0.ttf" Plist="" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Pass.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_O" ActionTag="1208553646" VisibleForFrame="False" Tag="605" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/O.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_X" ActionTag="1305599806" VisibleForFrame="False" Tag="606" IconVisible="False" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="1890" Scale9Height="1058" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/X.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="-2060058513" Tag="11" IconVisible="True" LeftMargin="18.0000" RightMargin="1344.0000" TopMargin="25.0001" BottomMargin="907.9999" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="558.0000" Y="147.0000" />
            <AnchorPoint />
            <Position X="18.0000" Y="907.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.8407" />
            <PreSize X="0.2906" Y="0.1361" />
            <FileData Type="Normal" Path="Layer_Option.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_Option" ActionTag="1602203658" Tag="4" IconVisible="False" LeftMargin="19.0000" RightMargin="1749.0000" TopMargin="24.0000" BottomMargin="904.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="187" Scale9Height="195" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="152.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="980.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0495" Y="0.9074" />
            <PreSize X="0.2724" Y="1.0340" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="Game_Resource/System/Button_Option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2" ActionTag="439008771" VisibleForFrame="False" Tag="12" IconVisible="True" LeftMargin="600.0000" RightMargin="520.0000" TopMargin="330.0000" BottomMargin="350.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="800.0000" Y="400.0000" />
            <AnchorPoint />
            <Position X="600.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.3241" />
            <PreSize X="0.4167" Y="0.3704" />
            <FileData Type="Normal" Path="ExitPopUp.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>